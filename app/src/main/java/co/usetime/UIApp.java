package co.usetime;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageInfo;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import net.gotev.uploadservice.UploadService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import co.usetime.di.AppModules;
import co.usetime.mvp.model.AppInfo;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import dagger.ObjectGraph;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import org.acra.*;
import org.acra.annotation.*;
/**
 * Created by Roger Patiño on 05/01/2016.
 */
@ReportsCrashes(
        //formUri = "https://collector.tracepot.com/b93aa3dd"
)
public class UIApp extends Application {
    protected static String TAG = UIApp.class.getSimpleName();

    //context
    public static Context getContext;

    //dagge
    private ObjectGraph objectGraph;
    private List<String> lockapps = null;


    @Override
    public void onCreate() {
        super.onCreate();
        getContext = getApplicationContext();
        MultiDex.install(this);

        new Prefs.Builder().setContext(this).setMode(ContextWrapper.MODE_PRIVATE).setPrefsName(getPackageName()).setUseDefaultSharedPreference(true).build();
        initDependencyInjection();

        RealmConfiguration config = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();//instacncia
        Realm.setDefaultConfiguration(config);//migracion por defector evitar error
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        UploadService.NAMESPACE = "co.usetime";

        ACRA.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public ObjectGraph buildGraphWithAditionalModules(List<Object> modules) {
        if (modules == null) {
            throw new IllegalArgumentException("You can't plus a null module, review your getModules() implementation");
        }
        return objectGraph.plus(modules.toArray());
    }

    private void initDependencyInjection() {
        objectGraph = ObjectGraph.create(new AppModules(this));
        objectGraph.inject(this);
        objectGraph.injectStatics();
    }

    //verificar si hay cuenta en adminitrador
    public static boolean getAccounts() {
        synchronized (getContext) {
            AccountManager accountManager = AccountManager.get(getContext);
            return (accountManager.getAccountsByType(getContext.getResources().getString(R.string.account_type))).length > 0;
        }
    }

    public static Account getUserAccount() {
        synchronized (getContext) {
            AccountManager accountManager = AccountManager.get(getContext);
            Account[] accounts = accountManager.getAccountsByType(getContext.getResources().getString(R.string.account_type));
            for (Account account : accounts) {
                if (account != null) {
                    return account;
                } else {
                    return null;
                }
            }
            return null;
        }
    }

    public void createFolders() {
        File internalFile = new File(Constants.PATH_BASE + Constants.PATH_SCREENSHOT);
        File externalFile = new File(Environment.getExternalStorageDirectory(), Constants.PATH_BASE + Constants.PATH_SCREENSHOT);

        File internalFile2 = new File(Constants.PATH_BASE + Constants.PATH_FILES);
        File externalFile2 = new File(Environment.getExternalStorageDirectory(), Constants.PATH_BASE + Constants.PATH_FILES);

        //ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
        //File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);

        // check if external storage is available and not read only
        if (Functions.isExternalStorageAvailable() || !Functions.isExternalStorageReadOnly()) {
            if (!externalFile.exists()) {
                if (!externalFile.mkdirs()) {
                    Log.e(TAG, "Problem creating folder External");
                }
            }
        } else {
            if (!internalFile.exists()) {
                if (!internalFile.mkdirs()) {
                    Log.e(TAG, "Problem creating folder Internal");
                }
            }
        }

        if (Functions.isExternalStorageAvailable() || !Functions.isExternalStorageReadOnly()) {
            if (!internalFile2.exists()) {
                if (!internalFile2.mkdirs()) {
                    Log.e(TAG, "Problem creating folder External");
                }
            }
        } else {
            if (!externalFile2.exists()) {
                if (!externalFile2.mkdirs()) {
                    Log.e(TAG, "Problem creating folder Internal");
                }
            }
        }



    }

    public static boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(getContext);
        if (result != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    public static synchronized AppInfo getPackage(String paquete) {
        List<AppInfo> apps = getInstalledApps(); /* false = no lo paquete del sistema */
        for (int i = 0; i < apps.size(); i++) {
            if (apps.get(i).packageName.equals(paquete)) {
                return apps.get(i);
            } else {
                return null;
            }
        }
        return null;
    }

    public static String getPackageName(String mPaquete) {
        Log.d(TAG, "getPackageName");
        List<AppInfo> apps = getInstalledApps();
        for (int i = 0; i < apps.size(); i++) {
            Log.d(TAG, "getPackageName name:" + apps.get(i).packageName);

            if (apps.get(i).packageName.equals(mPaquete)) {
                Log.d(TAG, "getPackageName" + apps.get(i).appName);
                return apps.get(i).appName;
            } else {
                return null;
            }
        }
        return null;
    }

    public static AppInfo getPackageInfo(String paquete) {
        List<AppInfo> apps = getInstalledApps(); /* false = no lo paquete del sistema */
        for (int i = 0; i < apps.size(); i++) {
            if (apps.get(i).packageName.equals(paquete)) {
                return apps.get(i);
            } else {
                return null;
            }
        }
        return null;
    }

    //obetener la lista que va a proveer los paquetes de las aplicaciòn
    public static synchronized List<AppInfo> getInstalledApps() {
        ArrayList<AppInfo> listPackage = new ArrayList<>();//arreglo donde se van a mater los paquetes
        List<PackageInfo> lisPacks = getContext.getPackageManager().getInstalledPackages(0);//obtener los paquetes
        for (int i = 0; i < lisPacks.size(); i++) {//recorrer los paquetes
            PackageInfo packageInfo = lisPacks.get(i);
            if ((packageInfo.versionName == null)) {
                continue;
            }
            //obetener los datos de los paquetes
            AppInfo appInfo = new AppInfo();
            appInfo.appName = packageInfo.applicationInfo.loadLabel(getContext.getPackageManager()).toString();
            appInfo.packageName = packageInfo.packageName;
            appInfo.versionName = packageInfo.versionName;
            appInfo.versionCode = packageInfo.versionCode;
            appInfo.srcDir = packageInfo.applicationInfo.sourceDir;
            appInfo.appIcon = packageInfo.applicationInfo.loadIcon(getContext.getPackageManager());
            listPackage.add(appInfo);
        }

        return listPackage;
    }

    //obetener la lista que va a proveer los paquetes de las aplicaciòn
    public static List<AppInfo> getInstalledApps(boolean getSysPackages) {
        ArrayList<AppInfo> listPackage = new ArrayList<>();//arreglo donde se van a mater los paquetes
        List<PackageInfo> lisPacks = getContext.getPackageManager().getInstalledPackages(0);//obtener los paquetes
        for (int i = 0; i < lisPacks.size(); i++) {//recorrer los paquetes
            PackageInfo packageInfo = lisPacks.get(i);
            if ((!getSysPackages) && (packageInfo.versionName == null)) {
                continue;
            }

            //obetener los datos de los paquetes
            AppInfo newPackageInfo = new AppInfo();
            newPackageInfo.setAppName(packageInfo.applicationInfo.loadLabel(getContext.getPackageManager()).toString());
            newPackageInfo.setPackageName(packageInfo.packageName);
            newPackageInfo.setVersionName(packageInfo.versionName);
            newPackageInfo.setVersionCode(packageInfo.versionCode);
            newPackageInfo.setSrcDir(packageInfo.applicationInfo.sourceDir);
            newPackageInfo.setAppIcon(packageInfo.applicationInfo.loadIcon(getContext.getPackageManager()));
            listPackage.add(newPackageInfo);
        }

        return listPackage;
    }

    //obetener la lista que va a proveer los paquetes de las aplicaciòn
    public static synchronized String getInstalledApps2(String mPackage) {
        List<PackageInfo> lisPacks = getContext.getPackageManager().getInstalledPackages(0);//obtener los paquetes
        for (int i = 0; i < lisPacks.size(); i++) {//recorrer los paquetes
            PackageInfo packageInfo = lisPacks.get(i);
            if ((packageInfo.versionName == null)) {
                continue;
            }

            Log.d(TAG, "getInstalledApps2:" + packageInfo.packageName);
            if (mPackage.equals(packageInfo.packageName)) {
                return packageInfo.applicationInfo.loadLabel(getContext.getPackageManager()).toString();
            } else {
                return null;
            }
        }
        return null;
    }

    public List<String> getLockapps() {
        return lockapps;
    }

    public void setLockapps(List<String> lockapps) {
        this.lockapps = lockapps;
    }

    public void addValue(String packageName) {
        System.out.println("........." + packageName);
        if (!this.lockapps.contains(packageName))
            this.lockapps.add(packageName);
    }

    public void removeValue(String packageName) {
        this.lockapps.remove(packageName);
    }

    public boolean clearValue() {
        return this.lockapps.removeAll(getLockapps());
    }
}