package co.usetime.bus;

/**
 * Created by Roger Patiño on 27/01/2016.
 */
public class CameraEvent {

    private int id;
    private boolean camena;

    public int getId() {
        return id;
    }

    public CameraEvent(boolean camena, int id) {
        this.camena = camena;
        this.id = id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public boolean isCamena() {
        return camena;
    }

    public void setCamena(boolean camena) {
        this.camena = camena;
    }
}
