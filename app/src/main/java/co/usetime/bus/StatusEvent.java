package co.usetime.bus;

/**
 * Created by Roger Patiño on 16/03/2016.
 */
public class StatusEvent {

    private int id;

    public StatusEvent(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}