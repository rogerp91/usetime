package co.usetime.bus;

/**
 * Created by Roger Patiño on 28/01/2016.
 */
public class CaptureEvent {

    private int id;

    public CaptureEvent(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}