package co.usetime.bus;

/**
 * Created by Roger Patiño on 14/03/2016.
 */
public class FileEvent {

    private int id;

    public FileEvent(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}