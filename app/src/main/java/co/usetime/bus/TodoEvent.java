package co.usetime.bus;

/**
 * Created by Roger Patiño on 21/04/2016.
 */
public class TodoEvent {

    private boolean ONLINE;
    private int STATUS;
    private boolean ERROR;

    public boolean isONLINE() {
        return ONLINE;
    }

    public void setONLINE(boolean ONLINE) {
        this.ONLINE = ONLINE;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public boolean isERROR() {
        return ERROR;
    }

    public void setERROR(boolean ERROR) {
        this.ERROR = ERROR;
    }
}