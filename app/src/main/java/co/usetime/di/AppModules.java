package co.usetime.di;

import co.usetime.UIApp;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
@Module(injects = UIApp.class, library = true, includes = {ExecutorModule.class, RepositoryModule.class})
public class AppModules {

    public UIApp app;

    public AppModules(UIApp app) {
        this.app = app;
    }

    @Provides
    public UIApp provideApplication() {
        return this.app;
    }
}