package co.usetime.di;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 01/12/2015.
 */
@Module(includes = {
        ActivityGraphModule.class,
        PresenterModule.class,
        InteractorModule.class,
        SnackbarModule.class,
        NetworkClientModule.class},
        library = true,
        complete = false)
public class ActivityModule {

    private Activity activityContext;

    public ActivityModule(Activity activityContext) {
        this.activityContext = activityContext;
    }

    @Provides
    public Context provideActivityContext() {
        return activityContext;
    }

    @Provides
    public Activity provideActivityActivity() {//para el fragment
        return activityContext;
    }
}