package co.usetime.di;

import javax.inject.Singleton;

import co.usetime.mvp.presenter.imp.InvistesDocumentPresenterImp;
import co.usetime.mvp.presenter.imp.LoginPresenterImp;
import co.usetime.mvp.presenter.imp.ProyectPresenterImp;
import co.usetime.mvp.presenter.imp.TaskPresenterImp;
import co.usetime.mvp.presenter.imp.TodoPresenterImp;
import co.usetime.mvp.presenter.interfaces.InvistesDocumentPresenter;
import co.usetime.mvp.presenter.interfaces.LoginPresenter;
import co.usetime.mvp.presenter.interfaces.ProyectPresenter;
import co.usetime.mvp.presenter.interfaces.TaskPresenter;
import co.usetime.mvp.presenter.interfaces.TodoPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 01/12/2015.
 */
@Module(library = true, complete = false)
public class PresenterModule {

    @Provides
    @Singleton
    public LoginPresenter provideLoginPresenter(LoginPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    public ProyectPresenter provideProyectPresenter(ProyectPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    public TaskPresenter provideProyectPresenter(TaskPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    public TodoPresenter provideTodoPresenter(TodoPresenterImp presenter) {
        return presenter;
    }


    @Provides
    @Singleton
    public InvistesDocumentPresenter provideInvitesPresenter(InvistesDocumentPresenterImp presenter) {
        return presenter;
    }
}