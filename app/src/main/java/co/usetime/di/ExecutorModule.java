package co.usetime.di;

import javax.inject.Singleton;

import co.usetime.executor.Executor;
import co.usetime.executor.MainThread;
import co.usetime.executor.MainThreadImpl;
import co.usetime.executor.ThreadExecutor;
import dagger.Module;
import dagger.Provides;


/**
 * Created by Roger Patiño on 30/11/2015.
 */
@Module(library = true)
public class ExecutorModule {

    @Provides
    @Singleton
    public Executor provideExecutor(ThreadExecutor threadExecutor) {
        return threadExecutor;
    }

    @Provides
    @Singleton
    public MainThread provideMainThread(MainThreadImpl impl) {
        return impl;
    }
}