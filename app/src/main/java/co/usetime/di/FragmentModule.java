package co.usetime.di;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
@Module(includes = {
        FragmentGraphInjectModule.class,
        PresenterModule.class,
        InteractorModule.class,
        SnackbarModule.class,
        NetworkClientModule.class},
        library = true,
        complete = false)
public class FragmentModule {

    private final Activity activityContext;

    public FragmentModule(Activity activityContext) {
        this.activityContext = activityContext;
    }

    @Provides
    public Context provideActivityContext() {
        return activityContext;
    }

    @Provides
    public Activity provideActivityActivity() {//para el fragment
        return activityContext;
    }
}