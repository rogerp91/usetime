package co.usetime.di;

import javax.inject.Singleton;

import co.usetime.domain.repository.imp.DocumentRepositoryImp;
import co.usetime.domain.repository.imp.ProyectRepositoryImp;
import co.usetime.domain.repository.imp.TaskRepositoryImp;
import co.usetime.domain.repository.imp.TodoRepositoryImp;
import co.usetime.domain.repository.imp.TotalImpRepository;
import co.usetime.domain.repository.interfaces.DocumentRepository;
import co.usetime.domain.repository.interfaces.Repository;
import co.usetime.domain.repository.interfaces.TodoRepository;
import co.usetime.domain.repository.interfaces.TotalRepository;
import co.usetime.mvp.model.Proyect;
import co.usetime.mvp.model.Tasks;
import co.usetime.mvp.model.TasksOfProject;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 18/02/2016.
 */
@Module(library = true, complete = false)
public class RepositoryModule {

    @Provides
    @Singleton
    public Repository<Proyect> provideProyectPresenter() {
        return new ProyectRepositoryImp();
    }

    @Provides
    @Singleton
    public Repository<TasksOfProject> provideTasksOfProjectRepository() {
        return new TaskRepositoryImp();
    }

    @Provides
    @Singleton
    public TodoRepository provideTodoRepository() {
        return new TodoRepositoryImp();
    }

    @Provides
    @Singleton
    public TotalRepository provideTotalRepository() {
        return new TotalImpRepository();
    }

    @Provides
    @Singleton
    public DocumentRepository provideDocumentlRepository() {
        return new DocumentRepositoryImp();
    }
}