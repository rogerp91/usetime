package co.usetime.di;

import javax.inject.Singleton;

import co.usetime.componets.ShowSnackbarMsg;
import co.usetime.mvp.presenter.interfaces.ShowMsgPresenter;
import co.usetime.mvp.view.GetView;
import dagger.Module;
import dagger.Provides;

@Module(library = true)
public class SnackbarModule {

    private GetView iView;

    public SnackbarModule(GetView iView) {
        this.iView = iView;
    }

    @Provides
    @Singleton
    public GetView provideView() {
        return this.iView;
    }

    @Provides
    @Singleton
    public ShowMsgPresenter provideErrorManager() {
        return new ShowSnackbarMsg(iView);
    }
}
