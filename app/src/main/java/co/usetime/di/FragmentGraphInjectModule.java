package co.usetime.di;

import co.usetime.ui.fragment.ProyectFragment;
import co.usetime.ui.fragment.TodoFragment;
import dagger.Module;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
@Module(injects = {
        ProyectFragment.class,
        TodoFragment.class,
}, complete = false, library = true)
public class FragmentGraphInjectModule {
}
