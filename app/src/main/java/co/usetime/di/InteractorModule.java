package co.usetime.di;

import javax.inject.Singleton;

import co.usetime.domain.interactor.imp.ChangeTaskTodoInteractorImp;
import co.usetime.domain.interactor.imp.CommentInteractorImp;
import co.usetime.domain.interactor.imp.InvitesAndDocumentInteractorImp;
import co.usetime.domain.interactor.imp.LoginInteractorImp;
import co.usetime.domain.interactor.imp.PeddingInteractorImp;
import co.usetime.domain.interactor.imp.ProyectInteractorImp;
import co.usetime.domain.interactor.imp.TaskInteractorImp;
import co.usetime.domain.interactor.imp.TodoInteractorImp;
import co.usetime.domain.interactor.interfaces.ChangeTaskTodoInteractor;
import co.usetime.domain.interactor.interfaces.CommentInteractor;
import co.usetime.domain.interactor.interfaces.InvitesAndDocumentInteractor;
import co.usetime.domain.interactor.interfaces.LoginInteractor;
import co.usetime.domain.interactor.interfaces.PeddingInteractor;
import co.usetime.domain.interactor.interfaces.ProyectInteractor;
import co.usetime.domain.interactor.interfaces.TaskInteractor;
import co.usetime.domain.interactor.interfaces.TodoInteractor;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 01/12/2015.
 */
@Module(library = true, complete = false)
public class InteractorModule {

    @Provides
    @Singleton
    public LoginInteractor provideLoginInteractor(LoginInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public ProyectInteractor provideLoginInteractor(ProyectInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public TaskInteractor provideLoginInteractor(TaskInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public TodoInteractor provideTodoInteractor(TodoInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public PeddingInteractor providePeddingInteractor(PeddingInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public ChangeTaskTodoInteractor providechangeTaskTodoInteractor(ChangeTaskTodoInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public InvitesAndDocumentInteractor provideDocumentInteractor(InvitesAndDocumentInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public CommentInteractor providecommentInteractor(CommentInteractorImp interactor) {
        return interactor;
    }
}