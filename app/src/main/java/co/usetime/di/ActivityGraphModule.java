package co.usetime.di;

import co.usetime.ui.activity.DetailTaskActivity;
import co.usetime.ui.activity.LoginActivity;
import co.usetime.ui.activity.MainActivity;
import co.usetime.ui.activity.MyTaskActivity;
import co.usetime.ui.fragment.ProyectFragment;
import dagger.Module;

@Module(injects = {
        MainActivity.class,
        LoginActivity.class,
        ProyectFragment.class,
        MyTaskActivity.class,
        DetailTaskActivity.class
}, complete = false)
public class ActivityGraphModule {

}
