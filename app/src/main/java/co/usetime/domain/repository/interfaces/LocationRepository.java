package co.usetime.domain.repository.interfaces;

import java.util.List;

import co.usetime.exception.ObtainException;
import co.usetime.exception.UpdateException;

/**
 * Created by Roger Patiño on 21/03/2016.
 */
public interface LocationRepository<E> {

    void persist(E objects);

    void persist(List<E> objects);

    List<E> obtain(String id) throws ObtainException;

    List<E> obtainParent(String parent_id) throws ObtainException;

    List<E> obtain() throws ObtainException;

    E obtainSimple(String id) throws ObtainException;

    boolean get(String id);

    boolean getParent(String id);

    void clear();

    void updateStatus(String id, String status) throws UpdateException;

}