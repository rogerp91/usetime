package co.usetime.domain.repository.interfaces;

import java.util.List;

import co.usetime.exception.AddException;
import co.usetime.exception.GetException;
import co.usetime.exception.UniqueException;
import co.usetime.exception.UpdateException;

/**
 * Created by Roger Patiño on 13/01/2016.
 */
public interface Repository2<E> {

    void close();

    void clear();

    void add(E e) throws AddException, UniqueException;

    void update(String pk, int kb) throws UpdateException;

    void update(E e) throws UpdateException;

    E get(String pk) throws GetException;

    List<E> obtain() throws GetException;

}