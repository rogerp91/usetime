package co.usetime.domain.repository.interfaces;

import java.util.List;

import co.usetime.exception.ObtainException;
import co.usetime.exception.UpdateException;

/**
 * Created by Roger Patiño on 26/01/2016.
 */
public interface Repository<E> {

    void persist(List<E> objects);

    List<E> obtain(String id) throws ObtainException;

    List<E> obtainParent(String parent_id) throws ObtainException;

    List<E> obtain() throws ObtainException;

    E obtainSimple(String id) throws ObtainException;

    void updateSimple(E objects) throws UpdateException;

    boolean get(String id);

    boolean getParent(String id);

    void clear();

    void updateStatus(String id, String status) throws UpdateException;
}