package co.usetime.domain.repository.imp;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.usetime.domain.repository.interfaces.DocumentRepository;
import co.usetime.exception.ObtainException;
import co.usetime.exception.UniqueException;
import co.usetime.mvp.model.ResponseDocument;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 11/05/2016.
 */
public class DocumentRepositoryImp implements DocumentRepository {

    protected static String TAG = DocumentRepositoryImp.class.getSimpleName();
    private volatile Realm realm;

    @Inject
    public DocumentRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void persist(ResponseDocument objects) {
        try {
            Log.d(TAG, "persist");
            realm.beginTransaction();
            ResponseDocument realmObject = realm.createObject(ResponseDocument.class);
            realmObject.setCreator_id(objects.getCreator_id());
            realmObject.setType(objects.getType());
            realmObject.setTask_id(objects.getTask_id());
            realmObject.setCreator_id(objects.getCreator_id());
            realmObject.setId(objects.getId());
            realmObject.setSend(objects.isSend());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            realm.cancelTransaction();
            new UniqueException();
        }
    }

    @Override
    public List<ResponseDocument> obtain(boolean send) throws ObtainException {
        List<ResponseDocument> documentList = new ArrayList<>();
        RealmResults<ResponseDocument> results = realm.where(ResponseDocument.class).equalTo("isSend", send).findAll();
        if (results.size() > 0) {
            for (ResponseDocument document : results) {
                documentList.add(document);
            }
            return documentList;
        } else {
            return null;
        }
    }

    @Override
    public List<ResponseDocument> obtain() throws ObtainException {
        List<ResponseDocument> documentList = new ArrayList<>();
        RealmResults<ResponseDocument> results = realm.where(ResponseDocument.class).findAll();
        if (results.size() > 0) {
            for (ResponseDocument document : results) {
                documentList.add(document);
            }
            return documentList;
        } else {
            return null;
        }
    }

    @Override
    public ResponseDocument obtainSimple(boolean send) throws ObtainException {
        return null;
    }

    @Override
    public ResponseDocument obtainSimple(String id) throws ObtainException {
        ResponseDocument results = realm.where(ResponseDocument.class).equalTo("id", id).findFirst();
        if (results != null) {
            return results;
        } else {
            return null;
        }
    }

    @Override
    public void clear() {
        realm.beginTransaction();
        realm.clear(ResponseDocument.class);
        realm.commitTransaction();
    }

    @Override
    public void setSend(int id, boolean send) {
        ResponseDocument results = realm.where(ResponseDocument.class).equalTo("id", id).findFirst();
        if (results != null) {
            realm.beginTransaction();
            results.setSend(send);
            realm.commitTransaction();
        }
    }
}