package co.usetime.domain.repository.imp;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.usetime.domain.repository.interfaces.TrafficRepository;
import co.usetime.exception.AddException;
import co.usetime.exception.GetException;
import co.usetime.exception.UniqueException;
import co.usetime.exception.UpdateException;
import co.usetime.mvp.model.Traffic;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 13/05/2016.
 */
public class TrafficRepositoryImp implements TrafficRepository<Traffic> {

    protected static String TAG = TrafficRepositoryImp.class.getSimpleName();
    private Realm realm;

    public TrafficRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void close() {
        realm.close();
    }

    @Override
    public void clear() {
        realm.beginTransaction();
        realm.clear(Traffic.class);
        realm.commitTransaction();
    }

    @Override
    public void add(Traffic traffic) throws AddException, UniqueException {
        Log.d(TAG, "Add traffic");
        try {
            realm.beginTransaction();
            Traffic traf = realm.createObject(Traffic.class);
            //traf.setmTotalKb(traffic.getmTotalKb());
            traf.setmWifiKb(traffic.getmWifiKb());
            traf.setmMovilKb(traffic.getmMovilKb());
            traf.setmPackage(traffic.getmPackage());
            traf.setmName(traffic.getmName());
            realm.commitTransaction();
        } catch (RealmException e) {
            //Log.e(TAG, e.toString());
            new AddException();
            realm.cancelTransaction();
        } catch (RealmPrimaryKeyConstraintException ec) {
            //Log.e(TAG, ec.toString());
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    @Override
    public void update(String pk, int kb) throws UpdateException {
        try {
            realm.beginTransaction();
            Traffic traffic = realm.where(Traffic.class).equalTo(RealmConstants.PAQUETE, pk).findAll().first();
            traffic.setmWifiKb(kb);
            realm.commitTransaction();
        } catch (RealmException e) {
            realm.cancelTransaction();
            new UpdateException();
        }
    }

    @Override
    public void update(Traffic traffic) throws UpdateException {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(traffic);
        realm.commitTransaction();
    }

    @Override
    public Traffic get(String pk) throws GetException {
        try {
            return realm.where(Traffic.class).equalTo(RealmConstants.PAQUETE, pk).findAll().first() != null ? realm.where(Traffic.class).equalTo(RealmConstants.PAQUETE, pk).findAll().first() : null;
        } catch (RuntimeException | RealmError e) {
            realm.cancelTransaction();
            new GetException();
        }
        return null;
    }

    @Override
    public List<Traffic> get() throws GetException {
        List<Traffic> traffics = null;
        try {
            RealmResults<Traffic> realmResults = realm.where(Traffic.class).findAll();
            if (realmResults != null) {
                traffics = new ArrayList<>();
                for (Traffic obj : realmResults) {
                    traffics.add(obj);
                }
            }
        } catch (RuntimeException | RealmError e) {
            realm.cancelTransaction();
            new GetException();
        }
        return traffics;
    }
}