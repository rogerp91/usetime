package co.usetime.domain.repository.imp;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.usetime.domain.repository.interfaces.Repository;
import co.usetime.domain.repository.interfaces.Repository2;
import co.usetime.exception.AddException;
import co.usetime.exception.GetException;
import co.usetime.exception.UniqueException;
import co.usetime.exception.UpdateException;
import co.usetime.mvp.model.NameClass;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.annotations.RealmClass;
import io.realm.exceptions.RealmError;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 12/04/2016.
 */
public class ClassRepository implements Repository2<NameClass> {
    //var
    protected static String TAG = ClassRepository.class.getSimpleName();
    private Realm realm;

    public ClassRepository() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void close() {
        realm.close();
    }

    @Override
    public void clear() {
        realm.beginTransaction();
        realm.clear(NameClass.class);
        realm.commitTransaction();
    }

    @Override
    public void add(final NameClass nameClass) throws AddException, UniqueException {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm1) {
                try {
                    NameClass nameClass1 = realm1.createObject(NameClass.class);
                    nameClass1.setmPackage(nameClass.getmPackage());
                    nameClass1.setmClass(nameClass.getmClass());
                    nameClass1.setmDuration(nameClass.getmDuration());
                } catch (RealmException e) {
                    Log.e(TAG, e.toString());
                    new AddException();
                    realm.cancelTransaction();
                } catch (RealmPrimaryKeyConstraintException ec) {
                    Log.e(TAG, ec.toString());
                    new UniqueException();
                    realm.cancelTransaction();
                }
            }
        });
    }

    @Override
    public void update(String pk, int kb) throws UpdateException {

    }

    @Override
    public void update(NameClass nameClass) throws UpdateException {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(nameClass);
        realm.commitTransaction();
    }

    @Override
    public NameClass get(String pk) throws GetException {
        return null;
    }

    @Override
    public List<NameClass> obtain() throws GetException {
        List<NameClass> nameClasses = null;
        try {
            RealmResults<NameClass> realmResults = realm.where(NameClass.class).findAll();
            if (realmResults != null) {
                nameClasses = new ArrayList<>();
                for (NameClass obj : realmResults) {
                    nameClasses.add(obj);
                }
            }
        } catch (RealmException e) {
            Log.e(TAG, e.toString());
            realm.cancelTransaction();
            new GetException();
        } catch (RuntimeException | RealmError e) {
            realm.cancelTransaction();
            Log.e(TAG, e.toString());
            new GetException();
        }
        return nameClasses;
    }
}