package co.usetime.domain.repository.imp;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.usetime.domain.repository.interfaces.Repository;
import co.usetime.exception.ObtainException;
import co.usetime.exception.UniqueException;
import co.usetime.exception.UpdateException;
import co.usetime.mvp.model.Imagen;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 26/02/2016.
 */
public class ImagenRepositoryImp implements Repository<Imagen> {

    protected static String TAG = ImagenRepositoryImp.class.getSimpleName();
    private Realm realm;

    public ImagenRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void persist(List<Imagen> objects) {
        Log.d(TAG, "persist");
        for (Imagen imagen : objects) {
            try {
                persist(imagen);
            } catch (UniqueException e) {
                update(imagen);
            }
        }
    }

    @Override
    public List<Imagen> obtain(String id) throws ObtainException {
        List<Imagen> imagenList = new ArrayList<>();//instancia la lista
        RealmResults<Imagen> results = realm.where(Imagen.class).equalTo("id_task", id).findAll();
        if (results.size() > 0) {
            for (Imagen imagen : results) {
                Log.d(TAG, imagen.getId());
                Imagen object = new Imagen();
                object.setId(imagen.getId());
                object.setId_task(imagen.getId_task());
                imagenList.add(object);
            }
            return imagenList;
        } else {
            return null;
        }
    }

    @Override
    public List<Imagen> obtainParent(String parent_id) throws ObtainException {
        return null;
    }

    @Override
    public List<Imagen> obtain() throws ObtainException {
        List<Imagen> imagenList = new ArrayList<>();//instancia la lista
        RealmResults<Imagen> results = realm.where(Imagen.class).findAll();
        if (results.size() > 0) {
            for (Imagen imagen : results) {
                Log.d(TAG, imagen.getId());
                Imagen object = new Imagen();
                object.setId(imagen.getId());
                object.setId_task(imagen.getId_task());
                imagenList.add(object);
            }
            return imagenList;
        } else {
            return null;
        }
    }

    @Override
    public Imagen obtainSimple(String id) throws ObtainException {
        return null;
    }

    @Override
    public void updateSimple(Imagen objects) throws UpdateException {

    }

    @Override
    public boolean get(String id) {
        RealmQuery<Imagen> query = realm.where(Imagen.class).equalTo("id", id);//se trae todos los objetos
        return query.count() == 0 ? false : true;
    }

    @Override
    public boolean getParent(String id) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public void updateStatus(String id, String status) {

    }

    private void persist(Imagen object) throws UniqueException {
        try {
            Log.d(TAG, object.getId());
            realm.beginTransaction();
            Imagen realmObject = realm.createObject(Imagen.class);
            realmObject.setId(object.getId());
            realmObject.setId_task(object.getId_task());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
        }
    }

    private void update(Imagen object) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(object);
        realm.commitTransaction();
    }
}