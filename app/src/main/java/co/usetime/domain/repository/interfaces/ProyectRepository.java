package co.usetime.domain.repository.interfaces;

import co.usetime.mvp.model.Proyect;

/**
 * Created by Roger Patiño on 18/02/2016.
 */
public interface ProyectRepository extends Repository<Proyect> {

}