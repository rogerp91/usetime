package co.usetime.domain.repository.imp;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.usetime.domain.repository.interfaces.Repository2;
import co.usetime.exception.AddException;
import co.usetime.exception.GetException;
import co.usetime.exception.UniqueException;
import co.usetime.exception.UpdateException;
import co.usetime.mvp.model.AppProcess;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 12/04/2016.
 */
public class ProcessRepository implements Repository2<AppProcess> {

    //var
    protected static String TAG = ProcessRepository.class.getSimpleName();
    private Realm realm;

    public ProcessRepository() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public synchronized void close() {
        realm.close();
    }

    @Override
    public synchronized void clear() {
        Log.d(TAG, "clear");
        realm.beginTransaction();
        realm.clear(AppProcess.class);
        realm.commitTransaction();

    }

    @Override
    public void add(final AppProcess appProcess) throws AddException, UniqueException {
        //Log.d(TAG, "addApp");
        try {
            realm.beginTransaction();
            AppProcess appProcess1 = realm.createObject(AppProcess.class);
            appProcess1.setmName(appProcess.getmName());
            //Log.d(TAG, "name: " + appProcess.getmName());

            appProcess1.setmPackage(appProcess.getmPackage());
            appProcess1.setmDate(appProcess.getmDate());
            appProcess1.setmTime(appProcess.getmTime());
            appProcess1.setmDuration(appProcess.getmDuration());
            appProcess1.setmPath(appProcess.getmPath());
            realm.commitTransaction();
        } catch (RealmException e) {
            Log.e(TAG, e.toString());
            new AddException();
            realm.cancelTransaction();
        } catch (RealmPrimaryKeyConstraintException ec) {
            Log.e(TAG, ec.toString());
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    @Override
    public void update(String pk, int kb) throws UpdateException {

    }

    @Override
    public void update(AppProcess appProcess) throws UpdateException {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(appProcess);
        realm.commitTransaction();
    }

    @Override
    public AppProcess get(String pk) throws GetException {
        try {
            return realm.where(AppProcess.class).equalTo(RealmConstants.PAQUETE, pk).findAll().first() != null ? realm.where(AppProcess.class).equalTo(RealmConstants.PAQUETE, pk).findAll().first() : null;
        } catch (RuntimeException | RealmError e) {
            realm.cancelTransaction();
            new GetException();
        }
        return null;
    }

    @Override
    public synchronized List<AppProcess> obtain() throws GetException {
        Log.d(TAG, "get process");
        List<AppProcess> appProcesses = new ArrayList<>();
        try {
            RealmResults<AppProcess> results = realm.where(AppProcess.class).findAll();
            for (AppProcess obj : results) {
                //Log.d(TAG, "get name: " + obj.getmName());
                appProcesses.add(obj);
            }
        } catch (RuntimeException | RealmError e) {
            Log.e(TAG, e.toString());
            realm.cancelTransaction();
            new GetException();
        }
        return appProcesses;
    }
}