package co.usetime.domain.repository.imp;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.usetime.domain.object.ProyectObject;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.domain.repository.interfaces.Repository;
import co.usetime.domain.repository.interfaces.TotalRepository;
import co.usetime.exception.ObtainException;
import co.usetime.exception.UniqueException;
import co.usetime.exception.UpdateException;
import co.usetime.mvp.model.Proyect;
import co.usetime.utils.Models;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 18/02/2016.
 */
public class ProyectRepositoryImp implements Repository<Proyect> {

    protected static String TAG = ProyectRepositoryImp.class.getSimpleName();
    private volatile Realm realm;

    @Inject
    TotalRepository totalRepository;

    @Inject
    public ProyectRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public synchronized void persist(List<Proyect> objects) {
        Log.d(TAG, "persist");
        for (Proyect proyect : objects) {
            try {
                persist(proyect);
            } catch (UniqueException e) {
                update(proyect);
            }
        }
    }

    @Override
    public synchronized List<Proyect> obtain(String id) throws ObtainException {
        List<Proyect> proyectList = new ArrayList<>();
        RealmResults<ProyectObject> results = realm.where(ProyectObject.class).equalTo("id", id).findAll();
        if (results.size() > 0) {
            for (ProyectObject proyectObject : results) {
                proyectList.add(Models.RealmToProyect(proyectObject));
            }
            return proyectList;
        } else {
            return null;
        }
    }

    @Override
    public synchronized List<Proyect> obtainParent(String parent_id) throws ObtainException {
        List<Proyect> proyectList = new ArrayList<>();//instancia la lista
        RealmResults<ProyectObject> results = realm.where(ProyectObject.class).equalTo("parent_id", parent_id).findAll();
        if (results.size() > 0) {
            for (ProyectObject proyectObject : results) {
                //Log.d(TAG, proyectObject.getName());
                ProyectObject object = proyectObject;
                proyectList.add(Models.RealmToProyect(object));
            }
            return proyectList;
        } else {
            return null;
        }
    }

    @Override
    public synchronized List<Proyect> obtain() throws ObtainException {
        List<Proyect> proyectList = new ArrayList<>();//instancia la lista
        RealmResults<ProyectObject> results = realm.where(ProyectObject.class).findAll();
        if (results.size() > 0) {
            for (ProyectObject proyectObject : results) {
                //Log.d(TAG, proyectObject.getName());
                ProyectObject object = proyectObject;
                proyectList.add(Models.RealmToProyect(object));
            }
            return proyectList;
        } else {
            return null;
        }
    }

    @Override
    public synchronized Proyect obtainSimple(String id) throws ObtainException {
        //Log.d(TAG, id);
        ProyectObject results = realm.where(ProyectObject.class).equalTo("id", id).findAll().first();
        if (results != null) {
            return Models.RealmToProyect(results);
        } else {
            return null;
        }
    }

    @Override
    public synchronized void updateSimple(Proyect objects) throws UpdateException {

    }

    @Override
    public synchronized boolean get(String id) {
        RealmQuery<ProyectObject> query = realm.where(ProyectObject.class).equalTo("id", id);//se trae todos los objetos
        return query.count() == 0 ? false : true;
    }

    @Override
    public synchronized boolean getParent(String id) {
        return false;
    }

    @Override
    public synchronized void clear() {
        realm.beginTransaction();
        realm.clear(ProyectObject.class);
        realm.commitTransaction();
    }

    @Override
    public synchronized void updateStatus(String id, String status) {

    }

    /**
     * private
     *
     * @param object
     * @throws UniqueException
     */
    private synchronized void persist(final Proyect object) throws UniqueException {
        //Log.d(TAG, object.getId());
        //System.out.println(TAG + " id: " + object.getId());
        try {
            realm.beginTransaction();
            ProyectObject realmObject = realm.createObject(ProyectObject.class);
            realmObject.setId(object.getId());
            realmObject.setCreator_id(object.getCreator_id());
            realmObject.setType(object.getType());
            realmObject.setName(object.getName());
            realmObject.setParent_id(object.getParent_id());
            realmObject.setProjects_id(object.getProjects_id());
            realmObject.setProject_category_id(object.getProject_category_id());
            realmObject.setStatus(object.getStatus());
            realmObject.setStart_date(object.getStart_date());
            realmObject.setEnd_date(object.getEnd_date());
            realmObject.setUser_id(object.getUser_id());
            realmObject.setTask_id(object.getTask_id());
            realmObject.setTotal_delivered_validate(object.getTotal_delivered_validate());
            realmObject.setTotal_delivered(object.getTotal_delivered());
            realmObject.setTotal_process(object.getTotal_process());
            realmObject.setTotal_expired(object.getTotal_expired());
            realmObject.setTotal_task(object.getTotal_task());
            realmObject.setAcumulado(object.getAcumulado());
            realmObject.setTotal_time(object.getTotal_time());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    private synchronized void update(Proyect object) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(Models.ProyectToRealm(object));
        realm.commitTransaction();

    }

}