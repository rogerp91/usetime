package co.usetime.domain.repository.imp;

import java.util.List;

import javax.inject.Inject;

import co.usetime.domain.object.TasksObject;
import co.usetime.domain.repository.interfaces.Repository;
import co.usetime.exception.ObtainException;
import co.usetime.exception.UniqueException;
import co.usetime.exception.UpdateException;
import co.usetime.mvp.model.TasksOfProject;
import co.usetime.utils.Models;
import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 18/02/2016.
 */
public class TaskRepositoryImp implements Repository<TasksOfProject> {

    protected static String TAG = TaskRepositoryImp.class.getSimpleName();
    private Realm realm;

    @Inject
    public TaskRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void persist(List<TasksOfProject> objects) {

    }

    @Override
    public List<TasksOfProject> obtain(String id) throws ObtainException {
        return null;
    }

    @Override
    public List<TasksOfProject> obtainParent(String parent_id) throws ObtainException {
        return null;
    }

    @Override
    public List<TasksOfProject> obtain() throws ObtainException {
        return null;
    }

    @Override
    public TasksOfProject obtainSimple(String id) throws ObtainException {
        return null;
    }

    @Override
    public void updateSimple(TasksOfProject objects) throws UpdateException {

    }

    @Override
    public boolean get(String id) {
        return false;
    }

    @Override
    public boolean getParent(String id) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public void updateStatus(String id, String status) throws UpdateException {

    }

    /**
     * private
     *
     * @param object
     * @throws UniqueException
     */
    private void persist(TasksOfProject object) throws UniqueException {
    }

    private void update(TasksOfProject object) {

    }
}