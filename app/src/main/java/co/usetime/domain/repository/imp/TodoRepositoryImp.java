package co.usetime.domain.repository.imp;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.usetime.domain.object.TasksObject;
import co.usetime.domain.repository.interfaces.TodoRepository;
import co.usetime.exception.ObtainException;
import co.usetime.exception.UniqueException;
import co.usetime.mvp.model.Completed;
import co.usetime.mvp.model.Expired;
import co.usetime.mvp.model.Pending;
import co.usetime.mvp.model.Progress;
import co.usetime.mvp.model.Todo;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 07/03/2016.
 */
public class TodoRepositoryImp implements TodoRepository {

    protected static String TAG = TodoRepositoryImp.class.getSimpleName();
    private Realm realm;

    @Inject
    public TodoRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void persist(Todo objects) {
        /**
        Log.d(TAG, "persist");
        for (Pending pending : objects.getPending()) {
            try {
                persistPedding(pending);
            } catch (UniqueException e) {
                updatePedding(pending);
            }
        }
        for (Progress progress : objects.getProgress()) {
            try {
                persistProcess(progress);
            } catch (UniqueException e) {
                updateProgress(progress);
            }
        }
        for (Completed completed : objects.getCompleted()) {
            try {
                persistCompleted(completed);
            } catch (UniqueException e) {
                updateCompleted(completed);
            }
        }
        for (Expired expired : objects.getExpired()) {
            try {
                persistExpired(expired);
            } catch (UniqueException e) {
                updateExpired(expired);
            }
        }
        */
    }

    @Override
    public Todo obtain() throws ObtainException {
        Log.d(TAG, "obtain");
        Todo todo = new Todo();
        /**
        List<Pending> pendingList = new ArrayList<>();
        List<Progress> progressList = new ArrayList<>();
        List<Completed> completedList = new ArrayList<>();
        List<Expired> expiredList = new ArrayList<>();

        RealmResults<Pending> results1 = realm.where(Pending.class).findAll();
        RealmResults<Progress> results2 = realm.where(Progress.class).findAll();
        RealmResults<Completed> results3 = realm.where(Completed.class).findAll();
        RealmResults<Expired> results4 = realm.where(Expired.class).findAll();


        if (results1.size() > 0) {
            for (Pending pendingObj : results1) {
                System.out.println("ID: " + pendingObj.getId());
                pendingList.add(pendingObj);
            }
        } else {
            pendingList.add(null);
        }

        if (results2.size() > 0) {
            for (Progress progressObj : results2) {
                progressList.add(progressObj);
            }
        } else {
            progressList.add(null);
        }

        if (results3.size() > 0) {
            for (Completed completedObj : results3) {
                completedList.add(completedObj);
            }
        } else {
            completedList.add(null);
        }

        if (results4.size() > 0) {
            for (Expired expiredObj : results4) {
                expiredList.add(expiredObj);
            }
        } else {
            expiredList.add(null);
        }

        todo.setPending(pendingList);
        todo.setProgress(progressList);
        todo.setCompleted(completedList);
        todo.setExpired(expiredList);
         */
        return todo;
    }

    @Override
    public Todo obtain(int type){
        Todo todo = new Todo();
        /**
        todo.setPending(null);
        todo.setProgress(null);
        todo.setCompleted(null);
        todo.setExpired(null);
        switch (type) {
            case 1:
                todo.setPending(getPeeding());
                break;
            case 2:
                todo.setProgress(getProgress());
                break;
            case 3:
                todo.setCompleted(getCompleted());
                break;
            case 4:
                todo.setExpired(getExpired());
                break;
            case 5:
                break;
        }
         */
        return todo;
    }

    @Override
    public boolean get(String id) {
        return false;
    }

    @Override
    public void clear() {
        /**
        realm.beginTransaction();
        realm.clear(Pending.class);
        realm.commitTransaction();
         */
    }

    @Override
    public List<?> obtainObject(int type) throws ObtainException {
        /**
        switch (type) {
            case 1:
                List<Pending> pendingList = new ArrayList<>();
                RealmResults<Pending> results1 = realm.where(Pending.class).findAll();
                if (results1.size() > 0) {
                    for (Pending pendingObj : results1) {
                        pendingList.add(pendingObj);
                    }
                } else {
                    pendingList.add(null);
                }
                return pendingList;
            case 2:
                List<Progress> progressList = new ArrayList<>();
                RealmResults<Progress> results2 = realm.where(Progress.class).findAll();
                if (results2.size() > 0) {
                    for (Progress progressObj : results2) {
                        progressList.add(progressObj);
                    }
                } else {
                    progressList.add(null);
                }
                return progressList;
            case 3:
                List<Completed> completedList = new ArrayList<>();
                RealmResults<Completed> results3 = realm.where(Completed.class).findAll();
                if (results3.size() > 0) {
                    for (Completed completedObj : results3) {
                        completedList.add(completedObj);
                    }
                } else {
                    completedList.add(null);
                }
                return completedList;
            case 4:
                List<Expired> expiredList = new ArrayList<>();
                RealmResults<Expired> results4 = realm.where(Expired.class).findAll();
                if (results4.size() > 0) {
                    for (Expired expiredObj : results4) {
                        expiredList.add(expiredObj);
                    }
                } else {
                    expiredList.add(null);
                }
                return expiredList;
            default:
        }
         */
        return null;
    }

    @Override
    public boolean update(String id, int type) {
        switch (type) {

        }
        return false;
    }

    /**
     * private
     *
     * @param object
     * @throws UniqueException
     */
    private void persistPedding(Pending object) throws UniqueException {
        /**
        try {
            Log.d(TAG, object.getId());
            realm.beginTransaction();
            Log.d(TAG, object.getName());
            Pending realmObject = realm.createObject(Pending.class);
            realmObject.setId(object.getId());
            realmObject.setName(object.getName());
            realmObject.setStatus(object.getStatus());
            realmObject.setCreated_at(object.getCreated_at());
            realmObject.setType(object.getType());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
         */
    }

    private void updatePedding(Pending object) {
        /**
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(object);
        realm.commitTransaction();
         */
    }

    private void persistProcess(Progress object) throws UniqueException {
        /**
        try {
            realm.beginTransaction();
            Progress realmObject = realm.createObject(Progress.class);
            realmObject.setId(object.getId());
            realmObject.setName(object.getName());
            realmObject.setStatus(object.getStatus());
            realmObject.setCreated_at(object.getCreated_at());
            realmObject.setType(object.getType());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
         */
    }

    private void updateProgress(Progress object) {
        /**
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(object);
        realm.commitTransaction();
         */
    }

    private void persistCompleted(Completed object) throws UniqueException {
        /**
        try {
            realm.beginTransaction();
            Completed realmObject = realm.createObject(Completed.class);
            realmObject.setId(object.getId());
            realmObject.setName(object.getName());
            realmObject.setStatus(object.getStatus());
            realmObject.setCreated_at(object.getCreated_at());
            realmObject.setType(object.getType());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
         */
    }

    private void updateCompleted(Completed object) {
        /**
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(object);
        realm.commitTransaction();
         */
    }

    private void persistExpired(Expired object) throws UniqueException {
        /**
        try {
            realm.beginTransaction();
            Expired realmObject = realm.createObject(Expired.class);
            realmObject.setId(object.getId());
            realmObject.setName(object.getName());
            realmObject.setStatus(object.getStatus());
            realmObject.setCreated_at(object.getCreated_at());
            realmObject.setType(object.getType());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
         */
    }

    private void updateExpired(Expired object) {
        /**
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(object);
        realm.commitTransaction();
         */
    }

    private List<Pending> getPeeding() {
        List<Pending> pendingList = new ArrayList<>();
        /**
        RealmResults<Pending> results2 = realm.where(Pending.class).findAll();
        if (results2.size() > 0) {
            for (Pending pendingObj : results2) {
                System.out.println("ID: " + pendingObj.getId());
                pendingList.add(pendingObj);
            }
        } else {
            pendingList.add(null);
        }
         */
        return pendingList;
    }

    private List<Progress> getProgress() {
        List<Progress> progressList = new ArrayList<>();
        /**
        RealmResults<Progress> results2 = realm.where(Progress.class).findAll();
        if (results2.size() > 0) {
            for (Progress progressObj : results2) {
                progressList.add(progressObj);
            }
        } else {
            progressList.add(null);
        }
        */
        return progressList;
    }

    private List<Completed> getCompleted() {
        List<Completed> completedList = new ArrayList<>();
        /**
        RealmResults<Completed> results = realm.where(Completed.class).findAll();
        if (results.size() > 0) {
            for (Completed completedObj : results) {
                completedList.add(completedObj);
            }
        } else {
            completedList.add(null);
        }
        */
        return completedList;
    }

    private List<Expired> getExpired() {
        List<Expired> expiredListList = new ArrayList<>();
        /**
        RealmResults<Expired> results = realm.where(Expired.class).findAll();
        if (results.size() > 0) {
            for (Expired expiredObj : results) {
                expiredListList.add(expiredObj);
            }
        } else {
            expiredListList.add(null);
        }
        */
        return expiredListList;
    }
}