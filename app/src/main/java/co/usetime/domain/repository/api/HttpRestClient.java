package co.usetime.domain.repository.api;

import java.util.List;

import co.usetime.BuildConfig;
import co.usetime.mvp.model.CommentsParameters;
import co.usetime.mvp.model.DocumentFile;
import co.usetime.mvp.model.Edit;
import co.usetime.mvp.model.Geoposition;
import co.usetime.mvp.model.Id;
import co.usetime.mvp.model.Invistes;
import co.usetime.mvp.model.Login;
import co.usetime.mvp.model.Profile;
import co.usetime.mvp.model.ProjectTasks;
import co.usetime.mvp.model.Proyect;
import co.usetime.mvp.model.Registries;
import co.usetime.mvp.model.Tasks;
import co.usetime.mvp.model.Todo;
import co.usetime.mvp.model.TodoSimple;
import co.usetime.mvp.model.TodoStatus;
import co.usetime.mvp.model.Token;
import co.usetime.mvp.model.UsersEnterprises;
import co.usetime.mvp.model.UsersProyect;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public interface HttpRestClient {

    public static String BASE_URL = BuildConfig.HOST;
    public static String BASE_URL_2 = "https://beta.usetime.co/api";
    public static String VERSION = "v1/";
    public static String PREFIX = "Bearer ";
    public static String USER_AGENT = "Usetime-Android/";

    /**
     * @param agent tipo de cliente
     * @param login objecto json
     * @return pr
     */
    @Headers("Content-Type: application/json")
    @POST(VERSION + "auth/login")
    Call<Token> performLogin(@Header("User-Agent") String agent, @Body Login login);

    /**
     * @param authorization token
     * @return pr
     */
    //perfil
    @Headers("Accept: application/json")
    @GET(VERSION + "profile/me")
    Call<Profile> performProfile(@Header("Authorization") String authorization);

    /**
     * @param authorization token
     * @return
     */
    //proyects
    @GET(VERSION + "projects?_with=totalDeliveredValidate,totalDelivered,totalProcess,totalExpired")
    Call<List<Proyect>> performProyect(@Header("Authorization") String authorization);

    @GET(VERSION + "projects?_with=totalTask,totalDeliveredValidate,totalDelivered,totalProcess,totalExpired,countNotifications")
    Call<List<Proyect>> performProyect2(@Header("Authorization") String authorization);

    /**
     * @param authorization autotizacion
     * @param id            id del proyecto
     * @return
     */
    @GET(VERSION + "project_with_tasks/{id}")
    Call<ProjectTasks> performProjectWithTasks(@Header("Authorization") String authorization, @Path("id") String id);

    /**
     * @param authorization token
     * @param id            de la tarea
     * @return
     */
    //tareas
    @GET(VERSION + "projects/{id}/tasks")
    Call<List<Tasks>> performTask(@Header("Authorization") String authorization, @Path("id") String id);

    /**
     * @param authorization token
     * @return
     */
    //tareas
    @GET(VERSION + "projects_with_tasks")
    Call<List<Tasks>> performProjectsWithTasks(@Header("Authorization") String authorization);

    /**
     * @param authorization token
     * @return
     */
    //todo
    @GET(VERSION + "todo")
    @Headers("Accept: application/json")
    Call<Todo> performTodo(@Header("Authorization") String authorization);

    /**
     * @param authorization token
     * @param status        estado
     * @param id            id de la tarea
     * @return
     */
    //type todo
    @PATCH(VERSION + "tasks/{id}/todo")
    Call<TodoSimple> perfomTypeTodo(@Header("Authorization") String authorization, @Body TodoStatus status, @Path("id") String id);

    @PATCH(VERSION + "tasks/{id}/reject")
    Call<TodoSimple> perfomTodoReject(@Header("Authorization") String authorization, @Body Id body, @Path("id") String id);

    @PATCH(VERSION + "tasks/{id}/complete")
    Call<TodoSimple> perfomTodoComplete(@Header("Authorization") String authorization, @Body Id body, @Path("id") String id);

    @PATCH(VERSION + "tasks/{id}/todo")
    Call<TodoSimple> perfomType3(@Header("Authorization") String authorization, @Body TodoStatus status, @Path("id") String id);

    //type todo
    @PATCH(VERSION + "tasks/{id}/{type}")
    Call<TodoSimple> perfomType(@Header("Authorization") String authorization, @Body Id body, @Path("id") String id, @Path("type") String type);

    @PATCH(VERSION + "tasks/{id}/{type}")
    Call<TodoSimple> perfomType2(@Header("Authorization") String authorization, @Body TodoStatus status, @Path("id") String id, @Path("type") String type);


    /**
     * @param authorization token
     * @param complete      objecto con los valores del objeto
     * @param id
     * @return
     */
    //type todo
    @PATCH(VERSION + "tasks/{id}/complete")
    Call<Void> perfomTaskComplete(@Header("Authorization") String authorization, @Body Id complete, @Path("id") String id);


    /**
     * @param authorization token
     * @param complete      objeto
     * @param id            de la tarea
     * @return
     */
    //type rechazar
    @PATCH(VERSION + "tasks/{id}/reject")
    Call<Void> perfomTaskRejected(@Header("Authorization") String authorization, @Body Id complete, @Path("id") String id);

    /**
     * @param authorization token
     * @param geoposition   objeto pasado por form-data
     * @return
     */
    //geolocalizacion

    /**
     * @FormUrlEncoded
     * @Headers("Accept: application/json")
     * @POST(VERSION + "geopositions")
     * Call<Void> perfomGeoposition(@Header("Authorization") String authorization, @Field("position") String geoposition);
     */
    @POST(VERSION + "geopositions")
    Call<Void> perfomGeoposition(@Header("Authorization") String authorization, @Body Geoposition geoposition);


    @Headers("Accept: application/json")
    @POST(VERSION + "docuement/{id}/geoposition")
    Call<Void> perfomGeopositionDocument(@Header("Authorization") String authorization, @Field("position") Geoposition geoposition);

    /**
     * @param authorization token
     * @param id            de la tarea para buscar los usuarios que estan registrado
     * @return
     */
    //enterprises
    @GET(VERSION + "enterprises/{id}/users")
    Call<List<UsersEnterprises>> perfomEnterprisesUser(@Header("Authorization") String authorization, @Path("id") String id);

    @GET(VERSION + "projects/{id}/users")
    Call<List<UsersEnterprises>> perfomsUser(@Header("Authorization") String authorization, @Path("id") String id);

    /**
     * @param authorization
     * @param id
     * @return
     */

    @GET(VERSION + "proyect/{id}/users")
    Call<List<UsersProyect>> perfomProyectUser(@Header("Authorization") String authorization, @Path("id") String id);

    /**
     * @param authorization token
     * @param id            de la tarea
     * @param edit          objeto en el body de la peticion
     * @return
     */
    //editar tarea
    @PATCH(VERSION + "tasks/{id}")
    Call<Void> perfomEditTask(@Header("Authorization") String authorization, @Path("id") String id, @Body Edit edit);

    /**
     * @param authorization
     * @param status
     * @param id
     * @return
     */
    @PATCH(VERSION + "tasks/{id}/todo")
    Call<Tasks> perfomTaskValidate(@Header("Authorization") String authorization, @Body TodoStatus status, @Path("id") String id);

    /**
     * @param authorization token
     * @param comments      objeto con lo datos para pasarlo al body
     * @return
     */
    @POST(VERSION + "comments")
    Call<Void> perfomComments(@Header("Authorization") String authorization, @Body CommentsParameters comments);

    @Headers("Accept: application/json")
    @POST(VERSION + "registries")
    Call<Void> perfomRegistries(@Header("Authorization") String authorization, @Body List<Registries> registriesList);


    @Headers("Accept: application/json")
    @GET(VERSION + "documents/task/{id}?_with=creator")
    Call<List<DocumentFile>> perfomDocument(@Header("Authorization") String authorization, @Path("id") String id);

    @Headers("Accept: application/json")
    @GET(VERSION + "invitees/{id}")
    Call<List<Invistes>> perfomInvite(@Header("Authorization") String authorization, @Path("id") String id);

}