package co.usetime.domain.repository.interfaces;

import co.usetime.exception.ObtainException;
import co.usetime.mvp.model.Expired;
import co.usetime.mvp.model.Progress;
import co.usetime.mvp.model.TotalDelivered;
import co.usetime.mvp.model.TotalDeliveredValidate;
import co.usetime.mvp.model.TotalExpired;
import co.usetime.mvp.model.TotalNotifications;
import co.usetime.mvp.model.TotalProcess;
import co.usetime.mvp.model.TotalTask;
import io.realm.Realm;

/**
 * Created by Roger Patiño on 13/04/2016.
 */
public interface TotalRepository {

    /**
     * total de tareas
     *
     * @param objects total task
     */
    void persistTask(TotalTask objects);

    TotalTask obtainTask(String id) throws ObtainException;

    TotalTask obtainTask() throws ObtainException;

    void update(TotalTask objects);

    /**
     * total de validada
     *
     * @param objects totaldelivered validate
     */
    void persistDeliveredValidate(TotalDeliveredValidate objects);

    TotalDeliveredValidate obtainTaskDeliveredValidate(String id) throws ObtainException;

    TotalDeliveredValidate obtainDeliveredValidate() throws ObtainException;

    void updateDeliveredValidate(TotalDeliveredValidate objects);

    /**
     * total de validada
     *
     * @param objects total delivered
     */
    void persistTotalDelivered(TotalDelivered objects);

    TotalDelivered obtainTotalDelivered(String id) throws ObtainException;

    TotalDelivered obtainTotalDeliverede() throws ObtainException;

    void updateTotalDelivered(TotalDelivered objects);

    /**
     * total de procesos
     *
     * @param objects progreso
     */

    void persistProcess(TotalProcess objects);

    TotalProcess obtainProcess(String id) throws ObtainException;

    TotalProcess obtainProcess() throws ObtainException;

    void updateProcess(TotalProcess objects);

    /**
     * total expirada
     *
     * @param objects expirada
     */

    void persistExpired(TotalExpired objects);

    TotalExpired obtainExpired(String id) throws ObtainException;

    TotalExpired obtainExpired() throws ObtainException;

    void updateExpired(TotalExpired objects);

    /**
     * total de notificaciones
     *
     * @param objects notication
     */

    void persistNotications(TotalNotifications objects);

    TotalNotifications obtainNotications(String id) throws ObtainException;

    TotalNotifications obtainNotications() throws ObtainException;

    void updateNotications(TotalNotifications objects);
}