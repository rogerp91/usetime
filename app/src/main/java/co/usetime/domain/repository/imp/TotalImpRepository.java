package co.usetime.domain.repository.imp;

import co.usetime.domain.repository.interfaces.TotalRepository;
import co.usetime.exception.ObtainException;
import co.usetime.exception.UniqueException;
import co.usetime.mvp.model.TotalDelivered;
import co.usetime.mvp.model.TotalDeliveredValidate;
import co.usetime.mvp.model.TotalExpired;
import co.usetime.mvp.model.TotalNotifications;
import co.usetime.mvp.model.TotalProcess;
import co.usetime.mvp.model.TotalTask;
import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 13/04/2016.
 */
public class TotalImpRepository implements TotalRepository {

    protected static String TAG = TotalImpRepository.class.getSimpleName();
    private Realm realm;

    public TotalImpRepository() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void persistTask(final TotalTask objects) {
        try {
            realm.beginTransaction();
            TotalTask realmObject = realm.createObject(TotalTask.class);
            realmObject.setProjects_id(objects.getProjects_id());
            realmObject.setAggregate(objects.getAggregate());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    @Override
    public synchronized TotalTask obtainTask(String id) throws ObtainException {
        return realm.where(TotalTask.class).equalTo("id", id).findAll().first();
    }

    @Override
    public synchronized TotalTask obtainTask() throws ObtainException {
        return null;
    }

    @Override
    public void update(TotalTask objects) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(objects);
        realm.commitTransaction();
    }

    @Override
    public synchronized void persistDeliveredValidate(final TotalDeliveredValidate objects) {
        try {
            realm.beginTransaction();
            TotalDeliveredValidate realmObject = realm.createObject(TotalDeliveredValidate.class);
            realmObject.setProjects_id(objects.getProjects_id());
            realmObject.setAggregate(objects.getAggregate());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    @Override
    public synchronized TotalDeliveredValidate obtainTaskDeliveredValidate(String id) throws ObtainException {
        return realm.where(TotalDeliveredValidate.class).equalTo("id", id).findAll().first();
    }

    @Override
    public synchronized TotalDeliveredValidate obtainDeliveredValidate() throws ObtainException {
        return null;
    }

    @Override
    public synchronized void updateDeliveredValidate(TotalDeliveredValidate objects) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(objects);
        realm.commitTransaction();
    }

    @Override
    public synchronized void persistTotalDelivered(final TotalDelivered objects) {
        try {
            realm.beginTransaction();
            TotalDelivered realmObject = realm.createObject(TotalDelivered.class);
            realmObject.setProjects_id(objects.getProjects_id());
            realmObject.setAggregate(objects.getAggregate());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    @Override
    public synchronized TotalDelivered obtainTotalDelivered(String id) throws ObtainException {
        return realm.where(TotalDelivered.class).equalTo("id", id).findAll().first();
    }

    @Override
    public TotalDelivered obtainTotalDeliverede() throws ObtainException {
        return null;
    }

    @Override
    public synchronized void updateTotalDelivered(TotalDelivered objects) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(objects);
        realm.commitTransaction();
    }

    @Override
    public synchronized void persistProcess(final TotalProcess objects) {
        try {
            realm.beginTransaction();
            TotalProcess realmObject = realm.createObject(TotalProcess.class);
            realmObject.setProjects_id(objects.getProjects_id());
            realmObject.setAggregate(objects.getAggregate());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    @Override
    public synchronized TotalProcess obtainProcess(String id) throws ObtainException {
        return realm.where(TotalProcess.class).equalTo("id", id).findAll().first();
    }

    @Override
    public synchronized TotalProcess obtainProcess() throws ObtainException {
        return null;
    }

    @Override
    public synchronized void updateProcess(TotalProcess objects) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(objects);
        realm.commitTransaction();
    }

    @Override
    public synchronized void persistExpired(final TotalExpired objects) {
        try {
            realm.beginTransaction();
            TotalExpired realmObject = realm.createObject(TotalExpired.class);
            realmObject.setProjects_id(objects.getProjects_id());
            realmObject.setAggregate(objects.getAggregate());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    @Override
    public synchronized TotalExpired obtainExpired(String id) throws ObtainException {
        return realm.where(TotalExpired.class).equalTo("id", id).findAll().first();
    }

    @Override
    public synchronized TotalExpired obtainExpired() throws ObtainException {
        return null;
    }

    @Override
    public synchronized void updateExpired(TotalExpired objects) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(objects);
        realm.commitTransaction();
    }

    @Override
    public synchronized void persistNotications(final TotalNotifications objects) {
        try {
            realm.beginTransaction();
            TotalNotifications realmObject = realm.createObject(TotalNotifications.class);
            realmObject.setProjects_id(objects.getProjects_id());
            realmObject.setAggregate(objects.getAggregate());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
            realm.cancelTransaction();
        }
    }

    @Override
    public synchronized TotalNotifications obtainNotications(String id) throws ObtainException {
        return realm.where(TotalNotifications.class).equalTo("id", id).findAll().first();
    }

    @Override
    public synchronized TotalNotifications obtainNotications() throws ObtainException {
        return null;
    }

    @Override
    public synchronized void updateNotications(TotalNotifications objects) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(objects);
        realm.commitTransaction();
    }
}