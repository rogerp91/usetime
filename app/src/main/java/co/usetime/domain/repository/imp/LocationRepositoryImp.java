package co.usetime.domain.repository.imp;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import co.usetime.domain.repository.interfaces.LocationRepository;
import co.usetime.exception.ObtainException;
import co.usetime.exception.UniqueException;
import co.usetime.exception.UpdateException;
import co.usetime.mvp.model.Loct;
import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 21/03/2016.
 */
public class LocationRepositoryImp implements LocationRepository<Loct> {

    protected static String TAG = LocationRepositoryImp.class.getSimpleName();
    private Realm realm;

    @Inject
    public LocationRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void persist(Loct objects) {
        try {
            Log.d(TAG, "persist");
            realm.beginTransaction();
            Loct realmObject = realm.createObject(Loct.class);
            //int id = (int) assigId(realm.where(Loct.class).max("id"));
            //realmObject.setId(id);
            realmObject.setLatitude(objects.getLatitude());
            realmObject.setLongitude(objects.getLongitude());
            realmObject.setSend(objects.isSend());
            realm.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            realm.cancelTransaction();
            new UniqueException();
        }
    }

    @Override
    public void persist(List<Loct> objects) {

    }

    @Override
    public List<Loct> obtain(String id) throws ObtainException {
        return null;
    }

    @Override
    public List<Loct> obtainParent(String parent_id) throws ObtainException {
        return null;
    }

    @Override
    public List<Loct> obtain() throws ObtainException {
        return null;
    }

    @Override
    public Loct obtainSimple(String id) throws ObtainException {
        return null;
    }

    @Override
    public boolean get(String id) {
        return false;
    }

    @Override
    public boolean getParent(String id) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public void updateStatus(String id, String status) throws UpdateException {

    }

    private long assigId(Number number) {
        if (number == null) return 1;
        else return ((long) number + 1);
    }
}