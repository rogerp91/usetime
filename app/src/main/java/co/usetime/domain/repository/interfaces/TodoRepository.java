package co.usetime.domain.repository.interfaces;

import java.util.List;

import co.usetime.exception.ObtainException;
import co.usetime.mvp.model.Todo;

/**
 * Created by Roger Patiño on 07/03/2016.
 */
public interface TodoRepository {

    void persist(Todo objects);

    Todo obtain() throws ObtainException;

    Todo obtain(int type);

    boolean get(String id);

    void clear();

    List<?> obtainObject(int type) throws ObtainException;

    boolean update(String id, int type);
}