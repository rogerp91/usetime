package co.usetime.domain.repository.interfaces;

import java.util.List;

import co.usetime.exception.ObtainException;
import co.usetime.mvp.model.ResponseDocument;

/**
 * Created by Roger Patiño on 11/05/2016.
 */
public interface DocumentRepository {

    void persist(ResponseDocument objects);

    List<ResponseDocument> obtain(boolean send) throws ObtainException;

    List<ResponseDocument> obtain() throws ObtainException;

    ResponseDocument obtainSimple(boolean send) throws ObtainException;

    ResponseDocument obtainSimple(String id) throws ObtainException;

    void clear();

    void setSend(int id, boolean send);

}