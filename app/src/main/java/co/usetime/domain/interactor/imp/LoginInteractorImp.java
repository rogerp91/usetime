package co.usetime.domain.interactor.imp;

import android.util.Log;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.LoginInteractor;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.executor.Executor;
import co.usetime.executor.Interactor;
import co.usetime.executor.MainThread;
import co.usetime.mvp.model.Login;
import co.usetime.mvp.model.Profile;
import co.usetime.mvp.model.Token;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 16/12/2015.
 */
public class LoginInteractorImp implements Interactor, LoginInteractor {

    protected static String TAG = LoginInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private Callback callback;

    private String email;
    private String password;

    HttpRestClient restClient;

    @Inject
    public LoginInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
    }

    @Override
    public void execute(String emailAnt, String passwordAnt, Callback callBackAnt) {
        if (emailAnt == null || emailAnt.equals("")) {
            throw new IllegalArgumentException("Error user.");
        }

        if (passwordAnt == null || passwordAnt.equals("")) {
            throw new IllegalArgumentException("Error pass.");
        }

        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }

        this.callback = callBackAnt;
        this.email = emailAnt;
        this.password = passwordAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                requestPost();
                //Log.d(TAG, "run");
            }
        });
    }

    public void requestPost() {
        restClient = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(true), Functions.provideOkHttpClient()));
        Call<Token> tokenCall = restClient.performLogin(HttpRestClient.USER_AGENT + Functions.getVersion(), new Login(email, password));
        tokenCall.enqueue(new retrofit.Callback<Token>() {
            @Override
            public void onResponse(Response<Token> response, Retrofit retrofit) {
                //System.out.println("requestPost - onResponse: " + response.body());
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        //Log.d(TAG, "ME: " + response.message());
                        //Log.d(TAG, "HE: " + response.headers());
                        if (response.body() == null || response.body().getToken().equals("")) {
                            notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_204));
                        } else {
                            Token token = response.body();
                            getProfile(token.getToken());
                        }
                        break;
                    case 204:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_204));
                        break;
                    case 400:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_400));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        System.out.println(response.body());
                        break;
                    case 401:
                        notifyPetitionError(UIApp.getContext.getString(R.string.validate));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionErrorData();
                        break;
                    case 403:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_403));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 404:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_404));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 500:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        System.out.println(response.body());
                        System.out.println(response.headers());
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_500));
                        break;
                    default:
                        notifyPetitionError(UIApp.getContext.getString(R.string.error_occurred));
                        break;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, t.toString());
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.error_occurred));
            }
        });
    }

    private void getProfile(final String token) {
        Call<Profile> profileCall = restClient.performProfile(HttpRestClient.PREFIX + token);
        profileCall.enqueue(new retrofit.Callback<Profile>() {
            @Override
            public void onResponse(Response<Profile> response, Retrofit retrofit) {
                //System.out.println("getProfile - onResponse: " + response.body());
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() != null || !response.body().equals("")) {
                            Profile profile = response.body();
                            Prefs.putString(Constants.TOKEN, token);
                            Prefs.putString(Constants.NAME, profile.getName());
                            Prefs.putString(Constants.EMAIL, profile.getEmail());

                            //System.out.println(TAG + " - Profile - " + response.body().getEmail());
                            //System.out.println(TAG + " - Profile - " + response.body().getRole().getName());
                            //System.out.println(TAG + " - Profile - " + response.body().getEmail());

                            notifyPetitionSuccess(profile);
                        } else {
                            notifyPetitionErrorProfile();
                        }
                        break;
                    case 400:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_400));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 401:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionErrorData();
                        break;
                    case 403:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_403));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 404:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_404));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 500:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_500));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    default:
                        notifyPetitionError(UIApp.getContext.getString(R.string.error_occurred));

                }
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println(t.toString());
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.error_occurred));
            }
        });
    }

    private void notifyPetitionError(final String msjError) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(msjError);
            }
        });
    }

    private void notifyPetitionErrorData() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorData();
            }
        });
    }

    private void notifyPetitionSuccess(final Profile profile) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(profile);
            }
        });
    }

    private void notifyPetitionErrorProfile() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorProfile();
            }
        });
    }
}