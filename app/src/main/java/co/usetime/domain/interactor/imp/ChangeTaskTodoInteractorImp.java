package co.usetime.domain.interactor.imp;

import android.util.Log;

import com.google.gson.Gson;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.ChangeTaskTodoInteractor;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.executor.Executor;
import co.usetime.executor.Interactor;
import co.usetime.executor.MainThread;
import co.usetime.mvp.model.Id;
import co.usetime.mvp.model.TodoSimple;
import co.usetime.mvp.model.TodoStatus;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 25/04/2016.
 */
public class ChangeTaskTodoInteractorImp implements Interactor, ChangeTaskTodoInteractor {
    protected static String TAG = ChangeTaskTodoInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private ChangeTaskTodoInteractor.Callback callback;
    public boolean typeMethod;
    public int status;
    public String id;
    public String token;

    @Inject
    HttpRestClient client;

    @Inject
    public ChangeTaskTodoInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
        token = Prefs.getString(Constants.TOKEN, "");
    }

    @Override
    public void execute(String id, int status, Callback callBackAnt) {
        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }
        this.id = id;
        this.status = status;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                getChange();
            }
        });
    }

    protected void getChange() {
        switch (status) {
            case 2:
                getChangeTodo();
                break;
            case 3:
                getChangeStatus("complete");
                break;
            case 6:
                getChangeStatus("reject");
                break;

        }
    }

    private void getChangeStatus(String type) {
        System.out.println(TAG + " GSON:" + new Gson().toJson(new Id(id)));
        client = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(true), Functions.provideOkHttpClient()));
        Call<TodoSimple> call = client.perfomType(HttpRestClient.PREFIX + token, new Id(id), id, type);
        call.enqueue(new retrofit.Callback<TodoSimple>() {
            @Override
            public void onResponse(Response<TodoSimple> response, Retrofit retrofit) {
                System.out.println(response.raw());
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() != null) {
                            TodoSimple todoList = response.body();
                            if (todoList != null) {
                                notifyPetitionSuccess(todoList);
                            } else {
                                notifyPetitionVoid();
                            }
                        }
                        break;
                    case 400:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 401:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 403:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 404:
                        notifyPetitionError("No se pudo cambiar la tarea, intente de nuevo");
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 500:
                        notifyPetitionError("No se pudo cambiar la tarea, intente de nuevo");
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    default:
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, t.toString());
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.error_occurred));
            }
        });
    }

    private void getChangeTodo() {
        System.out.println("GSON:" + new Gson().toJson(new TodoStatus(id, 2)));
        client = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(true), Functions.provideOkHttpClient()));
        Call<TodoSimple> call = client.perfomType3(HttpRestClient.PREFIX + token, new TodoStatus(id, 2), id);
        call.enqueue(new retrofit.Callback<TodoSimple>() {
            @Override
            public void onResponse(Response<TodoSimple> response, Retrofit retrofit) {
                System.out.println(response.raw());
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() != null) {
                            Log.d(TAG, "STATUS:" + Integer.toString(response.body().getStatus()));
                            TodoSimple todoList = response.body();
                            if (todoList != null) {
                                notifyPetitionSuccess(todoList);
                            } else {
                                notifyPetitionVoid();
                            }
                        }
                        break;
                    case 400:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 401:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 403:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 404:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 500:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    default:
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, t.toString());
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.error_occurred));
            }
        });
    }


    private synchronized void notifyPetitionError(final String error) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(error);
            }
        });
    }

    private synchronized void notifyPetitionSuccess(final TodoSimple simple) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(simple, id, status);
            }
        });
    }

    private synchronized void notifyPetitionVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onVoid();
            }
        });
    }
}