package co.usetime.domain.interactor.imp;

import android.util.Log;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.TodoInteractor;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.domain.repository.interfaces.TodoRepository;
import co.usetime.executor.Executor;
import co.usetime.executor.Interactor;
import co.usetime.executor.MainThread;
import co.usetime.mvp.model.Todo;
import co.usetime.utils.Constants;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 02/03/2016.
 */
public class TodoInteractorImp implements Interactor, TodoInteractor {
    protected static String TAG = TodoInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private Callback callback;
    private boolean typeMethod;
    private int id;
    private String token = "";
    private boolean checkSize = false;

    @Inject
    HttpRestClient client;

    @Inject
    TodoRepository repository;

    @Inject
    public TodoInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
        token = Prefs.getString(Constants.TOKEN, "");
    }

    @Override
    public void execute(boolean typeMethod, int id, Callback callBackAnt) {
        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }
        this.id = id;
        this.typeMethod = typeMethod;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                getTodo();
            }
        });
    }

    private synchronized void getTodo() {
        //Log.d(TAG, "TOKEN: " + token);
        Call<Todo> todoCall = client.performTodo(HttpRestClient.PREFIX + token);
        todoCall.enqueue(new retrofit.Callback<Todo>() {
            @Override
            public void onResponse(Response<Todo> response, Retrofit retrofit) {
                System.out.println(TAG + " response: " + response.raw());
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() != null) {
                            Todo todoList = response.body();
                            if (todoList != null) {
                                repository.persist(todoList);

                                /**
                                 System.out.println("1: " + todoList.getPending().size());
                                 System.out.println("2: " + todoList.getProgress().size());
                                 System.out.println("3: " + todoList.getCompleted().size());
                                 System.out.println("4: " + todoList.getExpired().size());
                                 */
                                notifyPetitionSuccess(todoList);
                            } else {
                                notifyPetitionVoid();
                            }
                        }
                        break;
                    case 400:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                        break;
                    case 401:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_http_401));
                        break;
                    case 403:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_http_403));
                        break;
                    case 404:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                        break;
                    case 500:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                        break;
                    default:
                }
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println(t.toString());
                Log.e(TAG, t.toString());
                if (!checkSize) {
                    notifyPetitionErrorConnect();
                } else {
                    notifyPetitionErrorConnectVoid();
                }
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.error_occurred));
            }
        });
    }

    private synchronized void notifyPetitionError(final String error) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(error);
            }
        });
    }

    private synchronized void notifyPetitionErrorConnect() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnection();
            }
        });
    }

    private synchronized void notifyPetitionErrorContent() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContect();
            }
        });
    }

    private void notifyPetitionErrorConnectVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnectionVoid();
            }
        });
    }

    private synchronized void notifyPetitionErrorContentVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContectVoid();
            }
        });
    }

    private synchronized void notifyPetitionSuccess(final Todo todo) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(todo);
            }
        });
    }

    private synchronized void notifyPetitionVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onVoid();
            }
        });
    }
}