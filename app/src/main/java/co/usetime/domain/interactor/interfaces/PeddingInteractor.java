package co.usetime.domain.interactor.interfaces;

import java.util.List;

import co.usetime.mvp.model.Pending;

/**
 * Created by Roger Patiño on 02/03/2016.
 */
public interface PeddingInteractor {

    interface Callback {

        void onSuccess(final List<Pending> objects);

        void onVoid();

        void onErrorConnection();

        void onErrorConnectionVoid();

        void onErrorContect();

        void onErrorContectVoid();

        void onError(final String error);
    }

    void execute(boolean typeMethod, int id, Callback callBackAnt);

}