package co.usetime.domain.interactor.imp;

import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.ProyectInteractor;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.domain.repository.interfaces.Repository;
import co.usetime.exception.ObtainException;
import co.usetime.executor.Executor;
import co.usetime.executor.Interactor;
import co.usetime.executor.MainThread;
import co.usetime.mvp.model.Proyect;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Models;
import co.usetime.utils.Prefs;

/**
 * Created by Roger Patiño on 07/01/2016.
 */
public class ProyectInteractorImp implements Interactor, ProyectInteractor {

    protected static String TAG = ProyectInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private Callback callback;
    private boolean typeMethod;

    String token = "";

    @Inject
    HttpRestClient client;

    @Inject
    Repository<Proyect> repository;

    private boolean checkSize = false;
    private List<Proyect> proyectList = null;//obtener listado local

    @Inject
    public ProyectInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
        token = Prefs.getString(Constants.TOKEN, "");
    }

    @Override
    public void execute(boolean typeMethod, int id, Callback callBackAnt) {
        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }
        this.typeMethod = typeMethod;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                boolean internet = Functions.isOnline();//verificar si hay internet
                try {
                    proyectList = repository.obtain();
                } catch (ObtainException e) {
                    e.printStackTrace();
                }
                if (proyectList == null) {
                    checkSize = true;
                } else {
                    checkSize = false;
                }
                if (typeMethod) {
                    Log.d(TAG, "typeMethod = true");
                    if (checkSize) { //si no hay nada
                        if (internet) {//si hay internet y no hay nada, buscar datos de servidor
                            getProyect();
                        } else {//si no hay nada y no hay intenet mostar informacion del layout completo
                            notifyPetitionErrorConnectVoid();
                        }
                    } else {//si hay algo almacenado en la base de dato mostrar
                        notifyPetitionSuccess(getProyectList(proyectList));
                    }
                } else {
                    Log.d(TAG, "typeMethod = false");
                    if (!internet && !checkSize) { //si no hay internet, pero hay dato en la base de dato, manda error
                        notifyPetitionErrorConnect();
                    } else {
                        if (!internet && checkSize) {//
                            notifyPetitionErrorConnectVoid();
                        } else {
                            if (internet && !checkSize) {
                                getProyect();
                            } else {
                                if (internet && checkSize) {
                                    getProyect();
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * obtener proyectos
     */
    private void getProyect() {
        String url = HttpRestClient.BASE_URL + HttpRestClient.VERSION + "projects?_with=totalTask,totalDeliveredValidate,totalDelivered,totalProcess,totalExpired,countNotifications";
        Ion.with(UIApp.getContext).load(url)
                .setHeader("Authorization", HttpRestClient.PREFIX + token)
                .setTimeout(30000)
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, Response<String> result) {
                        if (e != null) {
                            if (typeMethod) {
                                if (checkSize) { //pero hay dato en la base de dato, manda error
                                    notifyPetitionErrorContentVoid();
                                } else {
                                    notifyPetitionErrorContent();
                                }
                            } else {
                                notifyPetitionErrorContent();
                            }
                        } else {
                            int code = result.getHeaders().code();
                            switch (code) {
                                case 200:
                                    Log.d(TAG, "STATUS: " + code);
                                    if (result.getResult().equals("[]")) {
                                        notifyPetitionVoid();
                                    } else {
                                        List<Proyect> proyectList = Models.parseJsonProyect(result.getResult());
                                        if (proyectList != null) {
                                            repository.persist(proyectList);
                                            notifyPetitionSuccess(proyectList);
                                        }
                                    }
                                    break;
                                case 400:
                                    Log.d(TAG, "STATUS: " + code);
                                    if (checkSize) { //pero hay dato en la base de dato, manda error
                                        notifyPetitionMsg(UIApp.getContext.getResources().getString(R.string.msg_http_400), ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                    } else {
                                        notifyPetitionErrorContent();
                                    }
                                    break;
                                case 401:
                                    Log.d(TAG, "STATUS: " + code);
                                    break;
                                case 403:
                                    Log.d(TAG, "STATUS: " + code);
                                    if (checkSize) { //pero hay dato en la base de dato, manda error
                                        notifyPetitionMsg(UIApp.getContext.getResources().getString(R.string.msg_http_403), ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                    } else {
                                        notifyPetitionErrorContent();
                                    }
                                    break;
                                case 404:
                                    Log.d(TAG, "STATUS: " + code);
                                    if (checkSize) { //pero hay dato en la base de dato, manda error
                                        notifyPetitionMsg(UIApp.getContext.getResources().getString(R.string.msg_http_404), ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                    } else {
                                        notifyPetitionErrorContent();
                                    }
                                    break;
                                case 500:
                                    Log.d(TAG, "STATUS: " + code);
                                    if (checkSize) { //pero hay dato en la base de dato, manda error
                                        notifyPetitionMsg(UIApp.getContext.getResources().getString(R.string.msg_http_500), ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                    } else {
                                        notifyPetitionErrorContent();
                                    }
                                default:
                            }
                        }
                    }
                });
    }

    private void notifyPetitionError() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError();
            }
        });
    }

    private void notifyPetitionErrorConnect() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnection();
            }
        });
    }

    private void notifyPetitionErrorContent() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContect();
            }
        });
    }

    private void notifyPetitionErrorConnectVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnectionVoid();
            }
        });
    }

    private void notifyPetitionErrorContentVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContectVoid();
            }
        });
    }

    private void notifyPetitionSuccess(final List<?> list) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(list);
            }
        });
    }

    private void notifyPetitionVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onVoid();
            }
        });
    }

    private void notifyPetitionMsg(final String msg, final int color) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onMsg(msg, color);
            }
        });
    }

    private List<Proyect> getProyectList(List<Proyect> proyects) {
        List<Proyect> proyectList = new ArrayList<>();
        for (Proyect proyect : proyects) {
            if (proyect.getType().equals("project")) {
                proyectList.add(proyect);
            }
        }
        return proyectList;
    }
}