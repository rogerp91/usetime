package co.usetime.domain.interactor.imp;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.TaskInteractor;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.executor.Executor;
import co.usetime.executor.Interactor;
import co.usetime.executor.MainThread;
import co.usetime.mvp.model.ProjectTasks;
import co.usetime.mvp.model.Tasks;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 19/02/2016.
 */
public class TaskInteractorImp implements Interactor, TaskInteractor {

    protected static String TAG = TaskInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private Callback callback;
    private boolean typeMethod;
    private String id;
    String token = "";

    @Inject
    HttpRestClient client;

    //@Inject
    //Repository<Tasks> repository;

    private boolean checkSize = false;
    private List<Tasks> tasksList = null;//obtener listado local

    @Inject
    public TaskInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
        //this.repository = repository;
        token = Prefs.getString(Constants.TOKEN, "");
    }

    @Override
    public void execute(boolean typeMethod, int id, Callback callBackAnt) {
        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }
        this.id = Integer.toString(id);
        this.typeMethod = typeMethod;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                if (Functions.isOnline()) {
                    getTask();
                } else {
                    notifyPetitionError(UIApp.getContext.getResources().getString(R.string.no_connection2));
                }
            }
        });
    }

    private void getTask() {
        Call<ProjectTasks> call = client.performProjectWithTasks(HttpRestClient.PREFIX + token, id);
        call.enqueue(new retrofit.Callback<ProjectTasks>() {
            @Override
            public void onResponse(Response<ProjectTasks> response, Retrofit retrofit) {
                validateCode(response);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, t.toString());
                notifyPetitionError();
            }
        });
    }

    /**
     * @param response respuesta de proyectos conm tareas
     */
    private void validateCode(Response<ProjectTasks> response) {
        switch (response.code()) {
            case 200:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                Log.d(TAG, String.valueOf(response.raw()));
                notifyPetitionSuccess(response.body().getTasks());
                break;
            case 400:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 401:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 403:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_403));
                break;
            case 404:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 500:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            default:
        }
    }

    private void notifyPetitionError(final String msg) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError2(msg);
            }
        });
    }

    private void notifyPetitionError() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError();
            }
        });
    }

    private void notifyPetitionErrorConnect() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnection();
            }
        });
    }

    private void notifyPetitionErrorContent() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContect();
            }
        });
    }

    private void notifyPetitionErrorConnectVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnectionVoid();
            }
        });
    }

    private void notifyPetitionErrorContentVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContectVoid();
            }
        });
    }

    private void notifyPetitionSuccess(final List<?> list) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(list);
            }
        });
    }

    private void notifyPetitionVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onVoid();
            }
        });
    }
}