package co.usetime.domain.interactor.imp;

import android.util.Log;

import com.google.gson.Gson;

import javax.inject.Inject;

import co.usetime.domain.interactor.interfaces.CommentInteractor;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.executor.Executor;
import co.usetime.executor.Interactor;
import co.usetime.executor.MainThread;
import co.usetime.mvp.model.CommentsParameters;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 09/06/2016.
 */
public class CommentInteractorImp implements Interactor, CommentInteractor {

    protected static String TAG = CommentInteractorImp.class.getSimpleName();
    private final Executor executor;
    private final MainThread mainThread;
    private CommentInteractor.Callback callback;
    private String token = "";
    private CommentsParameters commentsParameters;
    @Inject
    HttpRestClient client;

    @Inject
    public CommentInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
        token = Prefs.getString(Constants.TOKEN, "");
    }

    @Override
    public void execute(CommentsParameters commentsParameters, Callback callBackAnt) {
        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }
        this.callback = callBackAnt;
        this.commentsParameters = commentsParameters;
        executor.run(this);
    }

    @Override
    public void run() {
        post();
    }

    public void post() {
        Log.d(TAG, "task_id: " + commentsParameters.getTask_id());
        Log.d(TAG, new Gson().toJson(commentsParameters));
        client = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(true), Functions.provideOkHttpClient()));
        Call<Void> call = client.perfomComments(HttpRestClient.PREFIX + token, commentsParameters);
        call.enqueue(new retrofit.Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response, Retrofit retrofit) {
                System.out.println(TAG + " " + response.raw());
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionSuccess();
                        break;
                    case 201:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionSuccess();
                        break;
                    case 400:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError();
                        break;
                    case 401:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError();
                        break;
                    case 403:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError();
                        break;
                    case 404:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError();
                        break;
                    case 500:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionError();
                        break;
                    default:
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, t.toString());
                notifyPetitionError();
            }
        });
    }


    private void notifyPetitionError() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError();
            }
        });
    }

    private void notifyPetitionSuccess() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess();
            }
        });
    }

}