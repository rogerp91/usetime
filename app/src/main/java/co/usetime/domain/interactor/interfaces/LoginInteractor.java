package co.usetime.domain.interactor.interfaces;

import co.usetime.mvp.model.Profile;

/**
 * Created by Roger Patiño on 16/12/2015.
 */
public interface LoginInteractor {

    interface Callback {

        void onSuccess(Profile profile);

        void onError(String msgError);

        void onErrorData();

        void onErrorProfile();
    }

    void execute(String user, String password, Callback callBackAnt);

}