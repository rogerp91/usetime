package co.usetime.domain.interactor.interfaces;

import java.util.List;

import co.usetime.mvp.model.DocumentFile;
import co.usetime.mvp.model.Invistes;

/**
 * Created by Roger Patiño on 06/06/2016.
 */
public interface InvitesAndDocumentInteractor {

    interface Callback {

        void onSuccess(final List<DocumentFile> documentFiles, final List<Invistes> invistes);

        void onVoid();

        void onErrorConexion();

        void onErrorConexionVoid();

        void onErrorContenido();

        void onErrorContenidoVoid();

        void onError();

        void onMsg(String msg, int color);
    }

    void execute(boolean typeMethod, int id, Callback callBackAnt);

}