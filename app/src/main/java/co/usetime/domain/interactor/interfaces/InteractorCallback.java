package co.usetime.domain.interactor.interfaces;

import java.util.List;

/**
 * Created by Roger Patiño on 17/02/2016.
 */
public interface InteractorCallback {

    interface Callback {

        void onSuccess(final List<?> listObjects);

        void onVoid();

        void onErrorConnection();

        void onErrorConnectionVoid();

        void onErrorContect();

        void onErrorContectVoid();

        void onError();

        void onError2(String msg);

        void onMsg(String msg, int color);
    }

    void execute(boolean typeMethod, int id, Callback callBackAnt);
}