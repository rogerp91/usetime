package co.usetime.domain.interactor.interfaces;

import co.usetime.mvp.model.Todo;
import co.usetime.mvp.model.TodoSimple;

/**
 * Created by Roger Patiño on 25/04/2016.
 */
public interface ChangeTaskTodoInteractor {

    interface Callback {

        void onSuccess(TodoSimple simple, String id, int status);

        void onVoid();

        void onError(final String error);
    }

    void execute(String id, int status, Callback callBackAnt);

}