package co.usetime.domain.interactor.interfaces;

import java.util.List;

/**
 * Created by Roger Patiño on 06/06/2016.
 */
public interface InvitesInteractor {

    interface Callback {

        void onSuccess(final List<?> listObjects);

        void onVoid();

        void onErrorConexion();

        void onErrorConexionVoid();

        void onErrorContenido();

        void onErrorContenidoVoid();

        void onError();

        void onMsg(String msg, int color);
    }

    void execute(boolean typeMethod, int id, Callback callBackAnt);

}