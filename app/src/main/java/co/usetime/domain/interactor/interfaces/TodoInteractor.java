package co.usetime.domain.interactor.interfaces;

import co.usetime.mvp.model.Todo;

/**
 * Created by Roger Patiño on 02/03/2016.
 */
public interface TodoInteractor {

    interface Callback {

        void onSuccess(final Todo objects);

        void onVoid();

        void onErrorConnection();

        void onErrorConnectionVoid();

        void onErrorContect();

        void onErrorContectVoid();

        void onError(final String error);

    }

    void execute(boolean typeMethod, int id, Callback callBackAnt);

}