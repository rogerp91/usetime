package co.usetime.domain.interactor.imp;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import co.usetime.domain.interactor.interfaces.PeddingInteractor;
import co.usetime.domain.interactor.interfaces.TodoInteractor;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.domain.repository.interfaces.TodoRepository;
import co.usetime.executor.Executor;
import co.usetime.executor.Interactor;
import co.usetime.executor.MainThread;
import co.usetime.mvp.model.Pending;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 02/03/2016.
 */
public class PeddingInteractorImp implements Interactor, PeddingInteractor {
    protected static String TAG = TodoInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private Callback callback;
    private boolean typeMethod;
    private int id;
    String token = "";
    private boolean checkSize = false;

    @Inject
    HttpRestClient client;

    @Inject
    TodoRepository repository;

    @Inject
    public PeddingInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
        token = Prefs.getString(Constants.TOKEN, "");
    }

    @Override
    public void execute(boolean typeMethod, int id, Callback callBackAnt) {
        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }
        this.id = id;
        this.typeMethod = typeMethod;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                getPedding();
            }
        });
    }

    private synchronized void getPedding() {
    }

    private synchronized void notifyPetitionError(final String error) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(error);
            }
        });
    }

    private synchronized void notifyPetitionErrorConnect() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnection();
            }
        });
    }

    private synchronized void notifyPetitionErrorContent() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContect();
            }
        });
    }

    private void notifyPetitionErrorConnectVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnectionVoid();
            }
        });
    }

    private synchronized void notifyPetitionErrorContentVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContectVoid();
            }
        });
    }

    private synchronized void notifyPetitionSuccess(final List<Pending> objects) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(objects);
            }
        });
    }

    private synchronized void notifyPetitionVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onVoid();
            }
        });
    }
}