package co.usetime.domain.interactor.interfaces;

import co.usetime.mvp.model.CommentsParameters;

/**
 * Created by Roger Patiño on 09/06/2016.
 */
public interface CommentInteractor {

    interface Callback {

        void onSuccess();

        void onError();

    }

    void execute(CommentsParameters commentsParameters, Callback callBackAnt);

}