package co.usetime.domain.interactor.imp;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.InvitesAndDocumentInteractor;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.executor.Executor;
import co.usetime.executor.Interactor;
import co.usetime.executor.MainThread;
import co.usetime.mvp.model.DocumentFile;
import co.usetime.mvp.model.Invistes;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 06/06/2016.
 */
public class InvitesAndDocumentInteractorImp implements Interactor, InvitesAndDocumentInteractor {
    protected static String TAG = InvitesInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private InvitesAndDocumentInteractor.Callback callback;
    private boolean typeMethod;
    private String token = "";
    private String id;

    @Inject
    HttpRestClient client;

    private List<DocumentFile> documentFiles;

    @Inject
    public InvitesAndDocumentInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
        token = Prefs.getString(Constants.TOKEN, "");
    }

    @Override
    public void execute(boolean typeMethod, int id, Callback callBackAnt) {
        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }
        this.id = Integer.toString(id);
        this.typeMethod = typeMethod;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                if (Functions.isOnline()) {
                    getDocument();
                } else {
                    notifyPetitionError(UIApp.getContext.getResources().getString(R.string.no_connection2));
                }
            }
        });
    }

    private void getDocument() {
        Log.d(TAG, "getDocument");
        Call<List<DocumentFile>> call = client.perfomDocument(HttpRestClient.PREFIX + token, id);
        call.enqueue(new retrofit.Callback<List<DocumentFile>>() {
            @Override
            public void onResponse(Response<List<DocumentFile>> response, Retrofit retrofit) {
                validateCodeDocument(response);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, t.toString());
                notifyPetitionError();
            }
        });
    }

    private void validateCodeDocument(Response<List<DocumentFile>> response) {
        switch (response.code()) {
            case 200:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                Log.d(TAG, String.valueOf(response.raw()));
                documentFiles = response.body();
                getInvites();
                break;
            case 400:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 401:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 403:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_403));
                break;
            case 404:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 500:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            default:
        }
    }


    private void getInvites() {
        Log.d(TAG, "getInvites");
        Call<List<Invistes>> call = client.perfomInvite(HttpRestClient.PREFIX + token, id);
        call.enqueue(new retrofit.Callback<List<Invistes>>() {
            @Override
            public void onResponse(Response<List<Invistes>> response, Retrofit retrofit) {
                validateCodeInvites(response);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, t.toString());
                notifyPetitionError();
            }
        });
    }

    /**
     * @param response respuesta de proyectos conm tareas
     */
    private void validateCodeInvites(Response<List<Invistes>> response) {
        switch (response.code()) {
            case 200:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                Log.d(TAG, String.valueOf(response.raw()));
                notifyPetitionSuccess(documentFiles, response.body());
                break;
            case 400:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 401:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 403:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_403));
                break;
            case 404:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            case 500:
                Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                notifyPetitionError(UIApp.getContext.getResources().getString(R.string.msg_error_many));
                break;
            default:
        }
    }

    private void notifyPetitionError(final String msg) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                //callback.onMsg(msg);
            }
        });
    }


    private void notifyPetitionError() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError();
            }
        });
    }

    private void notifyPetitionErrorConnect() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConexion();
            }
        });
    }

    private void notifyPetitionErrorConnectVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConexionVoid();
            }
        });
    }

    private void notifyPetitionErrorContent() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContenido();
            }
        });
    }

    private void notifyPetitionErrorContentVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContenidoVoid();
            }
        });
    }

    private void notifyPetitionSuccess(final List<DocumentFile> documentFiles, final List<Invistes> invistes) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                //Prefs.putBoolean("IS_CHECK_GET", true);
                callback.onSuccess(documentFiles, invistes);
            }
        });
    }

    private void notifyPetitionVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onVoid();
            }
        });
    }
}