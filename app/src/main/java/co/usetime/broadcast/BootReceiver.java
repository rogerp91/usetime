package co.usetime.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import co.usetime.services.LocationService;
import co.usetime.services.ProcessService;
import co.usetime.services.TrafficService;
import co.usetime.services.UploadProcessService;
import co.usetime.utils.Constants;
import co.usetime.utils.Prefs;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            boolean session = Prefs.getBoolean(Constants.SESSION, false);
            if (session) {
                LocationService.actionStart(context);
                ProcessService.actionStart(context);
                UploadProcessService.actionStart(context);
                TrafficService.actionStart();
            }
        } else {
            Toast.makeText(context, "not in ACTION_BOOT_COMPLETED", Toast.LENGTH_SHORT).show();
        }
    }
}