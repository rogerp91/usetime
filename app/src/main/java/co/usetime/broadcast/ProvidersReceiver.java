package co.usetime.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import co.usetime.services.LocationService;

/**
 * Created by Roger Patiño on 25/02/2016.
 */
public class ProvidersReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            Toast.makeText(context, "in PROVIDERS_CHANGED", Toast.LENGTH_SHORT).show();
            LocationService.actionStart(context);
        } else {
            Toast.makeText(context, "not in PROVIDERS_CHANGED", Toast.LENGTH_SHORT).show();
        }
    }

}