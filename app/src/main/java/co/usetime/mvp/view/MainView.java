package co.usetime.mvp.view;

import android.view.View;

/**
 * Created by Roger Patiño on 21/12/2015.
 */
public interface MainView extends GetView {

    //metodo que necesitar par aenviar mensaje a la vista
    View getView();
}
