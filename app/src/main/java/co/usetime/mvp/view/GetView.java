package co.usetime.mvp.view;

import android.view.View;

public interface GetView {

    abstract View getView();

}
