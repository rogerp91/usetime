package co.usetime.mvp.view;

import android.app.Activity;
import android.content.Intent;

import java.util.List;

import co.usetime.mvp.model.Proyect;

/**
 * Created by Roger Patiño on 07/01/2016.
 */
public interface ProyectView extends GetView {

    //muetra algun error
    void displayError();

    void displayErrorRefresh();

    //muestra una vista
    void showView(final boolean show);

    //muestras un mesaje snarback
    void showMsg(String mgs, int color);

    //asigna los valores
    void adapteList(List<Proyect> list, boolean type);

    //lanza una actividad
    void gotoMain(Intent i);

    Activity getActivity();

    //muestra el progress
    void showViewProgress(final boolean show);

    //muestras el refresh
    void showViewRefresh(final boolean show);

    //muesta o quita el refresh cuando van a buscar algo al servidror
    void setRefreshing(boolean refreshing);

    //muestra este error cuando no hay nada en la base de dato, va a buscar datos al servidor y no hay conex
    void displayLayoutConnection(final boolean connection);

    //muestra esto cuando no hay nada en la base de dato y hubo un error
    void displayLayoutNocontent(final boolean content);

    //muestra esto cuando no hay nada en la base de dato y hubo un error
    void displayLayoutVoid(final boolean empty);

    boolean isAddedFragment();

}
