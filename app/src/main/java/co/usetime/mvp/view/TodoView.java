package co.usetime.mvp.view;

import android.app.Activity;

import co.usetime.mvp.model.Todo;
import co.usetime.mvp.model.TodoSimple;

/**
 * Created by Roger Patiño on 14/03/2016.
 */
public interface TodoView extends GetView {

    void showMsg(String mgs, int color);

    void adapteLists(Todo todo, boolean type);

    void adapteSimple(TodoSimple simple, String id, int status);

    Activity getActivity();

    void showRefresh(final boolean show);

    void showViewProgress(final boolean show);

    void setRefreshing(boolean refreshing);

    //muestra esto cuando no hay nada en la base de dato y hubo un error
    void displayLayoutVoid(final boolean empty);

    void displayLayoutConnection(final boolean connection);

    void displayLayoutNocontent(final boolean content);

    void displayLayoutError(final boolean status, final String msg);

    void onError(String msg);

    boolean isAddedFragment();

}