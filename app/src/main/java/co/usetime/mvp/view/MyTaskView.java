package co.usetime.mvp.view;

import java.util.List;

import co.usetime.mvp.model.Tasks;
import co.usetime.mvp.model.TasksOfProject;

/**
 * Created by Roger Patiño on 19/02/2016.
 */
public interface MyTaskView extends GetView {

    void showMsg(String mgs, int color);

    void adapteList(List<TasksOfProject> list);

    void showViewProgress(boolean show);

    void displayLayoutConnection(boolean connection);

    void displayLayoutNocontent(boolean content);

    void displayLayoutNoTask(boolean task);

    void displayError();

    void displayLayoutError(final boolean status, final String msg);
}