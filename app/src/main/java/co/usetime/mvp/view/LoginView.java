package co.usetime.mvp.view;

import android.content.Intent;
import android.view.View;

import co.usetime.mvp.model.Profile;

/**
 * Created by Roger Patiño on 15/12/2015.
 */
public interface LoginView extends GetView {

    //metodo que necesitar par aenviar mensaje a la vista
    View getView();

    void onSuccess(Profile profile);

    //progress_white y form
    void showView(final boolean show);

    void displayMsg(String msg, int color);

    //enviar el intent
    void gotoMain(Intent i);

    //error que sucede durante la llamanda del interactor o error
    void displayErrorData();

    void displayError();

    void displayErrorConnection();

    //mensaje que se muestra a validar datos
    void displayInfoUser();

    void displayInfoUserSize();

    void displayInfoPassword();

    void displayInfoPasswordSize();

    void displayInfoProfile();
}
