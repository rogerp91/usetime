package co.usetime.mvp.view;

import java.util.List;

import co.usetime.mvp.model.Tasks;

/**
 * Created by Roger Patiño on 19/02/2016.
 */
public interface TaskView extends GetView {

    void showMsg(String mgs, int color);

    void adapteList(List<Tasks> list);

    void showViewProgress(boolean show);

    void displayLayoutConnection(boolean connection);

    void displayLayoutNocontent(boolean content);

    void displayLayoutNoTask(boolean task);

    void displayError();

    void displayLayoutError(final boolean status, final String msg);
}