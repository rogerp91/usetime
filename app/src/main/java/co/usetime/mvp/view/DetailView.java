package co.usetime.mvp.view;

import android.content.Intent;
import android.net.Uri;

import java.util.List;

import co.usetime.mvp.model.DocumentFile;
import co.usetime.mvp.model.Invistes;

/**
 * Created by Roger Patiño on 06/06/2016.
 */
public interface DetailView extends GetView {

    void adapteList(List<DocumentFile> documentFiles);

    void showMsg(String mgs, int color);

    void showViewProgress(boolean show);

    void displayLayoutConnection(boolean connection);

    void displayLayoutNocontent(boolean content);

    void displayLayoutNoTask(boolean task);

    void displayError();

    void displayLayoutError(final boolean status, final String msg);

    void showViewRefresh(final boolean show);

    void setRefreshing(boolean refreshing);

    void showProgressComment(final boolean show);

    void activityForResult(Intent intent, int code, Uri fileUr);

    void user1000(List<Invistes> invistes);

    void user6(List<Invistes> invistes);

    void user5(List<Invistes> invistes);

    void user4(List<Invistes> invistes);

    void user3(List<Invistes> invistes);

    void user2(List<Invistes> invistes);

    void user1(List<Invistes> invistes);

    void commentOnSuccess();

}