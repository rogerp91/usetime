package co.usetime.mvp.presenter.interfaces;

import co.usetime.mvp.view.DetailView;

/**
 * Created by Roger Patiño on 08/06/2016.
 */
public interface InvistesDocumentPresenter {

    void setView(DetailView view);

    void getListInvite(String id);

    void getListListInviteRefresh(String id);

    void postComment(String comment, String id);

    void eventCapture(String id);

}