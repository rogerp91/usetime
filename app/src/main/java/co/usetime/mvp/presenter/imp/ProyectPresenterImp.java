package co.usetime.mvp.presenter.imp;

import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.InteractorCallback;
import co.usetime.domain.interactor.interfaces.ProyectInteractor;
import co.usetime.mvp.model.Proyect;
import co.usetime.mvp.presenter.interfaces.ProyectPresenter;
import co.usetime.mvp.view.ProyectView;
import co.usetime.utils.Functions;

/**
 * Created by Roger Patiño on 07/01/2016.
 */
public class ProyectPresenterImp implements ProyectPresenter {

    protected static String TAG = ProyectPresenterImp.class.getSimpleName();
    ProyectView view;
    ProyectInteractor interactor;

    @Inject
    public ProyectPresenterImp(ProyectInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void setView(Object view) {
        if (view == null) {
            Log.e(TAG, "ViewOb must not be null!");
            throw new IllegalArgumentException("ViewOb must not be null!");
        }
        this.view = (ProyectView) view;
    }

    @Override
    public void getList() {
        view.displayLayoutConnection(false);
        view.displayLayoutNocontent(false);
        view.showViewRefresh(false);//refesh
        view.showViewProgress(true);//cargando
        interactor.execute(true, 0, new InteractorCallback.Callback() {
            @Override
            public void onSuccess(List<?> listObjects) {
                if (view.isAddedFragment() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(true);
                    view.adapteList((List<Proyect>) listObjects, true);
                }
            }

            @Override
            public void onVoid() {
                view.showViewProgress(false);
            }

            @Override
            public void onErrorConnection() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorConnectionVoid() {
                if (view.isAddedFragment() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(true);
                    view.displayLayoutConnection(true);
                }
            }

            @Override
            public void onErrorContect() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContectVoid() {
                Log.d(TAG, "onErrorContectVoid");
                if (view.isAddedFragment() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(true);
                    view.displayLayoutNocontent(true);
                }
            }

            @Override
            public void onError() {
                if (view.isAddedFragment() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(false);
                    view.displayError();
                }
            }

            @Override
            public void onError2(String msg) {

            }

            @Override
            public void onMsg(String msg, int color) {

            }

        });

    }

    @Override
    public void getListRefresh() {
        interactor.execute(false, 0, new ProyectInteractor.Callback() {
            @Override
            public void onSuccess(List<?> listObjects) {
                Log.d(TAG, "getListRefresh - onSuccess");
                if (view.isAddedFragment() && view.getActivity() != null) {
                    view.setRefreshing(false);
                    view.showViewRefresh(true);
                    view.adapteList((List<Proyect>) listObjects, false);
                }
            }

            @Override
            public void onVoid() {

            }

            @Override
            public void onErrorConnection() {
                view.setRefreshing(false);
                view.showViewRefresh(true);
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContect() {
                view.setRefreshing(false);
                view.showViewRefresh(true);
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorConnectionVoid() {
                view.setRefreshing(false);
                view.showViewRefresh(true);
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContectVoid() {
                view.setRefreshing(false);
                view.showViewRefresh(true);
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onError() {
                if (view.isAddedFragment() && view.getActivity() != null) {
                    view.showViewRefresh(false);
                    view.setRefreshing(false);
                    view.displayError();
                }
            }

            @Override
            public void onError2(String msg) {

            }

            @Override
            public void onMsg(String msg, int color) {
                view.setRefreshing(false);
                view.showViewRefresh(true);
            }
        });
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

}