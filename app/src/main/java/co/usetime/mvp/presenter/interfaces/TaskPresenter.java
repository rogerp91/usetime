package co.usetime.mvp.presenter.interfaces;

/**
 * Created by Roger Patiño on 19/02/2016.
 */
public interface TaskPresenter extends Presenter {
    /**
     * void getName(String id, Class aClass);
     * <p/>
     * void getList(String id, Class aClass);
     * <p/>
     * void getTask(String id);
     */

    void getList(String id);

    void getListRefresh(String id);

    void getTask(String id);

}