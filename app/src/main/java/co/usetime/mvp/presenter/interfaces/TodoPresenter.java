package co.usetime.mvp.presenter.interfaces;

import co.usetime.mvp.view.TodoView;

/**
 * Created by Roger Patiño on 14/03/2016.
 */
public interface TodoPresenter {

    void setView(TodoView view, int type);

    void getList();

    void getListRefresh();

    void onDestroy();

    void onPause();

    void onResume();

    void getChange(String id, int status);


}