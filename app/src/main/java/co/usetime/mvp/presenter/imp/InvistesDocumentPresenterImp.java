package co.usetime.mvp.presenter.imp;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.CommentInteractor;
import co.usetime.domain.interactor.interfaces.InvitesAndDocumentInteractor;
import co.usetime.mvp.model.CommentsParameters;
import co.usetime.mvp.model.DocumentFile;
import co.usetime.mvp.model.Invistes;
import co.usetime.mvp.presenter.interfaces.InvistesDocumentPresenter;
import co.usetime.mvp.view.DetailView;
import co.usetime.utils.Constants;

/**
 * Created by Roger Patiño on 08/06/2016.
 */
public class InvistesDocumentPresenterImp implements InvistesDocumentPresenter {

    protected static String TAG = InvistesDocumentPresenterImp.class.getSimpleName();

    //para tomar foto
    public static final int MEDIA_TYPE_IMAGE = 1;
    private Uri fileUri; // file url to store image/video
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    //inject
    DetailView view;
    InvitesAndDocumentInteractor interactor;
    CommentInteractor commentInteractor;

    @Inject
    public InvistesDocumentPresenterImp(InvitesAndDocumentInteractor interactor, CommentInteractor commentInteractor) {
        this.interactor = interactor;
        this.commentInteractor = commentInteractor;
    }

    @Override
    public void setView(DetailView view) {
        if (view == null) {
            Log.e(TAG, "DetailView must not be null!");
            throw new IllegalArgumentException("ViewOb must not be null!");
        }
        this.view = view;
    }

    @Override
    public void getListInvite(String id) {
        view.showViewProgress(true);
        interactor.execute(true, Integer.parseInt(id), new InvitesAndDocumentInteractor.Callback() {
            @Override
            public void onSuccess(List<DocumentFile> documentFiles, List<Invistes> invistes) {
                selectUser(invistes);
                view.adapteList(documentFiles);
                view.showViewProgress(false);
            }

            @Override
            public void onVoid() {
                view.showViewProgress(false);
            }

            @Override
            public void onErrorConexion() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorConexionVoid() {
                view.showViewProgress(false);
                view.displayLayoutConnection(true);
            }

            @Override
            public void onErrorContenido() {
                view.showViewProgress(false);
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContenidoVoid() {
                view.showViewProgress(false);
                view.displayLayoutNocontent(true);
            }

            @Override
            public void onError() {
                view.showViewProgress(false);
            }

            @Override
            public void onMsg(String msg, int color) {
                view.showViewProgress(false);
            }
        });
    }

    /**
     * escoger la cantida de usaruio en ivitados
     *
     * @param invistes
     */
    private void selectUser(List<Invistes> invistes) {
        if (invistes.size() >= 1000) {
            view.user1000(invistes);
        } else {
            if (invistes.size() > 5) {
                view.user6(invistes);
            } else {
                if (invistes.size() == 5) {
                    view.user5(invistes);
                } else {
                    if (invistes.size() == 4) {
                        view.user4(invistes);
                    } else {
                        if (invistes.size() == 3) {
                            view.user3(invistes);
                        } else {
                            if (invistes.size() == 2) {
                                view.user2(invistes);
                            } else {
                                view.user1(invistes);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void getListListInviteRefresh(String id) {

    }

    @Override
    public void postComment(String comment, String id) {
        if (comment.length() != 0) {
            postComment(new CommentsParameters(comment, id));
        } else {
            view.showMsg("Debe colocar algun comentario", ContextCompat.getColor(UIApp.getContext, R.color.info));
        }
    }

    /**
     * metodo usado por postComment para enviar el comentario a poolthr en interactor
     *
     * @param commentsParameters objecto que tiene comentario y el pos o id de tarea
     */
    private void postComment(CommentsParameters commentsParameters) {
        view.showProgressComment(true);
        commentInteractor.execute(commentsParameters, new CommentInteractor.Callback() {
            @Override
            public void onSuccess() {
                view.showProgressComment(false);
                view.commentOnSuccess();
                view.showMsg("Comentario enviado...", ContextCompat.getColor(UIApp.getContext, R.color.success));
            }

            @Override
            public void onError() {
                view.showProgressComment(false);
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }
        });

    }

    @Override
    public void eventCapture(String id) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = Uri.fromFile(getOutputMediaFile(MEDIA_TYPE_IMAGE, id));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        view.activityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE, fileUri);
    }

    /**
     * @param type tipo de recurso
     * @param id   id de la tarea
     * @return
     */
    private File getOutputMediaFile(int type, String id) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String nameCapture = id + "_IMG_" + timeStamp;
        Log.d(TAG, nameCapture);
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Constants.PATH_BASE);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Fallo a crear  directorio" + Constants.PATH_BASE + " directory");
                return null;
            }
        }
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + nameCapture + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }
}