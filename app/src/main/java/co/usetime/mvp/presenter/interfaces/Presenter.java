package co.usetime.mvp.presenter.interfaces;

/**
 * Created by Roger Patiño on 17/02/2016.
 */
public interface Presenter<T> {

    void setView(T view);

    void getList();

    void getListRefresh();

    void onDestroy();

    void onPause();

    void onResume();

}