package co.usetime.mvp.presenter.imp;

import android.support.v4.content.ContextCompat;
import android.util.Log;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.ChangeTaskTodoInteractor;
import co.usetime.domain.interactor.interfaces.TodoInteractor;
import co.usetime.mvp.model.Todo;
import co.usetime.mvp.model.TodoSimple;
import co.usetime.mvp.presenter.interfaces.TodoPresenter;
import co.usetime.mvp.view.TodoView;
import co.usetime.utils.Functions;

/**
 * Created by Roger Patiño on 14/03/2016.
 */
public class TodoPresenterImp implements TodoPresenter {

    protected static String TAG = TodoPresenterImp.class.getSimpleName();

    TodoView view;
    private TodoInteractor todoInteractor;
    private ChangeTaskTodoInteractor changeTaskTodoInteractor;
    private int type;

    @Inject
    public TodoPresenterImp(TodoInteractor interactor, ChangeTaskTodoInteractor changeTaskTodoInteractor) {
        this.todoInteractor = interactor;
        this.changeTaskTodoInteractor = changeTaskTodoInteractor;
    }

    @Override
    public void setView(TodoView view, int type) {
        if (view == null) {
            throw new IllegalArgumentException("View must not be null!");
        }
        this.type = type;
        this.view = view;
    }

    @Override
    public void getList() {
        if (Functions.isOnline()) {
            view.displayLayoutError(false, "");
            view.displayLayoutConnection(false);
            view.displayLayoutNocontent(false);
            view.showViewProgress(true);//cargando
            todoInteractor.execute(true, type, new TodoInteractor.Callback() {
                @Override
                public void onSuccess(Todo todo) {
                    Log.d(TAG, "onSuccess");
                    if (view.isAddedFragment()) {
                        view.displayLayoutError(false, "");
                        view.showViewProgress(false);
                        view.adapteLists(todo, true);
                    }
                }

                @Override
                public void onVoid() {
                    view.showViewProgress(false);
                }

                @Override
                public void onErrorConnection() {
                    if (view.isAddedFragment() && view.getActivity() != null) {
                        view.showRefresh(false);
                    }
                }

                @Override
                public void onErrorConnectionVoid() {
                    if (view.isAddedFragment() && view.getActivity() != null) {
                        view.showRefresh(false);
                    }
                }

                @Override
                public void onErrorContect() {
                    if (view.isAddedFragment() && view.getActivity() != null) {
                        view.showRefresh(false);
                    }
                }

                @Override
                public void onErrorContectVoid() {
                    if (view.isAddedFragment() && view.getActivity() != null) {
                        view.showRefresh(false);
                    }
                }

                @Override
                public void onError(String error) {
                    view.showViewProgress(false);
                    view.displayLayoutError(true, error);
                    //view.showMsg(error, ContextCompat.getColor(UIApp.getContext, R.color.alert));

                }
            });
        } else {
            view.showMsg("No hay conexion a internet", ContextCompat.getColor(UIApp.getContext, R.color.alert));
        }
    }

    @Override
    public void getListRefresh() {
        if (Functions.isOnline()) {
            todoInteractor.execute(false, type, new TodoInteractor.Callback() {
                @Override
                public void onSuccess(Todo todo) {
                    Log.d(TAG, "onSuccess");
                    if (view.isAddedFragment()) {
                        view.showRefresh(false);
                        view.adapteLists(todo, false);
                    }
                }

                @Override
                public void onVoid() {

                }

                @Override
                public void onErrorConnection() {
                    if (view.isAddedFragment() && view.getActivity() != null) {
                        view.showRefresh(false);
                    }
                }

                @Override
                public void onErrorConnectionVoid() {
                    if (view.isAddedFragment() && view.getActivity() != null) {
                        view.showRefresh(false);
                    }
                }

                @Override
                public void onErrorContect() {
                    if (view.isAddedFragment() && view.getActivity() != null) {
                        view.showRefresh(false);
                    }
                }

                @Override
                public void onErrorContectVoid() {
                    if (view.isAddedFragment() && view.getActivity() != null) {
                        view.showRefresh(false);
                    }
                }

                @Override
                public void onError(String error) {
                    view.showMsg(error, ContextCompat.getColor(UIApp.getContext, R.color.alert));

                }
            });
        } else {
            view.showMsg("No hay conexion a internet", ContextCompat.getColor(UIApp.getContext, R.color.alert));
        }
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void getChange(String id, int status) {
        if (Functions.isOnline()) {
            changeTaskTodoInteractor.execute(id, status, new ChangeTaskTodoInteractor.Callback() {

                @Override
                public void onSuccess(TodoSimple simple, String id, int status) {
                    view.adapteSimple(simple, id, status);
                }

                @Override
                public void onVoid() {

                }

                @Override
                public void onError(String error) {
                    view.onError(error);
                }
            });
        } else {
            view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
        }
    }
}