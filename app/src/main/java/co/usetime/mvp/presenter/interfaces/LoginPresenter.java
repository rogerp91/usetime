package co.usetime.mvp.presenter.interfaces;

/**
 * Created by Roger Patiño on 15/12/2015.
 */
public interface LoginPresenter extends Presenter {

    void validateCredentials(String user, String password);

    void onResume();

    void onPause();

}
