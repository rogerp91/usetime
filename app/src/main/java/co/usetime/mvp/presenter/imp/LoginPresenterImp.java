package co.usetime.mvp.presenter.imp;

import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.LoginInteractor;
import co.usetime.mvp.model.Profile;
import co.usetime.mvp.presenter.interfaces.LoginPresenter;
import co.usetime.mvp.view.LoginView;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;

/**
 * Created by Roger Patiño on 15/12/2015.
 */
public class LoginPresenterImp implements LoginPresenter {
    protected static String TAG = LoginPresenterImp.class.getSimpleName();

    //inject
    LoginView view;
    LoginInteractor interactor;

    @Inject
    public LoginPresenterImp(LoginInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void setView(Object view) {
        if (view == null) {
            Log.e(TAG, "ViewOb must not be null!");
            throw new IllegalArgumentException("ViewOb must not be null!");
        }
        this.view = (LoginView) view;
    }

    @Override
    public void validateCredentials(String user, String password) {
        //verificar que no hay error
        boolean cancel = false;
        //validate datos
        if (TextUtils.isEmpty(password)) {
            cancel = true;
            this.view.displayInfoPassword();
        } else {
            if (!(password.length() > 3)) {
                cancel = true;
                view.displayInfoPasswordSize();
            }
        }

        if (TextUtils.isEmpty(user)) {
            cancel = true;
            this.view.displayInfoUser();
        } else {
            if (!(user.length() > 3)) {
                cancel = true;
                this.view.displayInfoUserSize();
            }
        }
        if (!cancel) {
            if (Functions.isOnline()) {
                view.showView(true);//quitar el progress
                postLogin(user, password);
            } else {
                view.showView(false);//quitar el progress
                view.displayErrorConnection();
            }
        }
    }

    private void postLogin(String user, String password) {
        interactor.execute(user, password, new LoginInteractor.Callback() {

            @Override
            public void onSuccess(Profile profile) {
                Prefs.putBoolean(Constants.SESSION, true);
                Prefs.putBoolean(Constants.LOGIN, true);
                view.onSuccess(profile);
            }

            @Override
            public void onError(String msgError) {
                view.showView(false);//colocar el progress de cargando
                view.displayMsg(msgError, ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorData() {
                view.showView(false);//colocar el progress de cargando
                view.displayErrorData();
            }

            @Override
            public void onErrorProfile() {
                view.showView(false);//colocar el progress de cargando
                view.displayInfoProfile();
            }
        });
    }

    @Override
    public void getList() {

    }

    @Override
    public void getListRefresh() {

    }


    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }
}