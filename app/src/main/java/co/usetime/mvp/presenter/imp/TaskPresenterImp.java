package co.usetime.mvp.presenter.imp;

import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.interactor.interfaces.InteractorCallback;
import co.usetime.domain.interactor.interfaces.TaskInteractor;
import co.usetime.mvp.model.TasksOfProject;
import co.usetime.mvp.presenter.interfaces.TaskPresenter;
import co.usetime.mvp.view.MyTaskView;

/**
 * Created by Roger Patiño on 19/02/2016.
 */
public class TaskPresenterImp implements TaskPresenter {

    protected static String TAG = TaskPresenterImp.class.getSimpleName();
    MyTaskView view;

    //@Inject
    //Repository<Tasks> tasksRepository;
    //@Inject
    //Repository<Proyect> proyectRepository;

    TaskInteractor interactor;

    @Inject
    public TaskPresenterImp(TaskInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void setView(Object view) {
        if (view == null) {
            Log.e(TAG, "ViewOb must not be null!");
            throw new IllegalArgumentException("ViewOb must not be null!");
        }
        this.view = (MyTaskView) view;
    }

    @Override
    public void getList() {

    }

    @Override
    public void getListRefresh() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void getList(String id) {
        this.view.showViewProgress(true);
        interactor.execute(true, Integer.parseInt(id), new InteractorCallback.Callback() {
            @Override
            public void onSuccess(List<?> listObjects) {
                view.showViewProgress(false);
                view.adapteList((List<TasksOfProject>) listObjects);
            }

            @Override
            public void onVoid() {
                view.showViewProgress(false);
                view.displayLayoutNoTask(true);
            }

            @Override
            public void onErrorConnection() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorConnectionVoid() {
                view.showViewProgress(false);
                view.displayLayoutConnection(true);
            }

            @Override
            public void onErrorContect() {
                view.showViewProgress(false);
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContectVoid() {
                view.showViewProgress(false);
                view.displayLayoutNocontent(true);
            }

            @Override
            public void onError() {
                view.showViewProgress(false);
                view.showViewProgress(false);
            }

            @Override
            public void onError2(String msg) {
                view.showViewProgress(false);
                view.showMsg(msg, ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onMsg(String msg, int color) {
                view.showViewProgress(false);
            }
        });
    }

    @Override
    public void getListRefresh(String id) {
        interactor.execute(false, Integer.parseInt(id), new InteractorCallback.Callback() {
            @Override
            public void onSuccess(List<?> listObjects) {

            }

            @Override
            public void onVoid() {

            }

            @Override
            public void onErrorConnection() {

            }

            @Override
            public void onErrorConnectionVoid() {

            }

            @Override
            public void onErrorContect() {

            }

            @Override
            public void onErrorContectVoid() {

            }

            @Override
            public void onError() {

            }

            @Override
            public void onError2(String msg) {
                view.showMsg(msg, ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onMsg(String msg, int color) {

            }
        });
    }

    @Override
    public void getTask(String id) {

    }
}