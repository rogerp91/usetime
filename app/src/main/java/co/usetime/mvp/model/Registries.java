package co.usetime.mvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 09/05/2016.
 */
public class Registries {

    @SerializedName("application")
    private String application;

    @SerializedName("path")
    private String path;

    @SerializedName("caption")
    private String caption;

    @SerializedName("timestamp")
    private String timestamp;

    @SerializedName("datetime")
    private String datetime;

    @SerializedName("duration")
    private String duration;

    @SerializedName("keystrokes")
    private int keystrokes;

    @SerializedName("mouse")
    private int mouse;

    @SerializedName("type")
    private int type;

    @SerializedName("task_id")
    private int task_id;

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getKeystrokes() {
        return keystrokes;
    }

    public void setKeystrokes(Integer keystrokes) {
        this.keystrokes = keystrokes;
    }

    public Integer getMouse() {
        return mouse;
    }

    public void setMouse(Integer mouse) {
        this.mouse = mouse;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public int getTaskId() {
        return task_id;
    }

    public void setTaskId(int taskId) {
        this.task_id = taskId;
    }
}