package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 01/04/2016.
 */
public class Comments {

    private String task_id;
    private String comment;
    private CreatedAt created_at;
    private String user_id;
    private int id;

    public CreatedAt getCreated_at() {
        return created_at;
    }

    public void setCreated_at(CreatedAt created_at) {
        this.created_at = created_at;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}