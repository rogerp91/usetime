package co.usetime.mvp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roger Patiño on 11/05/2016.
 */
public class CaptureApp {

    private List<Integer> added_entries = new ArrayList<Integer>();
    private List<Integer> failed_indices = new ArrayList<Integer>();

    public List<Integer> getAddedEntries() {
        return added_entries;
    }

    public void setAddedEntries(List<Integer> addedEntries) {
        this.added_entries = addedEntries;
    }

    public List<Integer> getFailedIndices() {
        return failed_indices;
    }

    public void setFailedIndices(List<Integer> failedIndices) {
        this.failed_indices = failedIndices;
    }
}