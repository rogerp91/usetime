package co.usetime.mvp.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Roger Patiño on 10/05/2016.
 */
public class Example {

    private String id;
    private String userId;
    private String registrySessionId;
    private String date;
    private String time;
    private String duration;
    private String timeStamp;
    private String type;
    private String ipAddress;
    private Object creation;
    private String captionId;
    private String taskId;
    private Object placeId;
    private String categoryId;
    private String keystrokes;
    private String mouse;
    private String processed;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The registrySessionId
     */
    public String getRegistrySessionId() {
        return registrySessionId;
    }

    /**
     * @param registrySessionId The registry_session_id
     */
    public void setRegistrySessionId(String registrySessionId) {
        this.registrySessionId = registrySessionId;
    }

    /**
     * @return The date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return The time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return The duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return The timeStamp
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * @param timeStamp The time_stamp
     */
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress The ip_address
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return The creation
     */
    public Object getCreation() {
        return creation;
    }

    /**
     * @param creation The creation
     */
    public void setCreation(Object creation) {
        this.creation = creation;
    }

    /**
     * @return The captionId
     */
    public String getCaptionId() {
        return captionId;
    }

    /**
     * @param captionId The caption_id
     */
    public void setCaptionId(String captionId) {
        this.captionId = captionId;
    }

    /**
     * @return The taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId The task_id
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return The placeId
     */
    public Object getPlaceId() {
        return placeId;
    }

    /**
     * @param placeId The place_id
     */
    public void setPlaceId(Object placeId) {
        this.placeId = placeId;
    }

    /**
     * @return The categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId The category_id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return The keystrokes
     */
    public String getKeystrokes() {
        return keystrokes;
    }

    /**
     * @param keystrokes The keystrokes
     */
    public void setKeystrokes(String keystrokes) {
        this.keystrokes = keystrokes;
    }

    /**
     * @return The mouse
     */
    public String getMouse() {
        return mouse;
    }

    /**
     * @param mouse The mouse
     */
    public void setMouse(String mouse) {
        this.mouse = mouse;
    }

    /**
     * @return The processed
     */
    public String getProcessed() {
        return processed;
    }

    /**
     * @param processed The processed
     */
    public void setProcessed(String processed) {
        this.processed = processed;
    }
}