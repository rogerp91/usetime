package co.usetime.mvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Roger Patiño on 02/03/2016.
 */
public class Todo {

    @Expose
    @SerializedName("pending")
    private List<Pending> pending;

    @Expose
    @SerializedName("progress")
    private List<Progress> progress;

    @Expose
    @SerializedName("completed")
    private List<Completed> completed;

    @Expose
    @SerializedName("expired")
    private List<Expired> expired;

    @Expose
    @SerializedName("rejected")
    private List<Rejected> rejected;

    public List<Pending> getPending() {
        return pending;
    }

    public void setPending(List<Pending> pending) {
        this.pending = pending;
    }

    public List<Progress> getProgress() {
        return progress;
    }

    public void setProgress(List<Progress> progress) {
        this.progress = progress;
    }

    public List<Completed> getCompleted() {
        return completed;
    }

    public void setCompleted(List<Completed> completed) {
        this.completed = completed;
    }

    public List<Expired> getExpired() {
        return expired;
    }

    public void setExpired(List<Expired> expired) {
        this.expired = expired;
    }

    public List<Rejected> getRejected() {
        return rejected;
    }

    public void setRejected(List<Rejected> rejected) {
        this.rejected = rejected;
    }
}