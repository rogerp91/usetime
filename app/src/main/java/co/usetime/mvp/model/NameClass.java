package co.usetime.mvp.model;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by Roger Patiño on 13/01/2016.
 */
@RealmClass
public class NameClass extends RealmObject {

    private String mPackage;
    private String mClass;
    private String mDuration;

    public String getmClass() {
        return mClass;
    }

    public void setmClass(String mClass) {
        this.mClass = mClass;
    }

    public String getmPackage() {
        return mPackage;
    }

    public void setmPackage(String mPackage) {
        this.mPackage = mPackage;
    }

    public String getmDuration() {
        return mDuration;
    }

    public void setmDuration(String mDuration) {
        this.mDuration = mDuration;
    }
}