package co.usetime.mvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Roger Patiño on 31/05/2016.
 */
public class TasksOfProject {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("creator_id")
    private String creator_id;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("created_at")
    private String created_at;

    @Expose
    @SerializedName("updated_at")
    private String updated_at;

    @Expose
    @SerializedName("enterprise_id")
    private String enterprise_id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("completion")
    private String completion;

    @Expose
    @SerializedName("validated")
    private String validated;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("start_date")
    private String start_date;

    @Expose
    @SerializedName("end_date")
    private String end_date;

    @Expose
    @SerializedName("estimated_time")
    private String estimated_time;

    @Expose
    @SerializedName("parent_id")
    private String parent_id;

    @Expose
    @SerializedName("level")
    private int level;

    @Expose
    @SerializedName("deep")
    private String deep;

    @Expose
    @SerializedName("order_gantt")
    private String order_gantt;

    @Expose
    @SerializedName("projects_id")
    private String projects_id;

    @Expose
    @SerializedName("stakeholder_id")
    private int stakeholder_id;

    @Expose
    @SerializedName("project_category_id")
    private int project_category_id;

    @Expose
    @SerializedName("order_id")
    private String order_id;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("budget")
    private String budget;

    @Expose
    @SerializedName("completed_at")
    private String completed_at;

    @Expose
    @SerializedName("client_id")
    private int client_id;

    @Expose
    @SerializedName("is_private")
    private String is_private;

    @Expose
    @SerializedName("responsible")
    private String responsible;

    @Expose
    @SerializedName("is_flowchart")
    private String is_flowchart;

    @Expose
    @SerializedName("average")
    private String average;

    @Expose
    @SerializedName("position")
    private String position;

    @Expose
    @SerializedName("archived")
    private String archived;

    @Expose
    @SerializedName("favorite")
    private String favorite;

    @Expose
    @SerializedName("color")
    private String color;

    @Expose
    @SerializedName("tasks")
    private List<TasksOfProject> tasks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getEnterprise_id() {
        return enterprise_id;
    }

    public void setEnterprise_id(String enterprise_id) {
        this.enterprise_id = enterprise_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompletion() {
        return completion;
    }

    public void setCompletion(String completion) {
        this.completion = completion;
    }

    public String getValidated() {
        return validated;
    }

    public void setValidated(String validated) {
        this.validated = validated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getEstimated_time() {
        return estimated_time;
    }

    public void setEstimated_time(String estimated_time) {
        this.estimated_time = estimated_time;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDeep() {
        return deep;
    }

    public void setDeep(String deep) {
        this.deep = deep;
    }

    public String getOrder_gantt() {
        return order_gantt;
    }

    public void setOrder_gantt(String order_gantt) {
        this.order_gantt = order_gantt;
    }

    public String getProjects_id() {
        return projects_id;
    }

    public void setProjects_id(String projects_id) {
        this.projects_id = projects_id;
    }

    public int getStakeholder_id() {
        return stakeholder_id;
    }

    public void setStakeholder_id(int stakeholder_id) {
        this.stakeholder_id = stakeholder_id;
    }

    public int getProject_category_id() {
        return project_category_id;
    }

    public void setProject_category_id(int project_category_id) {
        this.project_category_id = project_category_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getCompleted_at() {
        return completed_at;
    }

    public void setCompleted_at(String completed_at) {
        this.completed_at = completed_at;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getIs_private() {
        return is_private;
    }

    public void setIs_private(String is_private) {
        this.is_private = is_private;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public String getIs_flowchart() {
        return is_flowchart;
    }

    public void setIs_flowchart(String is_flowchart) {
        this.is_flowchart = is_flowchart;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getArchived() {
        return archived;
    }

    public void setArchived(String archived) {
        this.archived = archived;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<TasksOfProject> getTasks() {
        return tasks;
    }

    public void setTasks(List<TasksOfProject> tasks) {
        this.tasks = tasks;
    }
}