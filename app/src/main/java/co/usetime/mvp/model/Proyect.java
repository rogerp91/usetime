package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 17/02/2016.
 */
public class Proyect {

    private String id;
    private String creator_id;
    private String type;
    private String created_at;
    private String updated_at;
    private String enterprise_id;
    private String name;
    private String completion;
    private String validated;
    private String description;
    private String start_date;
    private String end_date;
    private String estimated_time;
    private String parent_id;
    private String level;
    private String deep;
    private String lineage;
    private String projects_id;
    private String stakeholder_id;
    private String project_category_id;
    private String order_id;
    private String archived;
    private String favorite;
    private String status;
    private String budget;
    private String cost;
    private String completed_at;
    private String client_id;
    private String is_private;
    private String user_id;
    private String task_id;
    private String dependent_task;
    private String role;
    private String acumulado;
    private String total_time;

    public String getTotal_time() {
        return total_time;
    }

    public void setTotal_time(String total_time) {
        this.total_time = total_time;
    }

    public String getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(String acumulado) {
        this.acumulado = acumulado;
    }

    private String total_task;

    public String getTotal_task() {
        return total_task;
    }

    public void setTotal_task(String total_task) {
        this.total_task = total_task;
    }

    private String total_delivered_validate = null;
    private String total_delivered = null;
    private String total_process = null;
    private String total_expired = null;

    /**
     * @SerializedName("count_notifications") private TotalNotifications count_notifications;
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getEnterprise_id() {
        return enterprise_id;
    }

    public void setEnterprise_id(String enterprise_id) {
        this.enterprise_id = enterprise_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompletion() {
        return completion;
    }

    public void setCompletion(String completion) {
        this.completion = completion;
    }

    public String getValidated() {
        return validated;
    }

    public void setValidated(String validated) {
        this.validated = validated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getEstimated_time() {
        return estimated_time;
    }

    public void setEstimated_time(String estimated_time) {
        this.estimated_time = estimated_time;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDeep() {
        return deep;
    }

    public void setDeep(String deep) {
        this.deep = deep;
    }

    public String getLineage() {
        return lineage;
    }

    public void setLineage(String lineage) {
        this.lineage = lineage;
    }

    public String getProjects_id() {
        return projects_id;
    }

    public void setProjects_id(String projects_id) {
        this.projects_id = projects_id;
    }

    public String getStakeholder_id() {
        return stakeholder_id;
    }

    public void setStakeholder_id(String stakeholder_id) {
        this.stakeholder_id = stakeholder_id;
    }

    public String getProject_category_id() {
        return project_category_id;
    }

    public void setProject_category_id(String project_category_id) {
        this.project_category_id = project_category_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getArchived() {
        return archived;
    }

    public void setArchived(String archived) {
        this.archived = archived;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCompleted_at() {
        return completed_at;
    }

    public void setCompleted_at(String completed_at) {
        this.completed_at = completed_at;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getIs_private() {
        return is_private;
    }

    public void setIs_private(String is_private) {
        this.is_private = is_private;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getDependent_task() {
        return dependent_task;
    }

    public void setDependent_task(String dependent_task) {
        this.dependent_task = dependent_task;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTotal_delivered_validate() {
        return total_delivered_validate;
    }

    public void setTotal_delivered_validate(String total_delivered_validate) {
        this.total_delivered_validate = total_delivered_validate;
    }

    public String getTotal_delivered() {
        return total_delivered;
    }

    public void setTotal_delivered(String total_delivered) {
        this.total_delivered = total_delivered;
    }

    public String getTotal_process() {
        return total_process;
    }

    public void setTotal_process(String total_process) {
        this.total_process = total_process;
    }

    public String getTotal_expired() {
        return total_expired;
    }

    public void setTotal_expired(String total_expired) {
        this.total_expired = total_expired;
    }
}