package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 18/03/2016.
 */
public class Position {

    private Geoposition position;

    public Position(Geoposition position) {
        this.position = position;
    }
}