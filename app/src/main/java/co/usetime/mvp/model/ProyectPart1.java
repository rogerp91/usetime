package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 18/02/2016.
 */
public class ProyectPart1 {

    private String name;
    private int total_notificationses;
    private int total_task;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal_notificationses() {
        return total_notificationses;
    }

    public void setTotal_notificationses(int total_notificationses) {
        this.total_notificationses = total_notificationses;
    }

    public int getTotal_task() {
        return total_task;
    }

    public void setTotal_task(int total_task) {
        this.total_task = total_task;
    }
}