package co.usetime.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Roger Patiño on 24/02/2016.
 */
public class SubTasks implements Parcelable {

    private String id;
    private String parent_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.parent_id);
    }

    public SubTasks() {
    }

    private SubTasks(Parcel in) {
        this.id = in.readString();
        this.parent_id = in.readString();
    }

    public static final Parcelable.Creator<SubTasks> CREATOR = new Parcelable.Creator<SubTasks>() {
        public SubTasks createFromParcel(Parcel source) {
            return new SubTasks(source);
        }

        public SubTasks[] newArray(int size) {
            return new SubTasks[size];
        }
    };
}
