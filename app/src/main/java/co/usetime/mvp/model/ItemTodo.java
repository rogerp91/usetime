package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 25/04/2016.
 */
public class ItemTodo {

    private long id;
    private String name;
    private String date;
    private String person;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }
}