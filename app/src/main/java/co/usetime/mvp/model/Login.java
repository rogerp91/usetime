package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 17/12/2015.
 */
public class Login {

    private String email;
    private String password;

    public Login(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
