package co.usetime.mvp.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Roger Patiño on 26/02/2016.
 */
@RealmClass
public class Imagen extends RealmObject {

    @PrimaryKey
    private String id;
    private String id_task;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_task() {
        return id_task;
    }

    public void setId_task(String id_task) {
        this.id_task = id_task;
    }

}