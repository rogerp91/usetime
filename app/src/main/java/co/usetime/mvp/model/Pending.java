package co.usetime.mvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 02/03/2016.
 */
public class Pending {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("created_at")
    private String created_at;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("task_id")
    private String task_id;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    @Expose
    @SerializedName("is_flowchart")
    private String is_flowchart;

    public String getIs_flowchart() {
        return is_flowchart;
    }

    public void setIs_flowchart(String is_flowchart) {
        this.is_flowchart = is_flowchart;
    }

    @Expose
    @SerializedName("creator")
    private Creator creator;

    public Creator getCreator() {
        return creator;
    }

    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}