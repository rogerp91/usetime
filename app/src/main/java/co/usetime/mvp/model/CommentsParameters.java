package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 01/04/2016.
 */
public class CommentsParameters {

    private String comment;
    private String task_id;

    public CommentsParameters(String comment, String task_id) {
        this.comment = comment;
        this.task_id = task_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }
}