package co.usetime.mvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Roger Patiño on 31/05/2016.
 */
public class ProjectTasks {

    @Expose
    @SerializedName("id")
    private String id;
    /**
     * private String id;
     * private String creator_id;
     * private String type;
     * private String created_at;
     * private String updated_at;
     * private String enterprise_id;
     * private String name;
     * private String completion;
     * private String validated;
     * private String description;
     * private String start_date;
     * private String end_date;
     * private String estimated_time;
     * private int parent_id;
     * private String level;
     * private String deep;
     * private String order_gantt;
     * private int projects_id;
     * private int stakeholder_id;
     * private int project_category_id;
     * private String order_id;
     * private String status;
     * private String budget;
     * private String completed_at;
     * private int client_id;
     * private String is_private;
     * private int responsible;
     * private String is_flowchart;
     * private String average;
     * private String user_id;
     * private String task_id;
     * private int dependent_task;
     * private String task_role_id;
     */
    @Expose
    @SerializedName("tasks")
    private List<TasksOfProject> tasks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TasksOfProject> getTasks() {
        return tasks;
    }

    public void setTasks(List<TasksOfProject> tasks) {
        this.tasks = tasks;
    }
}