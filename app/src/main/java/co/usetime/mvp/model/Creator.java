package co.usetime.mvp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 27/04/2016.
 */
public class Creator {


    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    /**
     * @SerializedName("email") private String email;
     * @SerializedName("enterprise_id") private String enterprise_id;
     * @SerializedName("status") private String status;
     * @SerializedName("created_at") private String created_at;
     * @SerializedName("updated_at") private String updated_at;
     * @SerializedName("group_id") private String group_id;
     * @Expose
     * @SerializedName("role_id") private String role_id;
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}