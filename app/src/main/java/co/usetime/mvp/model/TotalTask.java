package co.usetime.mvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by Roger Patiño on 17/02/2016.
 */
@RealmClass
public class TotalTask extends RealmObject{

    @PrimaryKey
    @Expose
    @SerializedName("projects_id")
    private String projects_id;

    @Expose
    @SerializedName("aggregate")
    private String aggregate;

    public String getProjects_id() {
        return projects_id;
    }

    public void setProjects_id(String projects_id) {
        this.projects_id = projects_id;
    }

    public String getAggregate() {
        return aggregate;
    }

    public void setAggregate(String aggregate) {
        this.aggregate = aggregate;
    }
}