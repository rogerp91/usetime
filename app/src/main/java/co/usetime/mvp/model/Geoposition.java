package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 18/03/2016.
 */
public class Geoposition {

    private double latitude;
    private double longitude;

    public Geoposition(double latitude, double longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}