package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 01/03/2016.
 */
public class Content {

    public static final int HEADER_TYPE = 0;
    public static final int BODY_TYPE = 1;

}