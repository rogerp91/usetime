package co.usetime.mvp.model;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.TrafficStats;

/**
 * Created by Roger Patiño on 03/12/2015.
 */
public class TrafficItems {

    //TX = transmitir
    //RX = recibir
    private long tx = 0;
    private long rx = 0;
    private long wifi_tx = 0;
    private long wifi_rx = 0;
    private long mobil_tx = 0;
    private long mobil_rx = 0;
    private long current_tx = 0;
    private long current_rx = 0;
    private ApplicationInfo app;
    private boolean isMobil = false;

    public TrafficItems(ApplicationInfo info) {
        this.app = info;
        update();
    }

    public void setMobilTraffic(boolean movil) {
        this.isMobil = movil;
    }

    //actualizar
    public void update() {
        long delta_tx = TrafficStats.getUidTxBytes(app.uid) - tx;
        long delta_rx = TrafficStats.getUidRxBytes(app.uid) - rx;

        tx = TrafficStats.getUidTxBytes(app.uid);
        rx = TrafficStats.getUidRxBytes(app.uid);

        current_tx = current_tx + delta_tx;
        current_rx = current_rx + delta_rx;

        if (isMobil) {
            mobil_tx = mobil_tx + delta_tx;
            mobil_rx = mobil_rx + delta_rx;
        } else {
            wifi_tx = wifi_tx + delta_tx;
            wifi_rx = wifi_rx + delta_rx;
        }
    }

    //crear
    public static TrafficItems create(ApplicationInfo _app) {
        //Log.i("TrafficItems", "create");
        long _tx = TrafficStats.getUidTxBytes(_app.uid);
        long _rx = TrafficStats.getUidRxBytes(_app.uid);
        if ((_tx + _rx) > 0) return new TrafficItems(_app);
        return null;
    }

    public int getTotalUsageKb() {
        return Math.round((tx + rx) / 1024);
    }

    public int getTotalUsegeMovilKb() {
        return Math.round((mobil_tx + mobil_rx) / 1024);
    }

    public int getTotalUsegeWifiKb() {
        return Math.round((wifi_tx + wifi_rx) / 1024);
    }

    public String getApplicationLabel(PackageManager packageManager) {
        return packageManager.getApplicationLabel(app).toString();
    }

    public String getPackage() {
        return app.packageName;
    }

    public Drawable getIcon(PackageManager packageManager) {
        return packageManager.getApplicationIcon(app);
    }
}
