package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 18/02/2016.
 */
public class ProyectPart2 {

    private int total_delivered_validate;
    private int total_delivered;
    private int total_process;
    private int total_expired;

    public int getTotal_delivered_validate() {
        return total_delivered_validate;
    }

    public void setTotal_delivered_validate(int total_delivered_validate) {
        this.total_delivered_validate = total_delivered_validate;
    }

    public int getTotal_delivered() {
        return total_delivered;
    }

    public void setTotal_delivered(int total_delivered) {
        this.total_delivered = total_delivered;
    }

    public int getTotal_process() {
        return total_process;
    }

    public void setTotal_process(int total_process) {
        this.total_process = total_process;
    }

    public int getTotal_expired() {
        return total_expired;
    }

    public void setTotal_expired(int total_expired) {
        this.total_expired = total_expired;
    }
}