package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 29/03/2016.
 */
public class Edit {

    private String description;
    private String end_date;
    private int estimated_time;
    private boolean is_flowchart;
    private String name;
    private String responsible;
    private String start_date;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getEstimated_time() {
        return estimated_time;
    }

    public void setEstimated_time(int estimated_time) {
        this.estimated_time = estimated_time;
    }

    public boolean getIs_flowchart() {
        return is_flowchart;
    }

    public void setIs_flowchart(boolean is_flowchart) {
        this.is_flowchart = is_flowchart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }
}