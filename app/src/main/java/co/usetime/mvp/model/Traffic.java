package co.usetime.mvp.model;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Roger Patiño on 04/12/2015.
 */
public class Traffic extends RealmObject {

    //@PrimaryKey
    private String mPackage;
    private String mName;
    private int mMovilKb;
    private int mWifiKb;

    @Ignore
    private int mTotalKb;

    private String date;

    public String getmPackage() {
        return mPackage;
    }

    public void setmPackage(String mPackage) {
        this.mPackage = mPackage;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmMovilKb() {
        return mMovilKb;
    }

    public void setmMovilKb(int mMovilKb) {
        this.mMovilKb = mMovilKb;
    }

    public int getmWifiKb() {
        return mWifiKb;
    }

    public void setmWifiKb(int mWifiKb) {
        this.mWifiKb = mWifiKb;
    }

    public int getmTotalKb() {
        return mTotalKb;
    }

    public void setmTotalKb(int mTotalKb) {
        this.mTotalKb = mTotalKb;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}