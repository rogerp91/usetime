package co.usetime.mvp.model;

/**
 * Created by Roger Patiño on 14/03/2016.
 */
public class TodoStatus {

    private String id;
    private int status;

    public TodoStatus(String id, int status) {
        this.id = id;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}