package co.usetime.executor;

public interface Executor {
	
    public void run(final Interactor interactor);
    
}
