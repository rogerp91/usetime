package co.usetime.executor;

public interface MainThread {
	
	public void post(final Runnable runnable);
	
}
