package co.usetime.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import co.usetime.UIApp;

/**
 * Created by Roger Patiño on 13/06/2016.
 */
public class Network {

    /**
     * @return varifica si hay internet
     */
    public static boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) UIApp.getContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

}