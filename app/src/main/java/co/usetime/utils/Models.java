package co.usetime.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.usetime.domain.object.ProyectObject;
import co.usetime.domain.object.TasksObject;
import co.usetime.mvp.model.Proyect;
import co.usetime.mvp.model.ResponseDocument;
import co.usetime.mvp.model.Tasks;

/**
 * Created by Roger Patiño on 22/02/2016.
 */
public class Models {

    /**
     * ----------------------------------- PROYECT --------------------------------------------------
     */
    public static List<ProyectObject> ProyectObjectList(List<Proyect> proyects) {
        List<ProyectObject> proyectObjects = new ArrayList<>();
        for (Proyect proyect : proyects) {
            proyectObjects.add(ProyectToRealm(proyect));
        }

        return proyectObjects;
    }

    public static ProyectObject ProyectToRealm(Proyect proyect) {
        ProyectObject object = new ProyectObject();
        object.setId(proyect.getId());
        object.setCreator_id(proyect.getCreator_id());
        object.setType(proyect.getType());
        object.setName(proyect.getName());
        object.setParent_id(proyect.getParent_id());
        object.setProjects_id(proyect.getProjects_id());
        object.setProject_category_id(proyect.getProject_category_id());
        object.setStatus(proyect.getStatus());
        object.setUser_id(proyect.getUser_id());
        object.setTask_id(proyect.getTask_id());
        object.setTotal_delivered(proyect.getTotal_delivered());
        object.setTotal_expired(proyect.getTotal_expired());
        object.setTotal_delivered_validate(proyect.getTotal_delivered_validate());
        object.setTotal_process(proyect.getTotal_process());
        object.setTotal_task(object.getTotal_task());
        object.setAcumulado(object.getAcumulado());
        object.setTotal_time(object.getTotal_time());
        return object;
    }

    public static Proyect RealmToProyect(ProyectObject proyectObject) {
        Proyect object = new Proyect();
        object.setId(proyectObject.getId());
        object.setCreator_id(proyectObject.getCreator_id());
        object.setType(proyectObject.getType());
        object.setName(proyectObject.getName());
        object.setParent_id(proyectObject.getParent_id());
        object.setProjects_id(proyectObject.getProjects_id());
        object.setProject_category_id(proyectObject.getProject_category_id());
        object.setStatus(proyectObject.getStatus());
        object.setUser_id(proyectObject.getUser_id());
        object.setTask_id(proyectObject.getTask_id());
        object.setTotal_delivered_validate(proyectObject.getTotal_delivered_validate());
        object.setTotal_delivered(proyectObject.getTotal_delivered());
        object.setTotal_process(proyectObject.getTotal_process());
        object.setTotal_expired(proyectObject.getTotal_expired());
        object.setTotal_task(proyectObject.getTotal_task());
        object.setAcumulado(proyectObject.getAcumulado());
        object.setTotal_time(proyectObject.getTotal_time());
        return object;
    }

    /**
     * ------------------------------------TASK ----------------------------------------------------
     */

    public static List<TasksObject> TaskObjectList(List<Tasks> taskses) {
        List<TasksObject> proyectObjects = new ArrayList<>();
        for (Tasks tasks : taskses) {
            proyectObjects.add(TaskToRealm(tasks));
        }
        return proyectObjects;
    }

    public static TasksObject TaskToRealm(Tasks tasks) {
        TasksObject object = new TasksObject();
        object.setId(tasks.getId());
        object.setCreator_id(tasks.getCreator_id());
        object.setType(tasks.getType());
        object.setName(tasks.getName());
        object.setParent_id(tasks.getParent_id());
        object.setProjects_id(tasks.getProjects_id());
        object.setStart_date(tasks.getStart_date());
        object.setIs_flowchart(tasks.getIs_flowchart());
        object.setEnd_date(tasks.getEnd_date());
        object.setProject_category_id(tasks.getProject_category_id());
        object.setStatus(tasks.getStatus());
        object.setUser_id(tasks.getUser_id());
        object.setTask_id(tasks.getTask_id());
        return object;
    }

    public static Tasks RealmToTask(TasksObject tasksObject) {
        Tasks object = new Tasks();
        object.setId(tasksObject.getId());
        object.setCreator_id(tasksObject.getCreator_id());
        object.setType(tasksObject.getType());
        object.setName(tasksObject.getName());
        object.setParent_id(tasksObject.getParent_id());
        object.setProjects_id(tasksObject.getProjects_id());
        object.setStart_date(tasksObject.getStart_date());
        object.setIs_flowchart(tasksObject.getis_flowchart());
        object.setEnd_date(tasksObject.getEnd_date());
        object.setProject_category_id(tasksObject.getProject_category_id());
        object.setStatus(tasksObject.getStatus());
        object.setUser_id(tasksObject.getUser_id());
        object.setTask_id(tasksObject.getTask_id());
        return object;
    }

    /**
     * ------------------------------------------------------------------------------------------
     */
    public static List<Proyect> parseJsonProyect(String object) {
        List<Proyect> proyectList = null;
        Proyect proyect;
        try {
            proyectList = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(object);
            JSONObject jsonObject;
            for (int i = 0; i < jsonArray.length(); i++) {
                proyect = new Proyect();
                jsonObject = jsonArray.getJSONObject(i);
                proyect.setId(jsonObject.getString("id"));
                proyect.setCreator_id(jsonObject.getString("creator_id"));
                proyect.setType(jsonObject.getString("type"));
                proyect.setCreated_at(jsonObject.getString("created_at"));
                proyect.setUpdated_at(jsonObject.getString("updated_at"));
                proyect.setEnterprise_id(jsonObject.getString("enterprise_id"));
                proyect.setName(jsonObject.getString("name"));
                proyect.setDescription(jsonObject.getString("description"));
                proyect.setStart_date(jsonObject.getString("start_date"));
                proyect.setEnd_date(jsonObject.getString("end_date"));
                proyect.setEstimated_time(jsonObject.getString("estimated_time"));
                proyect.setStatus(jsonObject.getString("status"));

                if (jsonObject.getString("total_time").equals("null")) {
                    proyect.setTotal_time("00:00:00");
                }else{
                    proyect.setTotal_time(jsonObject.getString("total_time"));
                }


                proyect.setAcumulado(jsonObject.getString("acumulado"));


                if (jsonObject.getString("total_delivered_validate").length() != 0) {
                    JSONArray results = jsonObject.getJSONArray("total_delivered_validate");
                    if (results.length() != 0) {
                        for (int k = 0; k < results.length(); k++) {
                            JSONObject obj = results.getJSONObject(k);
                            proyect.setTotal_delivered_validate(obj.getString("aggregate"));
                        }
                    } else {
                        proyect.setTotal_delivered_validate(null);
                    }
                }

                if (jsonObject.getString("total_delivered").length() != 0) {
                    JSONArray results = jsonObject.getJSONArray("total_delivered");
                    if (results.length() != 0) {
                        for (int k = 0; k < results.length(); k++) {
                            JSONObject obj = results.getJSONObject(k);
                            proyect.setTotal_delivered(obj.getString("aggregate"));
                        }
                    } else {
                        proyect.setTotal_delivered(null);
                    }
                }

                if (jsonObject.getString("total_process").length() != 0) {
                    JSONArray results = jsonObject.getJSONArray("total_process");
                    if (results.length() != 0) {
                        for (int k = 0; k < results.length(); k++) {
                            JSONObject obj = results.getJSONObject(k);
                            proyect.setTotal_process(obj.getString("aggregate"));
                        }
                    } else {
                        proyect.setTotal_process(null);
                    }
                }

                if (jsonObject.getString("total_expired").length() != 0) {
                    JSONArray results = jsonObject.getJSONArray("total_expired");
                    if (results.length() != 0) {
                        for (int k = 0; k < results.length(); k++) {
                            JSONObject obj = results.getJSONObject(k);
                            proyect.setTotal_expired(obj.getString("aggregate"));
                        }
                    } else {
                        proyect.setTotal_expired(null);
                    }
                }

                if (jsonObject.getString("total_task").length() != 0) {
                    JSONArray results = jsonObject.getJSONArray("total_task");
                    if (results.length() != 0) {
                        for (int k = 0; k < results.length(); k++) {
                            JSONObject obj = results.getJSONObject(k);
                            proyect.setTotal_task(obj.getString("aggregate"));
                        }
                    } else {
                        proyect.setTotal_expired(null);
                    }
                }

                proyectList.add(proyect);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return proyectList;
    }

    public static ResponseDocument DocumentJsonToString(String s) {
        try {
            JSONObject object = new JSONObject(s);
            ResponseDocument document = new ResponseDocument();
            document.setCreator_id(object.getString("creator_id"));
            document.setTask_id(object.getString("task_id"));
            document.setType(object.getString("type"));
            document.setProject_id(object.getString("project_id"));
            document.setId(object.getInt("id"));
            return document;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * ------------------------------------ PROYECT TASK NEW ----------------------------------------------------
     */

}