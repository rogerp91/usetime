package co.usetime.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import co.usetime.UIApp;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.mvp.model.Completed;
import co.usetime.mvp.model.Pending;
import co.usetime.mvp.model.Permissions;
import co.usetime.mvp.model.Rejected;
import co.usetime.mvp.model.TodoSimple;
import co.usetime.ui.activity.MainActivity;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class Functions {//funciones de ayuda

    protected static String TAG = Functions.class.getSimpleName();

    /**
     * @param text texto de entrada
     * @return valida y retorna bool si el texto es mayor que 3
     */
    public static boolean minSize(String text) {
        return text.length() > 3;
    }

    /**
     * @param email valor de entrada
     * @return valida y devuelve bool y verifica si email tiene un @
     */
    public static boolean isEmail(String email) {
        return email.contains("@");
    }

    /**
     * @return varifica si hay internet
     */
    public static boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) UIApp.getContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * @return verifica si se puede leer la memoria externa
     */
    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    /**
     * @return verifica si pel almacenamiento esta disponible
     */
    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available for read and write */

    /**
     * @return verificar si se puede escribir en la memoria extenal
     */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */

    /**
     * @return
     */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * @return memoria exstenar esta disponible?
     */
    public static boolean externalMemoryAvailable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * @param exit variablle que tiene la uri del directorio
     * @return verificar si el diretorio existe
     */
    public static boolean dirExists(String exit) {
        boolean ret = false;
        File dir1 = new File(exit);
        File dir2 = new File(Environment.getExternalStorageDirectory(), exit);
        if (dir1.exists() && dir1.isDirectory()) {
            ret = true;
        } else {
            if (dir2.exists() && dir2.isDirectory()) {
                ret = true;
            }
        }
        return ret;
    }

    /**
     * @return es android lolipop
     */
    public static boolean isLolipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    /**
     * @return es android lolipop mr1?
     */
    public static boolean isLolipopMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    /**
     * @return verificar si es marshmallow
     */
    public static boolean isMarshmallow() {
        return Build.VERSION.SDK_INT > 22;
    }

    //uri de la imagen

    /**
     * @param type
     * @param id
     * @return
     */
    public static Uri getOutputMediaFileUri(int type, int id) {
        return Uri.fromFile(getOutputMediaFile(type, id));
    }

    /**
     * @param type
     * @param id
     * @return
     */
    private static File getOutputMediaFile(int type, int id) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String nameCapture = Integer.toString(id) + "_IMG_" + timeStamp;
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Constants.PATH_BASE);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                //Log.d(TAG, "Oops! Failed create " + Constants.PATH_BASE + " directory");
                return null;
            }
        }
        File mediaFile;
        if (type == MainActivity.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + nameCapture + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    public static int getHourToMilisecond(String hour) {
        return Integer.parseInt(hour) * 3600000;
    }

    public static int getMinuteToMilisecond(String min) {
        return Integer.parseInt(min) * 60000;
    }

    /**
     * @return busca la version de la app
     */
    public static String getVersion() {
        PackageInfo pInfo = null;
        String version = "";
        try {
            pInfo = UIApp.getContext.getPackageManager().getPackageInfo(UIApp.getContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "";
        }
        version = pInfo.versionName;
        return version;
    }

    /**
     * @param type tipo de gson si es con expose
     * @return crea en gson
     */
    public static Gson provideGson(boolean type) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        if (type) {
            return gsonBuilder.create();
        } else {//exponse
            gsonBuilder.excludeFieldsWithoutExposeAnnotation();
            return gsonBuilder.create();
        }
    }

    /**
     * @return crea una instancia okhttp
     */
    public static OkHttpClient provideOkHttpClient() {
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setConnectTimeout(60, TimeUnit.SECONDS);
        return client;
    }

    /**
     * @param gson
     * @param okHttpClient
     * @return retronar la insta retrofit par ahacer peticiones pero nececita sel gson para parsiar y okhttp para la cabezera
     */
    public static Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson)).baseUrl(HttpRestClient.BASE_URL).client(okHttpClient).build();
    }

    public static HttpRestClient provideHttpRestClient(Retrofit retrofit) {
        return retrofit.create(HttpRestClient.class);
    }

    /**
     * @param uri ubicacion donde esta el archivo a buscar
     * @return
     * @throws URISyntaxException
     */
    public static String getPath(Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = UIApp.getContext.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getTimeMiliter(int hour) {
        switch (hour) {
            case 13:
                return "1";
            case 14:
                return "2";
            case 15:
                return "3";
            case 16:
                return "4";
            case 17:
                return "5";
            case 18:
                return "6";
            case 19:
                return "7";
            case 20:
                return "8";
            case 21:
                return "9";
            case 22:
                return "10";
            case 23:
                return "11";
        }
        return "0";
    }

    public static String getTimeMiliter2(int hour) {
        switch (hour) {
            case 0:
                return "12";
            case 13:
                return "1";
            case 14:
                return "2";
            case 15:
                return "3";
            case 16:
                return "4";
            case 17:
                return "5";
            case 18:
                return "6";
            case 19:
                return "7";
            case 20:
                return "8";
            case 21:
                return "9";
            case 22:
                return "10";
            case 23:
                return "11";
        }
        return "0";
    }

    public static String getMinute(int minute) {
        return (minute > -1 && minute < 10) ? "0" + minute : Integer.toString(minute);
    }

    public static String caculateTime(int hourOfDay, int amOrPm) {
        switch (amOrPm) {
            case 0:
                return (hourOfDay == 0) ? "12" : Integer.toString(hourOfDay);
            case 1:
                return (hourOfDay == 12) ? "12" : getTimeMiliter(hourOfDay);
        }
        return null;
    }

    public static boolean hasPermission(String permission) {
        int res = UIApp.getContext.checkCallingOrSelfPermission(permission);
        return res == PackageManager.PERMISSION_GRANTED;

    }

    public static String listToJSON(List<Permissions> permissionses) {
        String json = new Gson().toJson(permissionses);
        return json;
    }

    public static List<Permissions> JSONToList(String json) {
        Type type = new TypeToken<List<Permissions>>() {
        }.getType();
        List<Permissions> inpList = new Gson().fromJson(json, type);
        for (int i = 0; i < inpList.size(); i++) {
            Permissions x = inpList.get(i);
            //System.out.println(x);
        }
        return inpList;
    }

    public static boolean getPermission(String id, List<Permissions> permissionses) {
        for (Permissions permissions : permissionses) {
            if (permissions.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public static Pending SimpleToPending(TodoSimple simple) {
        Pending pending = new Pending();
        pending.setName(simple.getName());
        pending.setCreated_at(simple.getCreated_at());
        pending.setId(simple.getId());
        pending.setStatus(Integer.toString(simple.getStatus()));
        pending.setType(simple.getType());
        return pending;
    }

    public static Completed SimpleToCompleted(TodoSimple simple) {
        Completed completed = new Completed();
        completed.setName(simple.getName());
        completed.setCreated_at(simple.getCreated_at());
        completed.setId(simple.getId());
        completed.setStatus(Integer.toString(simple.getStatus()));
        completed.setType(simple.getType());
        return completed;
    }

    public static Rejected SimpleToRejected(TodoSimple simple) {
        Rejected rejected = new Rejected();
        rejected.setName(simple.getName());
        rejected.setCreated_at(simple.getCreated_at());
        rejected.setId(simple.getId());
        rejected.setStatus(Integer.toString(simple.getStatus()));
        rejected.setType(simple.getType());
        return rejected;
    }

    /**
     * @return tiempo
     */
    public static synchronized String getTime() {
        //TimeZone.getDefault()
        //TimeZone.getTimeZone("UTC")
        /**
         Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
         return calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
         */
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
    }


    public static synchronized String getDateTime() {
        /**
         Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
         return getDate() + " " + calendar.get(Calendar.HOUR) + "-" + calendar.get(Calendar.MINUTE) + "-" + calendar.get(Calendar.SECOND);
         */
        Calendar calendar = Calendar.getInstance();
        return getDate() + " " + calendar.get(Calendar.HOUR) + "-" + calendar.get(Calendar.MINUTE) + "-" + calendar.get(Calendar.SECOND);
    }

    /**
     * @return fecha
     */
    public static synchronized String getDate() {
        //TimeZone.getDefault()
        Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        String year = Integer.toString(calendar.get(Calendar.YEAR));
        String mount = Integer.toString(calendar.get(Calendar.MONTH) + 1);
        String day = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
        return year + "-" + mount + "-" + day;
    }

    //diferencia de fechas
    public static synchronized String compareTime(String timeList, String dateList) {
        //obtener fecha
        Calendar calendar = Calendar.getInstance();

        String[] arrayList, arrayCache, arrayDateList, arrayDateCache = null;
        arrayList = timeList.split(":");
        arrayDateList = dateList.split("-");
        arrayCache = getTime().split(":");//hora actual
        arrayDateCache = getDate().split("-");//fecha actual

        /** viene del sistema**/
        //time - list
        int hourList = Integer.parseInt(arrayList[0]);
        int minuteList = Integer.parseInt(arrayList[1]);
        int secondList = Integer.parseInt(arrayList[2]);
        //date - list
        int yearList = Integer.parseInt(arrayDateList[0]);
        int mountList = Integer.parseInt(arrayDateList[1]);
        int dayList = Integer.parseInt(arrayDateList[2]);


        /** viene del la cache**/
        //cache - cache
        int hourCahe = Integer.parseInt(arrayCache[0]);
        int minuteCahe = Integer.parseInt(arrayCache[1]);
        int secondCache = Integer.parseInt(arrayCache[2]);
        //date - cache
        int yearCache = Integer.parseInt(arrayDateCache[0]);
        int mountCache = Integer.parseInt(arrayDateCache[1]);
        int dayCache = Integer.parseInt(arrayDateList[2]);

        // Crear 2 instancias de Calendar
        Calendar calList = Calendar.getInstance();
        Calendar calCache = Calendar.getInstance();

        // asignar los datos para ser restado
        calList.set(yearList, mountList, dayList, hourList, minuteList, secondList);
        calCache.set(yearCache, mountCache, dayCache, hourCahe, minuteCahe, secondCache);

        // conseguir la representacion de la fecha en milisegundos
        long milis1 = calList.getTimeInMillis();
        long milis2 = calCache.getTimeInMillis();

        long diff = milis2 - milis1;//restar
        int seconds = (int) (diff / 1000) % 60;
        int minutes = (int) ((diff / (1000 * 60)) % 60);
        int hours = (int) ((diff / (1000 * 60 * 60)) % 24);

        //Log.d(TAG, "Hora: " + hours);
        //Log.d(TAG, "minutes: " + minutes);
        //Log.d(TAG, "seconds: " + seconds);

        String sHour = "";
        String sMinutes = "";
        String sSeconds = "";
        if (hours <= 9) {
            sHour = "0" + (Integer.toString(hours));
        } else {
            sHour = (Integer.toString(hours));
        }
        if (minutes <= 9) {
            sMinutes = "0" + (Integer.toString(minutes));
        } else {
            sMinutes = (Integer.toString(minutes));
        }
        if (seconds <= 9) {
            sSeconds = "0" + (Integer.toString(seconds));
        } else {
            sSeconds = (Integer.toString(seconds));
        }

        //Log.d(TAG, sHour + ":" + sMinutes + ":" + sSeconds);

        return sHour + ":" + sMinutes + ":" + sSeconds;

        //return (Integer.toString(hours) + ":" + Integer.toString(minutes) + ":" + Integer.toString(seconds));

        //return (Integer.toString(hours) + ":" + Integer.toString(minutes));
    }

    public static int stringToJson(String json) {
        //System.out.println(TAG + " -- " + json);
        try {
            JSONObject object = new JSONObject(json);
            if (!json.equals("")) {
                return Integer.parseInt(object.getString("id"));
            } else {
                return -1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static String getTimestamp() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault()).format(new Date());
    }

    public static String getDateTime2() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(new Date());
    }

    private static char[] c = new char[]{'k', 'm', 'b', 't'};

    public static String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
        return (d < 1000 ? //this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? //this decides whether to trim the decimals
                        (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));

    }

}//endClass