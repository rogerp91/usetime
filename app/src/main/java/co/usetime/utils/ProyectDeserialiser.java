package co.usetime.utils;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import co.usetime.mvp.model.Proyect;

/**
 * Created by Roger Patiño on 13/04/2016.
 */
public class ProyectDeserialiser implements JsonDeserializer<Proyect> {

    @Override
    public Proyect deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        JsonElement value1 = json.getAsJsonObject().get("total_delivered_validate");
        JsonElement value2 = json.getAsJsonObject().get("total_delivered");
        JsonElement value3 = json.getAsJsonObject().get("total_process");
        JsonElement value4 = json.getAsJsonObject().get("total_expired");
        if (value1 == null) {
                
        }
        if (value2 == null) {

        }
        if (value3 == null) {

        }
        if (value4 == null) {

        }
        return null;
    }
}