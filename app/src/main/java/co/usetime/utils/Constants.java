package co.usetime.utils;

public class Constants {

    // claves compartida de seseion y introducion
    public static final String LOGIN = "login";
    public static final String INTRODUTION = "introdution";

    //session
    public static final String SESSION = "session";

    //account
    public static final String ACCOUNT = "account";

    //token
    public static final String TOKEN = "token";
    public static final String REFRESH = "refresh";

    //account
    public static String KEY_SUCCESS = "success";
    public static String KEY_ERROR = "error";
    public static String KEY_ERROR_MSG = "error_msg";
    public static final String ADD_ACCOUNT_TYPE = "co.usetime.account";
    public static final String AUTH_TOKEN_TYPE = "AuthTokenType";

    public static final String KEY_USERNAME = "UserName";
    public static final String KEY_USERFULLNAME = "UserFullName";
    public static final String KEY_AVATARURL = "AvatarUrl";
    public static final String KEY_USERTOKEN = "UserToken";

    public static final String KEY_ID = "KEY_ID";
    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_EMAIL = "KEY_EMAIL";
    public static final String KEY_ENTERPRISE_ID = "KEY_ENTERPRISE_ID";
    public static final String KEY_STATUS = "KEY_STATUS";
    public static final String KEY_CREATE_AT = "KEY_CREATE_AT";
    public static final String KEY_UPDATED_AT = "KEY_UPDATED_AT";
    public static final String KEY_AVATAR_URL = "KEY_AVATAR_URL";
    public static final String KEY_ROLE_ID = "KEY_ROLE_ID";
    public static final String KEY_ROLE = "KEY_ROLE";
    public static final String KEY_PERMISSION = "KEY_PERMISSION";

    //path folde
    public static final String PATH_BASE = "UseTime/";
    public static final String PATH_SCREENSHOT = "Screenshot/";
    public static final String PATH_FILES = "Files/";
    public static final String PATH_CAMERA = "Camera/";
    public static final String SCREENCAP = "/system/bin/screencap -p ";
    public static final String SDCARD = "/sdcard/";
    public static final String PNG = ".png";
    public static final String JPG = ".jpg";
    public static final String ASCII = "ASCII";

    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";


    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    private static final int UPDATE_INTERVAL_IN_SECONDS = 30;
    // Update frequency in milliseconds
    public static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 30;
    // A fast frequency ceiling in milliseconds
    public static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
    // Stores the lat / long pairs in a text file
    public static final String LOCATION_FILE = "sdcard/location.txt";
    // Stores the connect / disconnect data in a text file
    public static final String LOG_FILE = "sdcard/log.txt";

    /**
     * Suppress default constructor for noninstantiability
     */
    private Constants() {
        throw new AssertionError();
    }

    //dialog gps
    public static final String NO_DIALOG_SHOW = "NO_DIALOG_SHOW";
    public static final String NO_DIALOG_SHOW2 = "NO_DIALOG_SHOW2";

    //Task status
    public static final int DELETED = -1;
    public static final int INACTIVE = 0;
    public static final int PENDING = 1;
    public static final int PROGRESS = 2;
    public static final int COMPLETED = 3;
    public static final int VALIDATED = 4;
    public static final int EXPIRED = 5;
    public static final int REJECTED = 6;

    //permission
    public static final int REQUEST_CODE_STORAGE = 801;

    public static final int REQUEST_CODE_CAMERA = 804;

    public static final int REQUEST_CODE_LOCATION = 808;
    public static final int REQUEST_CODE_FINE_LOCATION = 809;
    public static final int REQUEST_CODE_COARSE_LOCATION = 810;

    public static final String CODE_LOCATION = "CODE_LOCATION";

    //process
    public static final String IS_CLASS = "IS_CLASS";
    public static final String IS_CLASS_DATE = "IS_CLASS_DATE";
    public static final String IS_CLASS_TIME = "IS_CLASS_TIME";

    public static final String IS_PACKAGE = "IS_PACKAGE";
    public static final String IS_PACKAGE_NAME = "IS_NAME";
    public static final String IS_PACKAGE_DATE = "IS_DATE";
    public static final String IS_PACKAGE_TIME = "IS_TIME";
    public static final String IS_PACKAGE_DURATION_DATE = "IS_PACKAGE_DURATION_DATE";
    public static final String IS_PACKAGE_DURATION_TIME = "IS_PACKAGE_DURATION_TIME";
    public static final String IS_PACKAGE_DURATION = "IS_DURATION";
    public static final String IS_PACKAGE_PATH = "IS_PATH";

}// endClass