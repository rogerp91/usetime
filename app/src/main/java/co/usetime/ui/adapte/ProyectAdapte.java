package co.usetime.ui.adapte;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.mvp.model.Proyect;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Roger Patiño on 22/02/2016.
 */
public class ProyectAdapte extends RecyclerView.Adapter<ProyectAdapte.ViewHolder> {
    protected static String TAG = ProyectAdapte.class.getSimpleName();

    private List<Proyect> proyectList = null;
    private Context context;

    //var
    private OnItemClickListener listener;
    private PopupMenuListener popupMenuListener;

    public ProyectAdapte(List<Proyect> alist) {
        this.proyectList = alist;
        context = UIApp.getContext;
    }

    //interfaces
    public interface OnItemClickListener {
        void onItemClick(RecyclerView.ViewHolder item, int position);
    }

    public interface PopupMenuListener {
        void onPopupMenuClicked(MenuItem menuItem, int adapterPosition);
    }

    //setter and getter
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return listener;
    }

    public void setOnItemClickListenerPopupMenu(PopupMenuListener listener) {
        this.popupMenuListener = listener;
    }

    public PopupMenuListener getOnItemClickListenerPopupMenu() {
        return popupMenuListener;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Campos respectivos de un item
        private TextView mName;
        private ImageView icCheck;
        private ImageView icThumbs;
        private ImageView icReflesh;
        private ImageView icClock;
        private TextView num_check;
        private TextView num_thumbs;
        private TextView num_reflesh;
        private TextView num_clock;
        private TextView num_task;
        private TextView title;
        private TextView num_acumulad;
        private TextView num_time;
        private ImageView ic_clock2;
        private ImageView ic_dollar;
        private CircleImageView fab2;
        private ProyectAdapte father = null;

        public ViewHolder(View view, ProyectAdapte father) {
            super(view);
            this.father = father;
            view.setOnClickListener(this);
            mName = (TextView) view.findViewById(R.id.title);
            icCheck = (ImageView) view.findViewById(R.id.ic_check);
            icThumbs = (ImageView) view.findViewById(R.id.ic_thumbs);
            icReflesh = (ImageView) view.findViewById(R.id.ic_reflesh);
            icClock = (ImageView) view.findViewById(R.id.ic_clock);
            ic_dollar = (ImageView) view.findViewById(R.id.ic_dollar);
            ic_clock2 = (ImageView) view.findViewById(R.id.ic_clock2);
            num_check = (TextView) view.findViewById(R.id.num_check);
            num_clock = (TextView) view.findViewById(R.id.num_clock);
            num_reflesh = (TextView) view.findViewById(R.id.num_reflesh);
            num_task = (TextView) view.findViewById(R.id.num_task);
            num_thumbs = (TextView) view.findViewById(R.id.num_thumbs);

            num_acumulad = (TextView) view.findViewById(R.id.num_acumulad);
            num_time = (TextView) view.findViewById(R.id.num_time);

            num_time = (TextView) view.findViewById(R.id.num_time);
            num_time = (TextView) view.findViewById(R.id.num_time);

            fab2 = (CircleImageView) view.findViewById(R.id.fab2);

        }

        @Override
        public void onClick(View v) {
            final ProyectAdapte.OnItemClickListener listener = father.getOnItemClickListener();
            if (listener != null) {
                listener.onItemClick(this, getAdapterPosition());
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_proyect2, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Proyect proyect = proyectList.get(position);
        if (proyect.getType().equals("project")) {

            Glide.with(context).load(HttpRestClient.BASE_URL + HttpRestClient.VERSION + "profile/" + proyect.getCreator_id() + "/avatar")
                    .placeholder(R.drawable.profile_default)
                    .error(R.drawable.error_128)
                    .into(holder.fab2);

            holder.mName.setText(proyect.getName());
            if (proyect.getTotal_expired() != null) {
                try {
                    holder.num_clock.setText(proyect.getTotal_expired());
                } catch (NullPointerException e) {
                    holder.num_clock.setText("0");
                }
            } else {
                holder.num_clock.setText("0");
            }

            if (proyect.getTotal_process() != null) {
                try {
                    holder.num_reflesh.setText(proyect.getTotal_process());
                } catch (NullPointerException e) {
                    holder.num_reflesh.setText("0");
                }
            } else {
                holder.num_reflesh.setText("0");
            }

            if (proyect.getTotal_delivered() != null) {
                try {
                    holder.num_thumbs.setText(proyect.getTotal_delivered());

                } catch (NullPointerException e) {
                    holder.num_thumbs.setText("0");
                }
            } else {
                holder.num_thumbs.setText("0");
            }

            if (proyect.getTotal_delivered_validate() != null) {
                try {
                    holder.num_check.setText(proyect.getTotal_delivered_validate());
                } catch (NullPointerException e) {
                    holder.num_check.setText("0");
                }
            } else {
                holder.num_check.setText("0");
            }

            if (proyect.getTotal_task() != null) {
                try {
                    holder.num_task.setText(proyect.getTotal_task());
                } catch (NullPointerException e) {
                    holder.num_task.setText("0");
                }
            } else {
                holder.num_task.setText("0");
            }

            if (proyect.getAcumulado() != null) {
                try {
                    holder.num_acumulad.setText(proyect.getAcumulado());
                } catch (NullPointerException e) {
                    holder.num_acumulad.setText("0");
                }
            } else {
                holder.num_acumulad.setText("0");
            }

            if (proyect.getTotal_time() != null) {
                try {
                    holder.num_time.setText(proyect.getTotal_time());
                } catch (NullPointerException e) {
                    holder.num_time.setText("0");
                }
            } else {
                holder.num_time.setText("0");
            }

            holder.icCheck.setColorFilter(ContextCompat.getColor(context, R.color.card_proyect_1));
            holder.icThumbs.setColorFilter(ContextCompat.getColor(context, R.color.card_proyect_2));
            holder.icReflesh.setColorFilter(ContextCompat.getColor(context, R.color.card_proyect_3));
            holder.icClock.setColorFilter(ContextCompat.getColor(context, R.color.card_proyect_4));

            holder.ic_clock2.setColorFilter(ContextCompat.getColor(context, R.color.card_proyect_5));
            holder.ic_dollar.setColorFilter(ContextCompat.getColor(context, R.color.card_proyect_5));
        }
    }

    @Override
    public int getItemCount() {
        return (proyectList != null ? proyectList.size() : 0);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(proyectList.get(position).getId());
    }

}