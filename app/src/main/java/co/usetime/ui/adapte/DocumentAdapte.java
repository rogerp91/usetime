package co.usetime.ui.adapte;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.otto.Bus;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.mvp.model.DocumentFile;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Roger Patiño on 08/06/2016.
 */
public class DocumentAdapte extends RecyclerView.Adapter<DocumentAdapte.ViewHolder> {

    protected static String TAG = DocumentAdapte.class.getSimpleName();
    private static Bus bus;
    private static List<DocumentFile> documentFiles;
    private Context context;
    private OnItemClickListener listener;

    public DocumentAdapte(List<DocumentFile> alist) {
        this.documentFiles = alist;
        this.context = UIApp.getContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_document, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DocumentFile documentFile = documentFiles.get(position);
        holder.name.setText(documentFile.getName());
        String url = HttpRestClient.BASE_URL + HttpRestClient.VERSION + "profile/" + documentFile.getId() + "/avatar";
        Glide.with(context).load(url).placeholder(R.drawable.usetime_logo).error(R.drawable.error_128).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return documentFiles.size();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(documentFiles.get(position).getId());
    }

    //interfaces
    public interface OnItemClickListener {
        void onItemClick(RecyclerView.ViewHolder item, int position);
    }

    //setter and getter
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.img)
        CircleImageView img;
        private DocumentAdapte father = null;

        public ViewHolder(View view, DocumentAdapte father) {
            super(view);
            this.father = father;
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final OnItemClickListener listener = father.getOnItemClickListener();
            if (listener != null) {
                listener.onItemClick(this, getAdapterPosition());
            }
        }
    }

}