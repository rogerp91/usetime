package co.usetime.ui.adapte;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.usetime.R;
import co.usetime.UIApp;

/**
 * Created by Roger Patiño on 01/02/2016.
 */
public class ContactAdapte extends RecyclerView.Adapter<ContactAdapte.ViewHolder> {

    private static List<String> stringList;
    private Context context;
    private boolean type;

    public ContactAdapte(List<String> alist) {
        this.stringList = alist;
        context = UIApp.getContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String url = stringList.get(position);
        if (!url.isEmpty()) {
            Glide.with(context).load(url).into(holder.icContact);
        }
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView icContact;
        private ContactAdapte father = null;

        public ViewHolder(View view, ContactAdapte father) {
            super(view);
            this.father = father;
            icContact = (ImageView) view.findViewById(R.id.circle_contact);
        }
    }
}