package co.usetime.ui.adapte;

import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;

import co.usetime.R;
import co.usetime.mvp.model.ItemTodo;

/**
 * Created by Roger Patiño on 25/04/2016.
 */
public class TodoAdapte2 extends DragItemAdapter<Pair<Long, ItemTodo>, TodoAdapte2.ViewHolder> {

    protected static String TAG = TodoAdapte2.class.getSimpleName();

    private int mLayoutId;
    private int mGrabHandleId;

    public TodoAdapte2(ArrayList<Pair<Long, ItemTodo>> list, int layoutId, int grabHandleId, boolean dragOnLongPress) {
        super(dragOnLongPress);
        mLayoutId = layoutId;
        mGrabHandleId = grabHandleId;
        setHasStableIds(true);
        setItemList(list);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        ItemTodo itemTodo = mItemList.get(position).second;
        holder.mText.setText(itemTodo.getName());
        holder.mDate.setText(itemTodo.getDate());
        //holder.mName.setText(itemTodo.getPerson());
    }

    @Override
    public long getItemId(int position) {
        return mItemList.get(position).first;
    }

    public class ViewHolder extends DragItemAdapter<Pair<Long, ItemTodo>, ViewHolder>.ViewHolder {

        public TextView mText;
        public TextView mDate;
        public TextView mName;

        public ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId);
            mText = (TextView) itemView.findViewById(R.id.text);
            mDate = (TextView) itemView.findViewById(R.id.date);
        }

        @Override
        public void onItemClicked(View view) {
            Toast.makeText(view.getContext(), "Item clicked", Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean onItemLongClicked(View view) {
            Toast.makeText(view.getContext(), "Item long clicked", Toast.LENGTH_SHORT).show();
            return true;
        }
    }
}