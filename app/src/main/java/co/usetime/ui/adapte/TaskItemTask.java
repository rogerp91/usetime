package co.usetime.ui.adapte;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.otto.Bus;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.domain.repository.interfaces.Repository;
import co.usetime.mvp.model.Permissions;
import co.usetime.mvp.model.Tasks;
import co.usetime.mvp.model.TasksOfProject;
import co.usetime.utils.Constants;
import co.usetime.utils.Prefs;

/**
 * Created by Roger Patiño on 02/03/2016.
 */
public class TaskItemTask extends RecyclerView.Adapter<TaskItemTask.ViewHolder> {
    protected static String TAG = TaskItemTask.class.getSimpleName();

    private static Bus bus;
    private static List<TasksOfProject> tasksList;
    List<Permissions> permissionses;
    private Context context;
    private boolean type;
    private Repository<Tasks> repository = null;

    //dialog
    private SwitchCompat flowchart = null;
    private EditText input_name = null;
    private TextInputLayout input_layout_name = null;
    private boolean isSwitchActive = false;
    private boolean isFlowchart = false;

    //spinner
    private String selection = "";

    //time and date
    private String date_1;
    private String date_2 = "";
    private String time_1;
    private String time_2;
    protected LocationManager locationManager;
    private String enterprises = Prefs.getString(Constants.KEY_ENTERPRISE_ID, ""); //id del empresa
    //Log.d(TAG, "enterprises: " + enterprises);
    private String token = Prefs.getString(Constants.TOKEN, "");//token

    /**
     * @param alist lista que se pasa por el constructor
     */
    public TaskItemTask(List<TasksOfProject> alist, List<Permissions> listPermissionses) {
        this.tasksList = alist;
        this.context = UIApp.getContext;
        this.permissionses = listPermissionses;
        this.enterprises = Prefs.getString(Constants.KEY_ENTERPRISE_ID, ""); //id del empresa
        this.token = Prefs.getString(Constants.TOKEN, "");//token
    }

    //var
    private OnItemClickListener listener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_task, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TasksOfProject ofProject = tasksList.get(position);
        holder.icType.setColorFilter(ContextCompat.getColor(context, R.color.warning));
        if (!ofProject.getName().equals("")) {
            holder.title.setText(ofProject.getName());
        }
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(tasksList.get(position).getId());
    }

    //interfaces
    public interface OnItemClickListener {
        void onItemClick(RecyclerView.ViewHolder item, int position);
    }

    //setter and getter
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.date)
        TextView date;
        @Bind(R.id.time)
        TextView time;
        @Bind(R.id.ic_type)
        ImageView icType;
        private TaskItemTask father = null;

        public ViewHolder(View view, TaskItemTask father) {
            super(view);
            this.father = father;
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final OnItemClickListener listener = father.getOnItemClickListener();
            if (listener != null) {
                listener.onItemClick(this, getAdapterPosition());
            }
        }
    }
}