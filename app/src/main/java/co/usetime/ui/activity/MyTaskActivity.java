package co.usetime.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.usetime.R;
import co.usetime.componets.ProgressWheel;
import co.usetime.di.SnackbarModule;
import co.usetime.domain.repository.interfaces.DocumentRepository;
import co.usetime.mvp.model.TasksOfProject;
import co.usetime.mvp.presenter.interfaces.ShowMsgPresenter;
import co.usetime.mvp.presenter.interfaces.TaskPresenter;
import co.usetime.mvp.view.MyTaskView;
import co.usetime.ui.adapte.TaskItemTask;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;

public class MyTaskActivity extends BaseActivity implements MyTaskView, TaskItemTask.OnItemClickListener {

    protected static String TAG = MyTaskActivity.class.getSimpleName();
    public static Activity ACTIVITY = null;
    public static View VIEW_ACTIVITY;

    @Inject
    ShowMsgPresenter msgPresenter;
    @Inject
    TaskPresenter presenter;
    @Inject
    DocumentRepository documentRepository;

    //view
    @Bind(R.id.container)
    RelativeLayout container;

    @Bind(R.id.recycler_task)
    RecyclerView recycler;
    @Bind(R.id.progress)
    ProgressWheel mProgressView;
    @Bind(R.id.layout_no_content)
    RelativeLayout layout_noContent;
    @Bind(R.id.layout_connection)
    RelativeLayout layout_Connection;
    @Bind(R.id.layout_no_task)
    RelativeLayout layout_no_task;

    @Bind(R.id.layout_code_http)
    RelativeLayout layout_code_http;

    @Bind(R.id.code_http)
    TextView code_http;

    private LinearLayoutManager linearManager;
    private TaskItemTask adaptador;
    private int task;
    private List<TasksOfProject> proyectList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        ButterKnife.bind(this);

        ACTIVITY = MyTaskActivity.this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        VIEW_ACTIVITY = container;

        recycler.setHasFixedSize(true);
        linearManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearManager);

        presenter.setView(this);

        if (getIntent() != null) {
            //Log.d(TAG, "GETINTET:" + Integer.toString(getIntent().getExtras().getInt("ID")));
            this.task = getIntent().getExtras().getInt("ID");
        } else {
            finish();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, Integer.toString(task));
        presenter.getList(Integer.toString(task));
    }

    @Override
    protected List<Object> getModules() {
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void showMsg(String mgs, int color) {
        msgPresenter.showMsgShort(mgs, color);
    }

    @Override
    public void adapteList(List<TasksOfProject> list) {
        DetailTaskActivity.IS_CHECK_GET = false;
        if (list.size() == 0) {
            msgPresenter.showMsgShort("No existe contenido", ContextCompat.getColor(getApplicationContext(), R.color.alert));
        } else {
            proyectList = list;
            String per = Prefs.getString(Constants.KEY_PERMISSION, "");
            adaptador = new TaskItemTask(list, Functions.JSONToList(per));
            adaptador.setOnItemClickListener(this);
            adaptador.setHasStableIds(true);
            recycler.setAdapter(adaptador);
            proyectList = list;
        }
    }

    @Override
    public void showViewProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void displayLayoutConnection(final boolean connection) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
        layout_Connection.animate().setDuration(shortAnimTime).alpha(connection ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void displayLayoutNocontent(final boolean content) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
        layout_noContent.animate().setDuration(shortAnimTime).alpha(content ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void displayLayoutNoTask(final boolean task) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_no_task.setVisibility(task ? View.VISIBLE : View.GONE);
        layout_no_task.animate().setDuration(shortAnimTime).alpha(task ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_no_task.setVisibility(task ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void displayError() {

    }

    @Override
    public void displayLayoutError(final boolean status, final String msg) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_code_http.setVisibility(status ? View.VISIBLE : View.GONE);
        layout_code_http.animate().setDuration(shortAnimTime).alpha(status ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_code_http.setVisibility(status ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public View getView() {
        return container;
    }

    @OnClick(R.id.text_try_again_http)
    public void TryAgainError() {
        presenter.setView(this);
        presenter.getList();
    }

    @Override
    public void onItemClick(RecyclerView.ViewHolder item, int position) {
        String pos = proyectList.get(position).getId();
        String name = proyectList.get(position).getName();
        Bundle bundle = new Bundle();
        bundle.putString("pos", pos);
        bundle.putString("name", name);
        startActivity(new Intent(getApplicationContext(), DetailTaskActivity.class).putExtras(bundle).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}