package co.usetime.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;

import co.usetime.R;
import co.usetime.ui.fragment.SampleSlide;
import co.usetime.utils.Constants;
import co.usetime.utils.Prefs;

public class IntroActivity extends AppIntro {
    protected static String TAG = IntroActivity.class.getSimpleName();

    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(SampleSlide.newInstance(R.layout.fragment_intro1));
        addSlide(SampleSlide.newInstance(R.layout.fragment_intro2));
        addSlide(SampleSlide.newInstance(R.layout.fragment_intro3));
        addSlide(SampleSlide.newInstance(R.layout.fragment_intro4));
    }

    @Override
    public void onSkipPressed() {
        loadMainActivity();
    }

    @Override
    public void onNextPressed() {

    }

    @Override
    public void onDonePressed() {
        loadMainActivity();
    }

    @Override
    public void onSlideChanged() {

    }

    //goto login
    private void loadMainActivity() {
        Prefs.putBoolean(Constants.INTRODUTION, true);
        startActivity(new Intent(this, LoginActivity.class));
    }
}