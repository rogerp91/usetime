package co.usetime.ui.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.vi.swipenumberpicker.SwipeNumberPicker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.usetime.BuildConfig;
import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.di.SnackbarModule;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.domain.repository.imp.ProyectRepositoryImp;
import co.usetime.domain.repository.imp.TaskRepositoryImp;
import co.usetime.domain.repository.imp.TodoRepositoryImp;
import co.usetime.mvp.model.Permissions;
import co.usetime.mvp.model.ProfileView;
import co.usetime.mvp.presenter.interfaces.ShowMsgPresenter;
import co.usetime.mvp.view.MainView;
import co.usetime.services.LocationService;
import co.usetime.services.ProcessService;
import co.usetime.services.TrafficService;
import co.usetime.services.UploadProcessService;
import co.usetime.ui.fragment.ProyectFragment;
import co.usetime.ui.fragment.TodoFragment;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;
import kr.co.namee.permissiongen.PermissionSuccess;

public class MainActivity extends BaseActivity implements MainView {
    protected static String TAG = MainActivity.class.getSimpleName();

    public static final int MEDIA_TYPE_IMAGE = 1;
    private Uri fileUri; // file url to store image/video
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    public static Activity ACTIVITY = null;
    public static View MAIN_ACTIVITY_VIEW;
    //drawer
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.nav_view)
    NavigationView navigationView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.contentLayout)
    LinearLayout layout;
    private String drawerTitle;

    ProfileView findProfile;
    MaterialDialog dialog = null;
    LogOut logOut;

    @Inject
    ShowMsgPresenter msgPresenter;

    //dialog
    SwipeNumberPicker swipeNumberPickerHour;
    SwipeNumberPicker swipeNumberPickerMin;
    EditText description;

    protected LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        accountExits();//verificar si existe un cuenta
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_Usetime);//style
        setContentView(R.layout.activity_main);
        ACTIVITY = MainActivity.this;//<- layout
        MAIN_ACTIVITY_VIEW = layout;
        ButterKnife.bind(this);

        //toolbar
        setToolbar();

        //drawer
        if (navigationView != null) {//set nav
            String json = Prefs.getString(Constants.KEY_PERMISSION, "");
            if (!json.equals("")) {
                List<Permissions> permissionses = Functions.JSONToList(json);
                //Log.d(TAG, Integer.toString(permissionses.size()));
                if (Functions.getPermission("11", permissionses)) {
                    List<Integer> integers = new ArrayList<>();
                    if (!Functions.getPermission("12", permissionses)) {
                        MenuItem item1 = navigationView.getMenu().getItem(0);
                        item1.setVisible(false);
                    }

                    if (!Functions.getPermission("15", permissionses)) {
                        MenuItem item2 = navigationView.getMenu().getItem(1);
                        item2.setVisible(false);
                    }
                    setupDrawerContent(navigationView);

                } else {
                    setupDrawerContent(navigationView);

                }
            }
        }
        drawerTitle = getResources().getString(R.string.home_item);
        if (savedInstanceState == null) {
            selectItem(drawerTitle);
        }

        //add profiles
        View headerNav = navigationView.inflateHeaderView(R.layout.nav_header);//inflar el header del drawer
        findProfile = new ProfileView();//asignar los vista al objeto
        findProfile.setUsername((TextView) headerNav.findViewById(R.id.username));
        findProfile.setEmail((TextView) headerNav.findViewById(R.id.email));
        findProfile.setCircleImageView((CircleImageView) headerNav.findViewById(R.id.circle_image));
        if (Prefs.getBoolean(Constants.SESSION, false)) {//verificar si la session fue establecida
            showProfile(findProfile);
        }

        //Intent intent = new Intent();
        //intent.setComponent(new ComponentName("co.usetime.monitoring", "co.usetime.monitoring.ui.activity.MainActivity"));
        //intent.addCategory("android.intent.category.LAUNCHER");
        //startActivity(intent);
        boolean session = Prefs.getBoolean(Constants.SESSION, false);
        if (session) {
            //and fragment
            setFragment(0, getResources().getString(R.string.todo_item));
            if (!LocationService.INSTANCE) {
                LocationService.actionStart(this);
            }

            if (!ProcessService.INSTANCE) {
                ProcessService.actionStart(this);
            }

            if (!UploadProcessService.INSTANCE) {
                UploadProcessService.actionStart(this);
            }

            TrafficService.actionStart();
        }

        if (Functions.isMarshmallow()) {
            PermissionGen.with(MainActivity.this).addRequestCode(100).permissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION).request();
        }

    }

    @PermissionSuccess(requestCode = 100)
    public void locationSucess() {

    }

    @PermissionFail(requestCode = 100)
    private void locationFail() {
        //Toast.makeText(getApplicationContext(), "PermissionFail", Toast.LENGTH_SHORT).show();
        Snackbar snack = Snackbar.make(layout, "Se necesiatan permisos especiales!", Snackbar.LENGTH_INDEFINITE);
        snack.getView().setBackgroundColor(ContextCompat.getColor(UIApp.getContext, R.color.info));
        snack.setAction("Configurariones", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(i);
            }
        });
        snack.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected List<Object> getModules() {
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public View getView() {
        return layout;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        invalidateOptionsMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    //···········································Private······································
    private void setToolbar() {//add toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(BuildConfig.NAME);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.mipmap.ic_menu_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    //verificar si exite una cuenta en el telefono de usetime
    private void accountExits() {//validar si existe una cuenta
        //buscar los valores locales
        boolean account = Prefs.getBoolean(Constants.ACCOUNT, false);
        boolean intro = Prefs.getBoolean(Constants.INTRODUTION, false);
        boolean session = Prefs.getBoolean(Constants.SESSION, false);
        //Log.d(TAG, Prefs.getString(Constants.TOKEN, ""));
        //verificar si hay una cuenta usetime en el dispositivo
        if (UIApp.getAccounts()) {//si exite una cuenta
            if (!account || !session) {//si no hay datos locales o no hay session, se borraron los datos del telefono y no se cerro session

                Prefs.putBoolean(Constants.ACCOUNT, true);
                Prefs.putBoolean(Constants.INTRODUTION, true);
                Prefs.putBoolean(Constants.SESSION, true);

                //adminitrador de cuenta
                AccountManager accountManager = AccountManager.get(this);
                Account accountUser = UIApp.getUserAccount();
                Prefs.putString(Constants.KEY_USERNAME, accountManager.getUserData(accountUser, Constants.KEY_NAME));
                Prefs.putString(Constants.TOKEN, accountManager.getUserData(accountUser, Constants.TOKEN));
                Prefs.putString(Constants.KEY_ENTERPRISE_ID, accountManager.getUserData(accountUser, Constants.KEY_ENTERPRISE_ID));
                Prefs.putString(Constants.KEY_ID, accountManager.getUserData(accountUser, Constants.KEY_ID));
                Prefs.putString(Constants.KEY_USERNAME, accountManager.getUserData(accountUser, Constants.KEY_USERNAME));

                //v3.0.3.33
                Prefs.putString(Constants.KEY_PERMISSION, accountManager.getUserData(accountUser, Constants.KEY_PERMISSION));

                Log.d(TAG, Prefs.getString(Constants.TOKEN, ""));
            }
        } else {//si no existe un cuenta
            if (account || session) {
                Prefs.putBoolean(Constants.ACCOUNT, false);
                Prefs.putBoolean(Constants.INTRODUTION, true);
                Prefs.putBoolean(Constants.SESSION, false);
                finish();
                startActivity(new Intent(this, LoginActivity.class));
            } else {
                if (!intro && !session) {// si el intro y login es vacio, esta iniciando la app por primera vez y no hoy cuenta
                    finish();
                    startActivity(new Intent(this, IntroActivity.class));//enviar al intro
                } else {
                    if (intro && !session) {//si el intro tiene valor y el login no tiene valor entonces paso por el intro pero no se la logiado
                        finish();
                        startActivity(new Intent(this, LoginActivity.class));
                    }
                }
            }
        }
    }

    //menu drawer
    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        String title = menuItem.getTitle().toString();
                        switch (menuItem.getItemId()) {
                            /**
                            case R.id.nav_tack:
                                menuItem.setChecked(true);
                                setFragment(0, title);
                                selectItem(title);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;

                             case R.id.nav_calenda:
                             menuItem.setChecked(true);
                             setFragment(1, title);
                             selectItem(title);
                             drawerLayout.closeDrawer(GravityCompat.START);
                             return true;
                             */
                            case R.id.nav_todo:
                                menuItem.setChecked(true);
                                setFragment(1, title);
                                selectItem(title);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.nav_setting:
                                drawerLayout.closeDrawer(GravityCompat.START);
                                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                return true;
                            case R.id.nav_log_out:
                                logOut();
                                return true;
                        }
                        return true;
                    }
                }
        );
    }

    //ir a los fragment
    private void setFragment(int position, String name) {
        switch (position) {
            case 0:
                gotoFragment(TodoFragment.newInstance());
                //gotoFragment(ProyectFragment.newInstance(name));
                //gotoFragment(TaskFragment.newInstance());
                break;
            /**
             case 1:
             gotoFragment(CalendarFragment.newInstance(name));
             break;
             */
            case 1:
                //gotoFragment(TodoFragment.newInstance());
                break;
        }
    }

    //salir
    private void logOut() {
        dialog = new MaterialDialog.Builder(this).title(R.string.log_out_title).content(R.string.log_out_content).progress(true, 0).show();
        logOut = new LogOut();
        logOut.execute((Void) null);
    }

    //ir a los fragment
    private void setFragment(int position, String name, List<Integer> integers) {
        switch (position) {
            case 0:
                gotoFragment(TodoFragment.newInstance());
                //gotoFragment(ProyectFragment.newInstance(name));
                //gotoFragment(TaskFragment.newInstance());
                break;
            /**
             case 1:
             gotoFragment(CalendarFragment.newInstance(name));
             break;
             */
            case 1:
                //gotoFragment(TodoFragment.newInstance());
                break;
        }
    }

    //ir al fragment
    private void gotoFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_content, fragment).setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                //.addToBackStack(null)
                .commit();
    }

    //hilo para salir
    private class LogOut extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            AccountManager accountManager = AccountManager.get(MainActivity.this);
            Account accountDelete = null;
            //listado de cuenta
            Account[] accounts = accountManager.getAccountsByType(getResources().getString(R.string.account_type));
            for (Account account : accounts) {
                accountDelete = account;
            }
            accountManager.removeAccount(accountDelete, null, null);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Prefs.clear();
            Prefs.putBoolean(Constants.INTRODUTION, true);
            new ProyectRepositoryImp().clear();
            new TaskRepositoryImp().clear();
            new TodoRepositoryImp().clear();
            LocationService.actionStop2(getApplicationContext());
            return true;
        }


        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool) {
                logOut.isCancelled();
                dialog.dismiss();
                finish();
                startActivity(new Intent(UIApp.getContext, LoginActivity.class));
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    //selecionar el item
    private void selectItem(String title) {
        drawerLayout.closeDrawers();
        setTitle(title);
    }

    //mostar perfil
    private void showProfile(ProfileView profile) {
        AccountManager accountManager = AccountManager.get(MainActivity.this);
        Account account = UIApp.getUserAccount();
        if (UIApp.getAccounts()) {
            profile.getUsername().setText(accountManager.getUserData(account, Constants.KEY_NAME));
            profile.getEmail().setText(accountManager.getUserData(account, Constants.KEY_EMAIL));
            //String url = accountManager.getUserData(account, Constants.KEY_AVATAR_URL);
            //https://usetimebeta.ingeniouskey.com/api/v1/profile/1/avatar
            String id = accountManager.getUserData(account, Constants.KEY_ID);
            String url2 = BuildConfig.HOST + HttpRestClient.VERSION + "profile/" + id + "/avatar";
            Log.d(TAG, "profileURL:" + url2);
            Glide.with(this).load(url2)
                    .placeholder(R.drawable.profile_default)
                    .error(R.drawable.error_128)
                    .into(profile.getCircleImageView());

        } else {
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            startActivity(new Intent(this, LoginActivity.class));
        }
    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        if (!Functions.isMarshmallow()) {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                Log.d(TAG, "!isGPSEnabled && !isNetworkEnabled");
                showDialog();
            } else {
                //Log.d(TAG, "active");
            }
        } else {
            if (Functions.hasPermission("android.permission.ACCESS_FINE_LOCATION") || Functions.hasPermission("android.permission.ACCESS_COARSE_LOCATION")) {
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    Log.d(TAG, "!isGPSEnabled && !isNetworkEnabled");
                    showDialog();
                } else {
                    //Log.d(TAG, "active");
                }
            }
        }
    }

    public void showDialog() {
        if (!Prefs.getBoolean(Constants.NO_DIALOG_SHOW, false)) {
            new MaterialDialog.Builder(this).title(R.string.setting_gps_title).
                    content(R.string.setting_gps_content)
                    .positiveText(R.string.setting_gps_agree)
                    .negativeText(R.string.setting_gps_disagree)
                    .neutralText(R.string.setting_gps_neutal)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    }).onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    dialog.dismiss();
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }).onNeutral(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    Prefs.putBoolean(Constants.NO_DIALOG_SHOW, true);
                }
            }).show();
        }
    }

    public void file() {
        File dir = new File("/proc/uid_stat/");
        String[] children = dir.list();
        List<Integer> uids = new ArrayList<>();
        if (children != null) {
            for (int i = 0; i < children.length; i++) {
                int uid = Integer.parseInt(children[i]);
                String uidString = String.valueOf(uid);
                File uidFileDir = new File("/proc/uid_stat/" + uidString);
                File uidActualFile = new File(uidFileDir, "tcp_rcv");
                StringBuilder text = new StringBuilder();
                try {
                    BufferedReader br = new BufferedReader(new FileReader(uidActualFile));
                    String line;
                    while ((line = br.readLine()) != null) {
                        Log.d(String.valueOf(uid), line);//this returns the amount of data received for the particular uid
                        text.append(line);
                        text.append('\n');
                    }
                } catch (IOException e) {
                    //handle this
                }
                uids.add(uid);
            }
        }


    }
}