package co.usetime.ui.activity;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.componets.ProgressWheel;
import co.usetime.di.SnackbarModule;
import co.usetime.mvp.model.Profile;
import co.usetime.mvp.presenter.interfaces.LoginPresenter;
import co.usetime.mvp.presenter.interfaces.ShowMsgPresenter;
import co.usetime.mvp.view.LoginView;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;

/**
 * Created by Roger Patiño on 16/02/2016.
 */
public class LoginActivity extends BaseActivity implements LoginView {
    protected static String TAG = LoginActivity.class.getSimpleName();
    public static boolean isInFront;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    public static Activity ACTIVITY = null;

    //layout para snackbar
    @Bind(R.id.linearLayout_container)
    LinearLayout linearLayout;

    //vista
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    //input
    @Bind(R.id.input_email)
    EditText inputEmail;
    @Bind(R.id.input_password)
    EditText inputPassword;

    @Bind(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;
    @Bind(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;

    @Bind(R.id.scroll_form)
    View mForm;
    @Bind(R.id.progress)
    ProgressWheel mProgress;

    @Inject
    ShowMsgPresenter msgPresenter;

    @Inject
    LoginPresenter presenter;

    private AccountManager accountManager = null;
    private AccountAuthenticatorResponse response = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        ACTIVITY = LoginActivity.this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        accountManager = AccountManager.get(getApplicationContext());
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            if (extras.getParcelable(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE) != null) {
                response = getIntent().getExtras().getParcelable(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);
            }
        }

        if (!UIApp.checkPlayServices()) {
            new MaterialDialog.Builder(this).content("Google Play Services no esta instalado").positiveText("Salir").negativeText("Instalar")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms&hl=es_419")));
                        }
                    })
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            finish();
                        }
                    }).show();
        }

        //Watcher
        inputEmail.addTextChangedListener(textWatcher);
        inputPassword.addTextChangedListener(textWatcher);
    }

    @Override
    protected List<Object> getModules() {
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public View getView() {
        return linearLayout;
    }

    @Override
    public void onSuccess(Profile profile) {
        UIApp app = (UIApp) getApplicationContext();
        app.createFolders();
        if (profile != null) {
            newAccount(profile);
        }
    }

    @Override
    public void showView(final boolean show) {
        if (mForm != null && mProgress != null) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mForm.setVisibility(show ? View.GONE : View.VISIBLE);
            mForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgress.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
    }


    @Override
    public void displayMsg(String msg, int color) {
        if (isInFront) {
            msgPresenter.showMsgShort(msg, color);
        }
    }

    @Override
    public void gotoMain(Intent i) {
        msgPresenter.showMsgShort(getResources().getString(R.string.validate), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayErrorData() {
        msgPresenter.showMsgShort(getResources().getString(R.string.validate), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayError() {
        msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayErrorConnection() {
        msgPresenter.showMsgShort(getResources().getString(R.string.no_connection2), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayInfoUser() {
        msgPresenter.showMsgShort(getResources().getString(R.string.not_empty_user), ContextCompat.getColor(getApplicationContext(), R.color.info));
    }

    @Override
    public void displayInfoUserSize() {
        msgPresenter.showMsgShort(getResources().getString(R.string.min_length_user), ContextCompat.getColor(getApplicationContext(), R.color.info));
    }

    @Override
    public void displayInfoPassword() {
        msgPresenter.showMsgShort(getResources().getString(R.string.not_empty_password), ContextCompat.getColor(getApplicationContext(), R.color.info));
    }

    @Override
    public void displayInfoPasswordSize() {
        msgPresenter.showMsgShort(getResources().getString(R.string.min_length_password), ContextCompat.getColor(getApplicationContext(), R.color.info));
    }

    @Override
    public void displayInfoProfile() {
        msgPresenter.showMsgShort(getResources().getString(R.string.error_profile), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    @OnClick(R.id.btnLogin)
    public void OnClickLogin() {
        presenter.validateCredentials(inputEmail.getText().toString(), inputPassword.getText().toString());
    }

    //varificar si el cursor esta borrando las letras
    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            inputLayoutEmail.setErrorEnabled(false);
            inputLayoutPassword.setErrorEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        isInFront = true;
        presenter.setView(this);
        Log.d(TAG, "Android: Marshmallow");
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInFront = false;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivity.this).setMessage(message).
                setPositiveButton("OK", okListener).
                setNeutralButton("Permision", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setData(Uri.parse("package:" + getPackageName()));
                        startActivity(i);
                    }
                }).
                setNegativeButton("Cancel", null).create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //se crea una cuenta despues del login
    private void newAccount(Profile profile) {
        addAccount(profile);//agregar una cuenta
        addPreference(profile);//agregar preference global
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    //agregar un cuenta
    private void addAccount(Profile profile) {
        Account userAccount = new Account(profile.getEmail(), getResources().getString(R.string.account_type));
        boolean accountCreated = accountManager.addAccountExplicitly(userAccount, "", null);
        if (accountCreated) {
            //Log.d(TAG, Prefs.getString(Constants.TOKEN, ""));
            accountManager.setUserData(userAccount, Constants.TOKEN, Prefs.getString(Constants.TOKEN, ""));
            accountManager.setUserData(userAccount, Constants.KEY_ID, profile.getId());
            accountManager.setUserData(userAccount, Constants.KEY_NAME, profile.getName());
            accountManager.setUserData(userAccount, Constants.KEY_EMAIL, profile.getEmail());
            accountManager.setUserData(userAccount, Constants.KEY_ENTERPRISE_ID, profile.getEnterpriseId());
            accountManager.setUserData(userAccount, Constants.KEY_STATUS, profile.getStatus());
            accountManager.setUserData(userAccount, Constants.KEY_CREATE_AT, profile.getCreatedAt());
            accountManager.setUserData(userAccount, Constants.KEY_UPDATED_AT, profile.getUpdatedAt());
            accountManager.setUserData(userAccount, Constants.KEY_ROLE_ID, profile.getRoleId());
            accountManager.setUserData(userAccount, Constants.KEY_PERMISSION, Functions.listToJSON(profile.getRole().getPermissions()));

            //Functions.JSONToList();

            Prefs.putBoolean(Constants.ACCOUNT, true);//almacenar los datos de la cuenta
            //todavia falta almacenar datos
            if (response != null) {
                Bundle result = new Bundle();
                result.putString(AccountManager.KEY_ACCOUNT_NAME, profile.getEmail());
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, getResources().getString(R.string.account_type));
                response.onResult(result);
            }
        }
    }

    //preference local, para no pedir datos a la cuenta
    private void addPreference(Profile profile) {
        Prefs.putString(Constants.KEY_ENTERPRISE_ID, profile.getEnterpriseId());
        Prefs.putString(Constants.KEY_ID, profile.getId());
        Prefs.putString(Constants.KEY_USERNAME, profile.getEmail());
        Prefs.putString(Constants.KEY_PERMISSION, Functions.listToJSON(profile.getRole().getPermissions()));
        Prefs.putBoolean(Constants.SESSION, true);
    }
}