package co.usetime.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.componets.LocationProvider;
import co.usetime.componets.ProgressWheel;
import co.usetime.di.SnackbarModule;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.domain.repository.interfaces.DocumentRepository;
import co.usetime.mvp.model.DocumentFile;
import co.usetime.mvp.model.Invistes;
import co.usetime.mvp.presenter.interfaces.InvistesDocumentPresenter;
import co.usetime.mvp.presenter.interfaces.ShowMsgPresenter;
import co.usetime.mvp.view.DetailView;
import co.usetime.ui.adapte.DocumentAdapte;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Models;
import co.usetime.utils.Path;
import co.usetime.utils.Prefs;
import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.namee.permissiongen.PermissionGen;

public class DetailTaskActivity extends BaseActivity implements DetailView, LocationProvider.LocationCallback {
    protected static String TAG = DetailTaskActivity.class.getSimpleName();

    @Bind(R.id.coordinator)
    CoordinatorLayout coordinatorLayout;

    @Inject
    ShowMsgPresenter msgPresenter;

    @Inject
    InvistesDocumentPresenter invistesDocumentPresenter;

    @Inject
    DocumentRepository documentRepository;

    /**
     * imagenes de usuarios
     */
    @Bind(R.id.user1)
    CircleImageView user1;
    @Bind(R.id.user2)
    CircleImageView user2;
    @Bind(R.id.user3)
    CircleImageView user3;
    @Bind(R.id.user4)
    CircleImageView user4;
    @Bind(R.id.user5)
    CircleImageView user5;
    @Bind(R.id.layout_count)
    LinearLayout layout_count;
    @Bind(R.id.count)
    TextView count;

    private List<CircleImageView> IMGS = null;

    @Bind(R.id.card_user)
    CardView cardUser;
    @Bind(R.id.card_edit)
    CardView cardEdit;
    @Bind(R.id.card_file)
    CardView cardFile;


    @Bind(R.id.container)
    LinearLayout container;
    @Bind(R.id.progress)
    ProgressWheel mProgress;

    @Bind(R.id.ic_camera)
    ImageView ic_camera;
    @Bind(R.id.ic_commet)
    ImageView ic_commet;
    @Bind(R.id.ic_attach)
    ImageView ic_attach;

    @Bind(R.id.comment)
    EditText comment;

    @Bind(R.id.recycler_document)
    RecyclerView recycler;
    //recycler
    private LinearLayoutManager linearManager;
    private DocumentAdapte adapte;

    //que viene desde ña otra activirty
    private String pos = "";
    private String name = "";

    private ImageView imageView = null;

    //carga de tarea
    private MaterialDialog.Builder builder = null;
    private MaterialDialog.Builder builderComment = null;
    private MaterialDialog dialog = null;
    private MaterialDialog dialogComment = null;

    //para tomar foto
    public static final int MEDIA_TYPE_IMAGE = 1;
    private Uri fileUri; // file url to store image/video
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static int FILE_SELECT_CODE = 0;

    /**
     * dialogo
     */
    private EditText inputCommet = null;
    private ImageView photo = null;

    /**
     * location
     */
    private LocationProvider mLocationProvider;
    private LocationManager locationManager;
    private Location location_files = null;
    public static boolean IS_CHECK_GET = false;
    private MaterialDialog dialog_file = null;

    private final UploadServiceBroadcastReceiver uploadReceiver = new UploadServiceBroadcastReceiver() {
        @Override
        public void onProgress(String uploadId, int progress) {
            super.onProgress(uploadId, progress);
            Log.d(TAG, "The progress of the upload with ID " + uploadId + " is: " + progress);
        }

        @Override
        public void onError(String uploadId, Exception exception) {
            super.onError(uploadId, exception);
            Log.e(TAG, "Error in upload with ID: " + uploadId + ". " + exception.getLocalizedMessage(), exception);
            msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, byte[] serverResponseBody) {
            super.onCompleted(uploadId, serverResponseCode, serverResponseBody);
            Log.d(TAG, "Upload with ID " + uploadId + " is completed: " + serverResponseCode + ", " + new String(serverResponseBody));
            String body = new String(serverResponseBody);
            switch (serverResponseCode) {
                case 200:
                    msgPresenter.showMsgShort(getResources().getString(R.string.upload_success), ContextCompat.getColor(getApplicationContext(), R.color.success));
                    Log.d(TAG, "STATUS: " + Integer.toString(serverResponseCode));
                    documentRepository.persist(Models.DocumentJsonToString(body));
                    break;
                case 201:
                    msgPresenter.showMsgShort(getResources().getString(R.string.upload_success), ContextCompat.getColor(getApplicationContext(), R.color.success));
                    Log.d(TAG, "STATUS: " + Integer.toString(serverResponseCode));
                    documentRepository.persist(Models.DocumentJsonToString(body));
                    break;
                case 401:
                    msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                    Log.d(TAG, "STATUS: " + Integer.toString(serverResponseCode));
                    break;
                case 403:
                    msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                    Log.d(TAG, "STATUS: " + Integer.toString(serverResponseCode));
                    break;
                case 404:
                    msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                    Log.d(TAG, "STATUS: " + Integer.toString(serverResponseCode));
                    break;
                case 422:
                    msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                    Log.d(TAG, "STATUS: " + Integer.toString(serverResponseCode));
                    break;
                case 500:
                    msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
                    Log.d(TAG, "STATUS: " + Integer.toString(serverResponseCode));
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onCancelled(String uploadId) {
            super.onCancelled(uploadId);
            Log.d(TAG, "Upload with ID " + uploadId + " is cancelled");
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_task);
        ButterKnife.bind(this);
        System.out.println(TAG + " onCreate: " + IS_CHECK_GET);
        init();

        if (getIntent() != null) {
            Bundle b = getIntent().getExtras();
            name = b.getString("name");
            setName(name);
            pos = b.getString("pos");
        }
    }

    private void init() {
        //color para iconos

        IMGS = new ArrayList<>();
        IMGS.add(user1);
        IMGS.add(user2);
        IMGS.add(user3);
        IMGS.add(user4);
        IMGS.add(user5);

        ic_camera.setColorFilter(ContextCompat.getColor(this, R.color.color_icon));
        ic_commet.setColorFilter(ContextCompat.getColor(this, R.color.color_icon));
        ic_attach.setColorFilter(ContextCompat.getColor(this, R.color.color_icon));

        if (getSupportActionBar() != null) // Habilitar up button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //init de vista para presentador
        invistesDocumentPresenter.setView(this);

        //para la lista de archivos
        recycler.setHasFixedSize(true);
        linearManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearManager);

        initDialog();
    }

    private void initDialog() {
        builder = new MaterialDialog.Builder(this).title("Descargando detalles de tarea!").content(R.string.log_out_content).progress(true, 0).progressIndeterminateStyle(false);
        dialog = builder.build();

        builderComment = new MaterialDialog.Builder(this).title(R.string.send_comentario).content(R.string.log_out_content).progress(true, 0).progressIndeterminateStyle(true);
        dialogComment = builderComment.build();
    }

    @Override
    protected List<Object> getModules() {
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    /**
     * @param name colcoar el nombre el collapser
     */
    private void setName(String name) {
        getSupportActionBar().setTitle(name);
    }

    @Override
    public View getView() {
        return coordinatorLayout;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        uploadReceiver.register(this);
        //!Prefs.getBoolean("IS_CHECK_GET", false)
        if (!IS_CHECK_GET) {
            System.out.println(TAG + " onResume: " + IS_CHECK_GET);
            invistesDocumentPresenter.getListInvite(pos);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        uploadReceiver.unregister(this);
        //Prefs.putBoolean("IS_CHECK_GET", false)
        IS_CHECK_GET = true;
        System.out.println(TAG + " onPause: " + IS_CHECK_GET);
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        mLocationProvider = new LocationProvider(this);
        mLocationProvider.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        IS_CHECK_GET = false;
        System.out.println(TAG + " onDestroy: " + IS_CHECK_GET);
        //Prefs.putBoolean("IS_CHECK_GET", false);
        Log.d(TAG, "onDestroy");
        if (mLocationProvider != null) {
            mLocationProvider.disconnect();
        }
    }

    @Override
    public void adapteList(List<DocumentFile> documentFiles) {
        document(documentFiles);
    }

    private void document(List<DocumentFile> documentFiles) {
        if (documentFiles.size() != 0) {
            adapte = new DocumentAdapte(documentFiles);
            adapte.setHasStableIds(true);
            recycler.setAdapter(adapte);
        } else {
            cardFile.setVisibility(View.GONE);
            recycler.setVisibility(View.GONE);
        }

    }

    @Override
    public void showMsg(String mgs, int color) {
        msgPresenter.showMsgShort(mgs, color);
    }

    @Override
    public void showViewProgress(final boolean show) {
        if (show) {
            dialog.show();
        } else {
            dialog.dismiss();
        }
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        container.setVisibility(show ? View.GONE : View.VISIBLE);
        container.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                container.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });
    }

    @Override
    public void displayLayoutConnection(final boolean show) {

    }

    @Override
    public void displayLayoutNocontent(boolean content) {

    }

    @Override
    public void displayLayoutNoTask(boolean task) {

    }

    @Override
    public void displayError() {

    }

    @Override
    public void displayLayoutError(boolean status, String msg) {

    }

    @Override
    public void showViewRefresh(boolean show) {

    }

    @Override
    public void setRefreshing(boolean refreshing) {

    }

    @Override
    public void showProgressComment(final boolean show) {
        if (show) {
            dialogComment.show();
        } else {
            dialogComment.dismiss();
        }
    }

    @Override
    public void activityForResult(Intent intent, int code, Uri fileUri) {
        this.fileUri = fileUri;
        startActivityForResult(intent, code);
    }

    @OnClick(R.id.container_ic_attach)
    public void attach() {
        showFileAttach();
    }

    /**
     * ##################################################################### comentario
     */
    @OnClick(R.id.container_ic_commet)
    public void commet() {
        invistesDocumentPresenter.postComment(comment.getText().toString(), pos);
    }

    @OnClick(R.id.container_ic_camera)
    public void camera() {
        showFile();
    }

    private void showFile() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            showDialog();
        } else {
            if (Functions.isMarshmallow()) {
                if (Functions.hasPermission("android.permission.ACCESS_FINE_LOCATION") || Functions.hasPermission("android.permission.ACCESS_COARSE_LOCATION")) {
                    if (Functions.hasPermission("android.permission.CAMERA") && Functions.hasPermission("android.permission.WRITE_EXTERNAL_STORAGE")) {
                        invistesDocumentPresenter.eventCapture(pos);
                    } else {
                        showSnackbarPermission("Se necesita permiso especial para usar la camara y almacenamiento externo!", 2);
                    }
                } else {
                    showSnackbarPermission("Se necesita permiso especial de localización!", 2);
                }
            } else {
                invistesDocumentPresenter.eventCapture(pos);
            }
        }
    }

    private void showFileAttach() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            showDialog();
        } else {
            if (Functions.isMarshmallow()) {
                if (Functions.hasPermission("android.permission.ACCESS_FINE_LOCATION") || Functions.hasPermission("android.permission.ACCESS_COARSE_LOCATION")) {
                    if (Functions.hasPermission("android.permission.READ_EXTERNAL_STORAGE")) {
                        showFileChooser();
                    } else {
                        showSnackbarPermission("Se necesita permiso especial de lectura de almacenamiento!", 2);
                    }
                } else {
                    showSnackbarPermission("Se necesita permiso especial de localización!", 2);
                }
            } else {
                showFileChooser();
            }
        }
    }

    /**
     * @param msg  string mensaje para mostrar
     * @param type int tipo de mesnaje largo o corto
     */
    private void showSnackbarPermission(String msg, int type) {
        Snackbar snack = null;
        switch (type) {
            case 1:
                snack = Snackbar.make(container, msg, Snackbar.LENGTH_SHORT);
                break;
            case 2:
                snack = Snackbar.make(container, msg, Snackbar.LENGTH_LONG);
                break;
            case 3:
                snack = Snackbar.make(container, msg, Snackbar.LENGTH_INDEFINITE);
                break;
        }
        snack.getView().setBackgroundColor(ContextCompat.getColor(UIApp.getContext, R.color.info));
        snack.setAction("Ajustes", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(i);
            }
        });
        snack.show();
    }

    private void showDialog() {
        new MaterialDialog.Builder(this).title(R.string.setting_gps_title).
                content(R.string.setting_gps_content)
                .positiveText(R.string.setting_gps_agree)
                .negativeText(R.string.setting_gps_disagree)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).show();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            requestCapture(resultCode);
        } else {
            if (requestCode == FILE_SELECT_CODE) {
                requestFile(resultCode, data);
            }
        }
    }

    /**
     * @param resultCode codigo que viene del result
     */
    private void requestCapture(int resultCode) {
        if (resultCode == RESULT_OK) {
            msgPresenter.showMsgShort(getResources().getString(R.string.photo_task), ContextCompat.getColor(this, R.color.success));
            showPhoto(Path.getPath(fileUri), Integer.parseInt(pos));
        } else {
            if (resultCode == RESULT_CANCELED) {
                msgPresenter.showMsgShort("No se pudo capturar la imagen", ContextCompat.getColor(this, R.color.alert));
            } else {
                msgPresenter.showMsgShort("Lo siento! Fallo a capturar image", ContextCompat.getColor(this, R.color.alert));
            }
        }
    }

    /**
     * @param resultCode codigo que viene del result
     */
    private void requestFile(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String path = "";
            if (Build.VERSION.SDK_INT < 19) {
                try {
                    path = Functions.getPath(uri);
                    Log.d(TAG, "File Path: " + path);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                path = Path.getPath(uri);
            }
            assert path != null;
            if (path.equals("")) {
                msgPresenter.showMsgShort(getResources().getString(R.string.error_path), ContextCompat.getColor(DetailTaskActivity.this, R.color.alert));
            } else {
                try {
                    showFile(path, FILE_SELECT_CODE);
                } catch (RuntimeException e) {
                    Log.e(TAG, e.toString());
                    msgPresenter.showMsgShort(getResources().getString(R.string.error_path), ContextCompat.getColor(DetailTaskActivity.this, R.color.alert));
                }
            }
        } else {
            if (resultCode == RESULT_CANCELED) {
                msgPresenter.showMsgShort("Lo siento! No se pudo obtener el archivo", ContextCompat.getColor(this, R.color.alert));
            } else {
                msgPresenter.showMsgShort("Lo siento! Fallo a capturar image", ContextCompat.getColor(this, R.color.alert));
            }
        }
    }

    private void showFile(final String path, final int id) {
        //msgPresenter.showMsgShort("Archivo cargado!", ContextCompat.getColor(DetailTaskActivity.this, R.color.success));
        dialog_file = new MaterialDialog.Builder(this)
                .title(R.string.file)
                .customView(R.layout.img_and_comment_layout2, true)
                .positiveText(R.string.acept)
                .negativeText(R.string.setting_gps_disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (inputCommet.getText().length() != 0) {
                            uploadFile(path, id, inputCommet.getText().toString());
                        } else {
                            uploadFile(path, id, getResources().getString(R.string.default_file));
                        }
                        dialog.dismiss();
                        msgPresenter.showMsgShort("Preparando archivo para ser enviado...", ContextCompat.getColor(DetailTaskActivity.this, R.color.info));
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).build();
        inputCommet = (EditText) dialog_file.getCustomView().findViewById(R.id.input_comment);
        dialog_file.show();
    }

    public void showPhoto(final String path, final int id) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.file)
                .customView(R.layout.img_and_comment_layout2, true)
                .positiveText(R.string.acept)
                .negativeText(R.string.setting_gps_disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (inputCommet.getText().length() != 0) {
                            uploadFile(path, id, inputCommet.getText().toString());
                        } else {
                            uploadFile(path, id, getResources().getString(R.string.default_file));
                        }
                        dialog.dismiss();
                        msgPresenter.showMsgShort("Enviado archivo...", ContextCompat.getColor(DetailTaskActivity.this, R.color.info));
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).build();
        inputCommet = (EditText) dialog.getCustomView().findViewById(R.id.input_comment);
        photo = (ImageView) dialog.getCustomView().findViewById(R.id.photo);
        Glide.with(this).load(path).centerCrop().crossFade().into(photo);
        dialog.show();
    }

    /**
     * obtener archivo
     */
    private void showFileChooser() {
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            FILE_SELECT_CODE = Integer.parseInt(pos);
            startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.select_file)), FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            msgPresenter.showMsgShort(getResources().getString(R.string.please_file_manager), ContextCompat.getColor(DetailTaskActivity.this, R.color.alert));
        }
    }

    private void uploadFile(String fileName, int id, String description) {
        boolean error = false;
        try {
            String token = Prefs.getString(Constants.TOKEN, "");
            String agent = HttpRestClient.USER_AGENT + Functions.getVersion();
            String url = HttpRestClient.BASE_URL + HttpRestClient.VERSION + "documents";
            Log.d(TAG, agent);
            Log.d(TAG, url);
            Log.d(TAG, "");

            MultipartUploadRequest req = new MultipartUploadRequest(this, url);
            req.addHeader("Authorization", HttpRestClient.PREFIX + token);
            req.addHeader("User-Agent", agent);
            req.addHeader("Accept", "application/json");
            req.setCustomUserAgent("User-Agent/" + agent);
            req.addFileToUpload(fileName, "file");
            req.addParameter("task_id", Integer.toString(id));
            if ((location_files.getLatitude() != 0) || (location_files.getLongitude() != 0)) {
                Log.d(TAG, "latitude: " + Double.toString(location_files.getLatitude()));
                Log.d(TAG, "longitude: " + Double.toString(location_files.getLongitude()));
                req.addParameter("latitude", Double.toString(location_files.getLatitude()));
                req.addParameter("longitude", Double.toString(location_files.getLongitude()));
            } else {
                error = true;
                msgPresenter.showMsgShort(getResources().getString(R.string.error_gps_entry), ContextCompat.getColor(DetailTaskActivity.this, R.color.info));
            }

            if (Functions.isLolipop()) {
                req.setNotificationConfig(getNotificationConfigMoreLolipop(description));
            } else {
                req.setNotificationConfig(getNotificationConfig(description));
            }

            req.setAutoDeleteFilesAfterSuccessfulUpload(false);
            req.setUsesFixedLengthStreamingMode(true);
            req.setMaxRetries(2);
            req.setUtf8Charset();
            if (!error) {
                req.startUpload();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private UploadNotificationConfig getNotificationConfig(String filename) {
        return new UploadNotificationConfig()
                .setIcon(R.mipmap.ic_launcher)
                .setTitle(filename)
                .setRingToneEnabled(true)
                .setInProgressMessage(getString(R.string.uploading))
                .setCompletedMessage(getString(R.string.upload_success))
                .setErrorMessage(getString(R.string.upload_error))
                .setClickIntent(new Intent(this, DetailTaskActivity.class))
                .setClearOnAction(true)
                .setRingToneEnabled(false);
    }

    private UploadNotificationConfig getNotificationConfigMoreLolipop(String filename) {
        return new UploadNotificationConfig()
                .setIcon(R.mipmap.ic_file_upload_white_24dp)
                .setTitle(filename)
                .setRingToneEnabled(true)
                .setInProgressMessage(getString(R.string.uploading))
                .setCompletedMessage(getString(R.string.upload_success))
                .setErrorMessage(getString(R.string.upload_error))
                .setClickIntent(new Intent(this, DetailTaskActivity.class))
                .setClearOnAction(true)
                .setRingToneEnabled(false);
    }

    @Override
    public void user1000(List<Invistes> invistes) {
        layout_count.setVisibility(View.VISIBLE);
        int counts = invistes.size() - 5;
        count.setText(Integer.toString(counts));
        for (CircleImageView cv : IMGS) {
            cv.setVisibility(View.VISIBLE);
        }
        getImgProfile(invistes.get(0).getId(), user1);
        getImgProfile(invistes.get(1).getId(), user2);
        getImgProfile(invistes.get(2).getId(), user3);
        getImgProfile(invistes.get(3).getId(), user4);
        getImgProfile(invistes.get(4).getId(), user5);

    }

    @Override
    public void user6(List<Invistes> invistes) {
        layout_count.setVisibility(View.VISIBLE);
        count.setText(Integer.toString(invistes.size() - 5));
        for (CircleImageView cv : IMGS) {
            cv.setVisibility(View.VISIBLE);
        }
        getImgProfile(invistes.get(0).getId(), user1);
        getImgProfile(invistes.get(1).getId(), user2);
        getImgProfile(invistes.get(2).getId(), user3);
        getImgProfile(invistes.get(3).getId(), user4);
        getImgProfile(invistes.get(4).getId(), user5);
    }

    @Override
    public void user5(List<Invistes> invistes) {
        for (CircleImageView cv : IMGS) {
            cv.setVisibility(View.VISIBLE);
        }
        getImgProfile(invistes.get(0).getId(), user1);
        getImgProfile(invistes.get(1).getId(), user2);
        getImgProfile(invistes.get(2).getId(), user3);
        getImgProfile(invistes.get(3).getId(), user4);
        getImgProfile(invistes.get(4).getId(), user5);
    }

    @Override
    public void user4(List<Invistes> invistes) {
        int i = 0;
        for (CircleImageView cv : IMGS) {
            if (i <= 3) {
                cv.setVisibility(View.VISIBLE);
            }
            i++;
        }
        getImgProfile(invistes.get(0).getId(), user1);
        getImgProfile(invistes.get(1).getId(), user2);
        getImgProfile(invistes.get(2).getId(), user3);
        getImgProfile(invistes.get(3).getId(), user4);
    }

    @Override
    public void user3(List<Invistes> invistes) {
        int i = 0;
        for (CircleImageView cv : IMGS) {
            if (i <= 2) {
                cv.setVisibility(View.VISIBLE);
            }
            i++;
        }
        getImgProfile(invistes.get(0).getId(), user1);
        getImgProfile(invistes.get(1).getId(), user2);
        getImgProfile(invistes.get(2).getId(), user3);
    }

    @Override
    public void user2(List<Invistes> invistes) {
        int i = 0;
        for (CircleImageView cv : IMGS) {
            if (i <= 1) {
                cv.setVisibility(View.VISIBLE);
            }
            i++;
        }
        getImgProfile(invistes.get(0).getId(), user1);
        getImgProfile(invistes.get(1).getId(), user2);
    }

    @Override
    public void user1(List<Invistes> invistes) {
        user1.setVisibility(View.VISIBLE);
        getImgProfile(invistes.get(0).getId(), user1);
    }

    @Override
    public void commentOnSuccess() {
        comment.setText("");
    }

    /**
     * descargar imagen de perfil
     *
     * @param id              id del usaurio
     * @param circleImageView view donde va la imagen
     */
    private void getImgProfile(String id, CircleImageView circleImageView) {
        String url1 = HttpRestClient.BASE_URL + HttpRestClient.VERSION + "profile/" + id + "/avatar";
        Glide.with(this).load(url1)
                .placeholder(R.drawable.profile_default)
                .error(R.drawable.error_128)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(circleImageView);
    }

    @Override
    public void handleNewLocation(Location location) {
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        System.out.println(TAG + ": " + currentLatitude + ":::" + currentLongitude);
        if (location_files == null) {
            location_files = location;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }
}