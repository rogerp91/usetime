package co.usetime.ui.activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.util.ArrayList;
import java.util.List;

import co.usetime.UIApp;
import co.usetime.bus.BusProvider;
import co.usetime.di.ActivityModule;
import dagger.ObjectGraph;

/**
 * Created by Roger Patiño on 06/01/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ObjectGraph activityGraph;
    public static Bus bus;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
        bus = new Bus(ThreadEnforcer.MAIN);
        bus.register(this);
    }

    protected abstract List<Object> getModules();

    public void injectDependencies() {
        UIApp IkApplication = (UIApp) getApplication();
        List<Object> activityScopeModules = (getModules() != null) ? getModules() : new ArrayList<>();
        activityScopeModules.add(new ActivityModule(this));
        activityGraph = IkApplication.buildGraphWithAditionalModules(activityScopeModules);
        inject(this);
    }

    public void inject(Object entityToGetInjected) {
        activityGraph.inject(entityToGetInjected);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityGraph = null;
    }
}