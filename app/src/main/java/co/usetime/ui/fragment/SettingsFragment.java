package co.usetime.ui.fragment;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceFragment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.jenzz.materialpreference.CheckBoxPreference;
import com.jenzz.materialpreference.Preference;
import com.vi.swipenumberpicker.OnValueChangeListener;
import com.vi.swipenumberpicker.SwipeNumberPicker;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.mvp.presenter.interfaces.ShowMsgPresenter;
import co.usetime.services.ManagerService;
import co.usetime.ui.activity.SettingsActivity;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
public class SettingsFragment extends PreferenceFragment {

    protected static String TAG = SettingsFragment.class.getSimpleName();

    Preference pref_version;
    Preference pref_develop;
    Preference pref_device;
    Preference pref_rest;
    CheckBoxPreference pref_api_new;

    //package
    PackageInfo pinfo;

    //time
    SwipeNumberPicker swipeNumberPickerHour;
    SwipeNumberPicker swipeNumberPickerMin;
    ShowMsgPresenter msgPresenter;

    SettingsActivity actionBarActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.setting_preferences);
        actionBarActivity = (SettingsActivity) getActivity();// obtener actividad en q esta trabajando el fragment

        pref_develop = (Preference) findPreference("keyDevelop");
        pref_device = (Preference) findPreference("keyDevice");
        pref_version = (Preference) findPreference("keyVersion");
        pref_api_new = (CheckBoxPreference) findPreference("keyApiNew");
        pref_rest = (Preference) findPreference("keyRest");

        //version
        try {
            pinfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.toString() + "Package");
            e.printStackTrace();
        }

        //version
        pref_version.setSummary(pinfo.versionName);
        pref_rest.setSummary(co.usetime.BuildConfig.TYPE);
        //dispositivo
        pref_device.setSummary(android.os.Build.MANUFACTURER + ", " + android.os.Build.MODEL);

        //empresa
        pref_develop.setSummary(getResources().getString(R.string.app_name_empresa));
        findPreference("keyLocalitation").setOnPreferenceClickListener(new android.preference.Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(android.preference.Preference preference) {
                showCustomView();
                return false;
            }
        });
        findPreference("keyStates").setOnPreferenceClickListener(new android.preference.Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(android.preference.Preference preference) {
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.modo)
                        .items(R.array.preference_type)
                        .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                showMsg(getResources().getString(R.string.min_length_password), ContextCompat.getColor(getActivity(), R.color.info));
                                return true; // allow selection
                            }
                        })
                        .positiveText(R.string.acept)
                        .show();
                return false;
            }
        });
    }

    //capture
    public void showCustomView() {
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title("Tiempo de geolocalización")
                .customView(R.layout.dialog_gestion2, true)
                .positiveText("Agregar")
                .negativeText(android.R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        final String hour = Integer.parseInt(swipeNumberPickerHour.getText().toString()) > -1 && Integer.parseInt(swipeNumberPickerHour.getText().toString()) < 10 ? swipeNumberPickerHour.getText().toString() : swipeNumberPickerHour.getText().toString();
                        final String min = Integer.parseInt(swipeNumberPickerMin.getText().toString()) > -1 && Integer.parseInt(swipeNumberPickerMin.getText().toString()) < 10 ? swipeNumberPickerMin.getText().toString() : swipeNumberPickerMin.getText().toString();
                        //Toast.makeText(getActivity(), "Agregar ", Toast.LENGTH_SHORT).show();
                        //msgPresenter.showMsgShort(Integer.toString(Functions.getHourToMilisecond(hour)) + " y " + Integer.toString(Functions.getMinuteToMilisecond(min)), ContextCompat.getColor(UIApp.getContext, R.color.success));
                        //Log.d(TAG, "Hour: " + Integer.toString(Functions.getHourToMilisecond(hour)));
                        //Log.d(TAG, "Min: " + Integer.toString(Functions.getMinuteToMilisecond(min)));


                        int hourMilisecond = Functions.getHourToMilisecond(hour);
                        int minMilisecond = Functions.getMinuteToMilisecond(min);

                        Log.d(TAG, "Hour: " + Integer.toString(hourMilisecond));
                        Log.d(TAG, "Min: " + Integer.toString(minMilisecond));

                        Prefs.putInt(ManagerService.HOUR_LOCALITATION, hourMilisecond);
                        Prefs.putInt(ManagerService.MINUTE_LOCALITATION, minMilisecond);

                        int time = Prefs.getInt(ManagerService.HOUR_LOCALITATION, 0) + Prefs.getInt(ManagerService.MINUTE_LOCALITATION, 0);

                        Log.d(TAG, "TIme: " + Integer.toString(time));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Snackbar snackbar = Snackbar.make(getView(), "Tiempo " + hour + " h y " + min + "m", Snackbar.LENGTH_LONG);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(ContextCompat.getColor(UIApp.getContext, R.color.success));
                                snackbar.show();
                            }
                        }, (300));
                    }
                }).build();

        swipeNumberPickerHour = (SwipeNumberPicker) dialog.getCustomView().findViewById(R.id.picker_hour);
        swipeNumberPickerMin = (SwipeNumberPicker) dialog.getCustomView().findViewById(R.id.picker_min);
        swipeNumberPickerHour.setOnValueChangeListener(new OnValueChangeListener() {
            @Override
            public boolean onValueChange(SwipeNumberPicker view, int oldValue, int newValue) {
                Log.d(TAG, String.valueOf(newValue));
                return true;
            }
        });
        swipeNumberPickerMin.setOnValueChangeListener(new OnValueChangeListener() {
            @Override
            public boolean onValueChange(SwipeNumberPicker view, int oldValue, int newValue) {
                Log.d(TAG, String.valueOf(newValue));
                return true;
            }
        });
        dialog.show();
    }

    private void showMsg(String msg, int color) {
        Snackbar snackbar = Snackbar.make(actionBarActivity.getLayout(), msg, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(color);
        snackbar.show();
    }

}