package co.usetime.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.usetime.UIApp;
import co.usetime.di.FragmentModule;
import co.usetime.services.ManagerService;
import co.usetime.ui.activity.MainActivity;
import dagger.ObjectGraph;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public abstract class BaseFragment extends Fragment {
    protected static String TAG = BaseFragment.class.getSimpleName();

    protected boolean isGPS = false;
    private ObjectGraph activityGraph;

    public MainActivity mainActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
        mainActivity = (MainActivity) getActivity();
    }

    //inyectar por actividades modulos de cada actividad
    protected abstract List<Object> getModules();

    //injectar dependencias
    private void injectDependencies() {
        UIApp IkApplication = (UIApp) getActivity().getApplication();
        List<Object> activityScopeModules = (getModules() != null) ? getModules() : new ArrayList<>();
        activityScopeModules.add(new FragmentModule(getActivity()));
        activityGraph = IkApplication.buildGraphWithAditionalModules(activityScopeModules);
        inject(this);
    }

    public void inject(Object entityToGetInjected) {
        activityGraph.inject(entityToGetInjected);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activityGraph = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiverAlert);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ManagerService.ACTION_SETTING_GPS);
        getActivity().registerReceiver(receiverAlert, intentFilter);
    }

    private BroadcastReceiver receiverAlert = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null) {
                if (intent.getAction().equals(ManagerService.ACTION_SETTING_GPS)) {
                    //isGPS = true;
                    setGPS(true);
                    Log.d(TAG, "receiverAlert");
                }
            }
        }
    };

    public boolean isGPS() {
        return isGPS;
    }

    public void setGPS(boolean GPS) {
        isGPS = GPS;
    }
}