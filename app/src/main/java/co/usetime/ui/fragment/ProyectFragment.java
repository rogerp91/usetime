package co.usetime.ui.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.componets.ProgressWheel;
import co.usetime.di.SnackbarModule;
import co.usetime.mvp.model.Proyect;
import co.usetime.mvp.presenter.interfaces.ProyectPresenter;
import co.usetime.mvp.presenter.interfaces.ShowMsgPresenter;
import co.usetime.mvp.view.ProyectView;
import co.usetime.ui.activity.MyTaskActivity;
import co.usetime.ui.adapte.ProyectAdapte;
import co.usetime.utils.Functions;

/**
 * Created by Roger Patiño on 12/01/2016.
 */
public class ProyectFragment extends BaseFragment implements ProyectView,
        ProyectAdapte.OnItemClickListener,
        ProyectAdapte.PopupMenuListener,
        SwipeRefreshLayout.OnRefreshListener {

    protected static String TAG = ProyectFragment.class.getSimpleName();

    public static final String ARG_SECTION_TITLE = "section_number";

    public static ProyectFragment newInstance(String sectionTitle) {
        ProyectFragment fragment = new ProyectFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    @Bind(R.id.progress)
    ProgressWheel mProgressView;
    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.layout_no_content)
    RelativeLayout layout_noContent;
    @Bind(R.id.layout_no_proyect)
    RelativeLayout layout_no_proyect;

    @Bind(R.id.layout_connection)
    RelativeLayout layout_Connection;

    @Inject
    ProyectPresenter presenter;
    @Inject
    ShowMsgPresenter msgPresenter;

    @Bind(R.id.recycler_view)
    RecyclerView recycler;
    private LinearLayoutManager linearManager;
    private ProyectAdapte adaptador;

    private List<Proyect> proyectList;

    @Override
    protected List<Object> getModules() {
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_proyect, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler.setHasFixedSize(true);
        linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);

        presenter.setView(this);
        presenter.getList();
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(UIApp.getContext, R.color.colorPrimary));
    }

    @Nullable
    @Override
    public View getView() {
        return mainActivity.getView();
    }

    @Override
    public void displayError() {

    }

    @Override
    public void displayErrorRefresh() {
        msgPresenter.showMsgLong(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getActivity(), R.color.alert));
    }

    @Override
    public void showView(boolean show) {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void showMsg(String mgs, int color) {
        if (isAdded() && getActivity() != null) {
            msgPresenter.showMsgShort(mgs, color);
        }
    }

    @Override
    public void adapteList(List<Proyect> list, boolean type) {
        if (list != null) {
            adaptador = new ProyectAdapte(list);
            adaptador.setHasStableIds(true);
            adaptador.setOnItemClickListener(this);
            recycler.setAdapter(adaptador);
            proyectList = list;
            if (!type) {
                swipeRefreshLayout.setRefreshing(false);
                msgPresenter.showMsgShort("Contenido cargado", ContextCompat.getColor(getActivity(), R.color.success));
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void gotoMain(Intent i) {

    }

    @Override
    public void showViewProgress(final boolean show) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @Override
    public void showViewRefresh(final boolean show) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            swipeRefreshLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            swipeRefreshLayout.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    swipeRefreshLayout.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @Override
    public void displayLayoutConnection(final boolean connection) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
            layout_Connection.animate().setDuration(shortAnimTime).alpha(connection ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @Override
    public void displayLayoutNocontent(final boolean content) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
            layout_noContent.animate().setDuration(shortAnimTime).alpha(content ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @Override
    public void displayLayoutVoid(final boolean empty) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            layout_no_proyect.setVisibility(empty ? View.VISIBLE : View.GONE);
            layout_no_proyect.animate().setDuration(shortAnimTime).alpha(empty ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    layout_no_proyect.setVisibility(empty ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @Override
    public boolean isAddedFragment() {
        return isAdded();
    }

    @OnClick(R.id.text_try_again_content)
    public void TryAgainContent() {
        presenter.setView(this);
        presenter.getList();
    }

    @OnClick(R.id.text_try_again)
    public void TryAgain() {
        presenter.setView(this);
        presenter.getListRefresh();
    }

    @OnClick(R.id.layout_no_proyect)
    public void TryAgainProyect() {
        presenter.setView(this);
        presenter.getList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
        if (isGPS()) {
            //msgPresenter.showMsgLong(getResources().getString(R.string.setting_gps_content), ContextCompat.getColor(getActivity(), R.color.info));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(RecyclerView.ViewHolder item, int position) {
        if (proyectList != null) {
            int id = Integer.parseInt(proyectList.get(position).getId());
            Log.d(TAG, Integer.toString(id));
            Intent intent = new Intent(getActivity(), MyTaskActivity.class);
            intent.putExtra("ID", id);
            startActivity(intent);
        }
    }

    @Override
    public void onPopupMenuClicked(MenuItem menuItem, int adapterPosition) {

    }

    @Override
    public void onRefresh() {
        if (Functions.isOnline()) {
            presenter.getListRefresh();
        } else {
            swipeRefreshLayout.setRefreshing(false);
            showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
        }
    }
}