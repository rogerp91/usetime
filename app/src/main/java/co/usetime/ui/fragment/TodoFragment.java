package co.usetime.ui.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.woxthebox.draglistview.BoardView;
import com.woxthebox.draglistview.DragItem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.componets.ProgressWheel;
import co.usetime.componets.ShowSnackbarMsg;
import co.usetime.di.SnackbarModule;
import co.usetime.mvp.model.Completed;
import co.usetime.mvp.model.ItemTodo;
import co.usetime.mvp.model.Pending;
import co.usetime.mvp.model.Progress;
import co.usetime.mvp.model.Rejected;
import co.usetime.mvp.model.Todo;
import co.usetime.mvp.model.TodoSimple;
import co.usetime.mvp.presenter.interfaces.ShowMsgPresenter;
import co.usetime.mvp.presenter.interfaces.TodoPresenter;
import co.usetime.mvp.view.TodoView;
import co.usetime.ui.adapte.TodoAdapte2;
import co.usetime.utils.Functions;

public class TodoFragment extends BaseFragment implements TodoView {

    protected static String TAG = TodoFragment.class.getSimpleName();

    public static TodoFragment newInstance() {
        return new TodoFragment();
    }

    private List<Pending> LIST_PENDING = null;
    private List<Progress> LIST_PROGRESS = null;
    private List<Completed> LIST_COMPLETED = null;
    private List<Rejected> LIST_EXPIRED = null;


    @Bind(R.id.fabAdd)
    FloatingActionButton fabAdd;

    @Bind(R.id.progress)
    ProgressWheel mProgressView;
    private ShowMsgPresenter showSnackbarMsg;

    @Bind(R.id.layout_no_content)
    RelativeLayout layout_noContent;

    @Bind(R.id.layout_no_proyect)
    RelativeLayout layout_no_proyect;

    @Bind(R.id.layout_connection)
    RelativeLayout layout_Connection;

    @Bind(R.id.layout_code_http)
    RelativeLayout layout_code_http;

    @Bind(R.id.code_http)
    TextView code_http;

    @Bind(R.id.board_view)
    BoardView mBoardView;
    private static int sCreatedItems = 0;
    private int mColumns;

    @Inject
    TodoPresenter presenter;

    private int column = 0, row = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected List<Object> getModules() {
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBoardView = (BoardView) view.findViewById(R.id.board_view);
        mBoardView.setSnapToColumnsWhenScrolling(true);
        mBoardView.setSnapToColumnWhenDragging(true);
        mBoardView.setSnapDragItemToTouch(true);
        mBoardView.setCustomDragItem(new MyDragItem(getActivity(), R.layout.column_item));
        mBoardView.setBoardListener(new BoardView.BoardListener() {
            @Override
            public void onItemDragStarted(int column, int row) {
                //Toast.makeText(mBoardView.getContext(), "Start - column: " + column + " row: " + row, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemChangedColumn(int oldColumn, int newColumn) {
            }

            @Override
            public void onItemDragEnded(int fromColumn, int fromRow, int toColumn, int toRow) {
                if (fromColumn != toColumn || fromRow != toRow) {
                    //Toast.makeText(mBoardView.getContext(), "fromColumn: " + fromColumn + " ,fromRow" + fromRow + ", toColumn: " + toColumn + " ,toRow: " + toRow, Toast.LENGTH_SHORT).show();
                    changeFromTo(fromColumn, fromRow, toColumn, toRow);
                }
            }
        });

        showSnackbarMsg = new ShowSnackbarMsg(this);
        presenter.setView(this, 1);
    }

    /**
     * @param fromColumn columna desde
     * @param fromRow    fila desde
     * @param toColumn   columna donde voy
     * @param toRow      file donde voy
     */
    private void changeFromTo(int fromColumn, int fromRow, int toColumn, int toRow) {
        String id_1 = null;
        String id_2 = null;
        String id_3 = null;
        switch (fromColumn) {
            case 0:
                try {
                    id_1 = LIST_PENDING.get(fromRow).getTask_id();
                    Log.d(TAG, LIST_PENDING.get(fromRow).getStatus());
                    //changeTo(toColumn);
                    switch (toColumn) {
                        case 1:
                            presenter.getChange(id_1, 2);
                            break;
                        case 2:
                            presenter.getChange(id_1, 3);
                            break;
                        case 3:
                            presenter.getChange(id_1, 6);
                            break;
                    }
                } catch (NullPointerException | IndexOutOfBoundsException e) {
                    Log.e(TAG, e.toString());
                }
                break;
            case 1:
                try {
                    id_2 = LIST_PROGRESS.get(fromRow).getTask_id();
                    Log.d(TAG, LIST_PROGRESS.get(fromRow).getStatus());
                    switch (toColumn) {
                        case 0:
                            showMsg(UIApp.getContext.getResources().getString(R.string.error_pendiente), ContextCompat.getColor(UIApp.getContext, R.color.info));
                            break;
                        case 2:
                            presenter.getChange(id_2, 3);
                            break;
                        case 3:
                            presenter.getChange(id_2, 6);
                            break;
                    }
                } catch (NullPointerException | IndexOutOfBoundsException e) {
                    Log.e(TAG, e.toString());
                }
                break;
            case 2:
                try {
                    id_3 = LIST_COMPLETED.get(fromRow).getTask_id();
                    Log.d(TAG, LIST_COMPLETED.get(fromRow).getStatus());
                    switch (toColumn) {
                        case 0:
                            showMsg(UIApp.getContext.getResources().getString(R.string.error_pendiente), ContextCompat.getColor(UIApp.getContext, R.color.info));
                            break;
                        case 1:
                            showMsg(UIApp.getContext.getResources().getString(R.string.error_progreso), ContextCompat.getColor(UIApp.getContext, R.color.info));
                            break;
                        case 3:
                            presenter.getChange(id_3, 6);
                            break;
                    }
                } catch (NullPointerException | IndexOutOfBoundsException e) {
                    Log.e(TAG, e.toString());
                }
                break;
            case 3:
                //String id_4 = LIST_EXPIRED.get(fromRow).getId();
                switch (toColumn) {
                    case 0:
                        showMsg(UIApp.getContext.getResources().getString(R.string.error_pendiente), ContextCompat.getColor(UIApp.getContext, R.color.info));
                        break;
                    case 1:
                        showMsg(UIApp.getContext.getResources().getString(R.string.error_progreso), ContextCompat.getColor(UIApp.getContext, R.color.info));
                        break;
                    case 2:
                        showMsg(UIApp.getContext.getResources().getString(R.string.error_completed), ContextCompat.getColor(UIApp.getContext, R.color.info));
                        break;
                }
                break;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (isVisibleToUser && isResumed()) {
                Log.d(TAG, "isVisibleToUser && isResumed(): " + isVisibleToUser);
                return;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG, "onResume");
        if (Functions.isOnline()) {
            presenter.getList();
        } else {
            displayLayoutConnection(true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void showMsg(String mgs, int color) {
        if (isAdded()) {
            showSnackbarMsg.showMsgShort(mgs, color);
        }
    }

    @Override
    public void adapteLists(Todo todo, boolean type) {
        if (!type) {
            showMsg("Contenido cargado", ContextCompat.getColor(getActivity(), R.color.success));
        }

        if (todo.getPending().size() == 0 && todo.getProgress().size() == 0 && todo.getCompleted().size() == 0 && todo.getRejected().size() == 0) {
            mBoardView.setVisibility(View.GONE);
            displayLayoutVoid(true);
        } else {
            displayLayoutVoid(false);
            //displayBtnAdd(true);
            mBoardView.setVisibility(View.VISIBLE);
            LIST_PENDING = todo.getPending();
            LIST_PROGRESS = todo.getProgress();
            LIST_COMPLETED = todo.getCompleted();
            LIST_EXPIRED = todo.getRejected();

            Log.d(TAG, "adapteLists:LIST_PENDING " + Integer.toString(LIST_PENDING.size()));
            Log.d(TAG, "adapteLists:LIST_PROGRESS " + Integer.toString(LIST_PROGRESS.size()));
            Log.d(TAG, "adapteLists:LIST_COMPLETED " + Integer.toString(LIST_COMPLETED.size()));
            Log.d(TAG, "adapteLists:LIST_EXPIRED " + Integer.toString(LIST_EXPIRED.size()));

            addColumnList(todo.getPending());
            addColumnList2(todo.getProgress());
            addColumnList3(todo.getCompleted());
            addColumnList4(todo.getRejected());
        }
    }

    @Override
    public void adapteSimple(TodoSimple simple, String id, int status) {
        Log.d(TAG, "adapteSimple");
        switch (status) {
            case 2:
                Log.d(TAG, "adapteSimple .. 2");
                LIST_PROGRESS.remove(id);
                Log.d(TAG, "remove:LIST_PENDING " + Integer.toString(LIST_PROGRESS.size()));
                addList(simple, status);
                break;
            case 3:
                Log.d(TAG, "adapteSimple .. 3");
                LIST_COMPLETED.remove(id);
                Log.d(TAG, "remove:LIST_COMPLETED " + Integer.toString(LIST_COMPLETED.size()));
                addList(simple, status);
                break;
            case 6:
                Log.d(TAG, "adapteSimple .. 6");
                LIST_EXPIRED.remove(id);
                Log.d(TAG, "remove:LIST_EXPIRED " + Integer.toString(LIST_EXPIRED.size()));
                addList(simple, status);
                break;
        }
    }

    private void addList(TodoSimple simple, int status) {
        Log.d(TAG, "addList ");
        Log.d(TAG, "ADDLIST: " + Integer.toString(status));
        Log.d(TAG, "TodoSimple: " + Integer.toString(simple.getStatus()));
        switch (status) {
            case 2:
                switch (simple.getStatus()) {
                    case 2:
                        Log.d(TAG, "addList .. 2");
                        LIST_PENDING.add(Functions.SimpleToPending(simple));
                        Log.d(TAG, "adapteSimple:LIST_PENDING " + Integer.toString(LIST_PENDING.size()));
                        break;
                    case 3:
                        Log.d(TAG, "addList .. 3");
                        LIST_COMPLETED.add(Functions.SimpleToCompleted(simple));
                        Log.d(TAG, "adapteSimple:LIST_COMPLETED " + Integer.toString(LIST_COMPLETED.size()));
                        break;
                    case 6:
                        Log.d(TAG, "addList .. 6");
                        LIST_EXPIRED.add(Functions.SimpleToRejected(simple));
                        Log.d(TAG, "adapteSimple:LIST_EXPIRED " + Integer.toString(LIST_EXPIRED.size()));
                        break;
                }
                break;
            case 3:
                switch (simple.getStatus()) {
                    case 2:
                        LIST_PENDING.add(Functions.SimpleToPending(simple));
                        Log.d(TAG, "adapteSimple:LIST_PENDING " + Integer.toString(LIST_PENDING.size()));
                        break;
                    case 3:
                        LIST_COMPLETED.add(Functions.SimpleToCompleted(simple));
                        Log.d(TAG, "adapteSimple:LIST_COMPLETED " + Integer.toString(LIST_COMPLETED.size()));
                        break;
                    case 6:
                        LIST_EXPIRED.add(Functions.SimpleToRejected(simple));
                        Log.d(TAG, "adapteSimple:LIST_EXPIRED " + Integer.toString(LIST_EXPIRED.size()));
                        break;
                }
                break;
            case 6:
                switch (simple.getStatus()) {
                    case 2:
                        LIST_PENDING.add(Functions.SimpleToPending(simple));
                        Log.d(TAG, "adapteSimple:LIST_PENDING " + Integer.toString(LIST_PENDING.size()));
                        break;
                    case 3:
                        LIST_COMPLETED.add(Functions.SimpleToCompleted(simple));
                        Log.d(TAG, "adapteSimple:LIST_COMPLETED " + Integer.toString(LIST_COMPLETED.size()));
                        break;
                    case 6:
                        LIST_EXPIRED.add(Functions.SimpleToRejected(simple));
                        Log.d(TAG, "adapteSimple:LIST_EXPIRED " + Integer.toString(LIST_EXPIRED.size()));
                        break;
                }
                break;
        }
    }

    private void addColumnList(List<Pending> pendingList) {
        final ArrayList<Pair<Long, ItemTodo>> mItemArray = new ArrayList<>();
        for (int i = 0; i < pendingList.size(); i++) {
            long id = sCreatedItems++;
            ItemTodo itemTodo = new ItemTodo();
            Pending pending = pendingList.get(i);
            itemTodo.setName(pending.getName());
            itemTodo.setDate(pending.getCreated_at());
            itemTodo.setId(Long.parseLong(pending.getId()));
            mItemArray.add(new Pair<>(id, itemTodo));
        }

        final TodoAdapte2 listAdapter = new TodoAdapte2(mItemArray, R.layout.column_item, R.id.item_layout, true);
        final View header = View.inflate(getActivity(), R.layout.column_header, null);
        ((TextView) header.findViewById(R.id.text)).setText("Pendiente ");
        mBoardView.addColumnList(listAdapter, header, false);
        mColumns++;
    }

    private void addColumnList2(List<Progress> progressList) {
        final ArrayList<Pair<Long, ItemTodo>> mItemArray = new ArrayList<>();
        for (int i = 0; i < progressList.size(); i++) {
            long id = sCreatedItems++;
            ItemTodo itemTodo = new ItemTodo();
            itemTodo.setName(progressList.get(i).getName());
            itemTodo.setDate(progressList.get(i).getCreated_at());
            itemTodo.setId(Long.parseLong(progressList.get(i).getId()));
            mItemArray.add(new Pair<>(id, itemTodo));
        }
        final TodoAdapte2 listAdapter = new TodoAdapte2(mItemArray, R.layout.column_item, R.id.item_layout, true);
        final View header = View.inflate(getActivity(), R.layout.column_header, null);
        ((TextView) header.findViewById(R.id.text)).setText("Progreso ");
        mBoardView.addColumnList(listAdapter, header, false);
        mColumns++;
    }

    private void addColumnList3(List<Completed> completedList) {
        final ArrayList<Pair<Long, ItemTodo>> mItemArray = new ArrayList<>();
        for (int i = 0; i < completedList.size(); i++) {
            long id = sCreatedItems++;
            ItemTodo itemTodo = new ItemTodo();
            itemTodo.setName(completedList.get(i).getName());
            itemTodo.setDate(completedList.get(i).getCreated_at());
            itemTodo.setId(Long.parseLong(completedList.get(i).getId()));
            mItemArray.add(new Pair<>(id, itemTodo));
        }
        final TodoAdapte2 listAdapter = new TodoAdapte2(mItemArray, R.layout.column_item, R.id.item_layout, true);
        final View header = View.inflate(getActivity(), R.layout.column_header, null);
        ((TextView) header.findViewById(R.id.text)).setText("Completada ");
        mBoardView.addColumnList(listAdapter, header, false);
        mColumns++;
    }

    private void addColumnList4(List<Rejected> rejectedList) {
        final ArrayList<Pair<Long, ItemTodo>> mItemArray = new ArrayList<>();
        for (int i = 0; i < rejectedList.size(); i++) {
            long id = sCreatedItems++;
            ItemTodo itemTodo = new ItemTodo();
            itemTodo.setName(rejectedList.get(i).getName());
            itemTodo.setDate(rejectedList.get(i).getCreated_at());
            itemTodo.setId(Long.parseLong(rejectedList.get(i).getId()));
            mItemArray.add(new Pair<>(id, itemTodo));
        }

        final TodoAdapte2 listAdapter = new TodoAdapte2(mItemArray, R.layout.column_item, R.id.item_layout, true);
        final View header = View.inflate(getActivity(), R.layout.column_header, null);
        ((TextView) header.findViewById(R.id.text)).setText("Rechazada ");
        mBoardView.addColumnList(listAdapter, header, false);
        mColumns++;
    }

    @Override
    public void showRefresh(boolean show) {
        if (isAdded()) {

        }
    }

    @Override
    public void showViewProgress(final boolean show) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @Override
    public void setRefreshing(boolean refreshing) {

    }

    @Override
    public void displayLayoutVoid(final boolean empty) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            layout_no_proyect.setVisibility(empty ? View.VISIBLE : View.GONE);
            layout_no_proyect.animate().setDuration(shortAnimTime).alpha(empty ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    layout_no_proyect.setVisibility(empty ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @Override
    public boolean isAddedFragment() {
        return isAdded();
    }


    public void displayBtnAdd(final boolean add) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            fabAdd.setVisibility(add ? View.VISIBLE : View.GONE);
            fabAdd.animate().setDuration(shortAnimTime).alpha(add ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    fabAdd.setVisibility(add ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @Override
    public void displayLayoutConnection(final boolean connection) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
            layout_Connection.animate().setDuration(shortAnimTime).alpha(connection ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @OnClick(R.id.layout_no_proyect)
    public void TryAgainProyect() {
        presenter.setView(this, 1);
        presenter.getList();
    }

    @Override
    public void displayLayoutNocontent(final boolean content) {
        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
            layout_noContent.animate().setDuration(shortAnimTime).alpha(content ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    @OnClick(R.id.text_try_again_http)
    public void TryAgainError() {
        presenter.setView(this, 1);
        presenter.getList();
    }

    @OnClick(R.id.text_try_again)
    public void TryAgainConection() {
        presenter.setView(this, 1);
        presenter.getList();
    }

    @Override
    public void displayLayoutError(final boolean status, final String msg) {


        if (isAdded()) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            layout_code_http.setVisibility(status ? View.VISIBLE : View.GONE);
            layout_code_http.animate().setDuration(shortAnimTime).alpha(status ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    layout_code_http.setVisibility(status ? View.VISIBLE : View.GONE);
                }
            });
            code_http.setText(msg);

        }
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    private class MyDragItem extends DragItem {

        public MyDragItem(Context context, int layoutId) {
            super(context, layoutId);
        }

        @Override
        public void onBindDragView(View clickedView, View dragView) {
            CharSequence text = ((TextView) clickedView.findViewById(R.id.text)).getText();
            CharSequence date = ((TextView) clickedView.findViewById(R.id.date)).getText();
            ((TextView) dragView.findViewById(R.id.text)).setText(text);
            ((TextView) dragView.findViewById(R.id.date)).setText(date);
            CardView dragCard = ((CardView) dragView.findViewById(R.id.card));
            CardView clickedCard = ((CardView) clickedView.findViewById(R.id.card));
            dragCard.setMaxCardElevation(40);
            dragCard.setCardElevation(clickedCard.getCardElevation());
            dragCard.setForeground(clickedView.getResources().getDrawable(R.drawable.card_view_drag_foreground));
        }

        @Override
        public void onMeasureDragView(View clickedView, View dragView) {
            CardView dragCard = ((CardView) dragView.findViewById(R.id.card));
            CardView clickedCard = ((CardView) clickedView.findViewById(R.id.card));
            int widthDiff = dragCard.getPaddingLeft() - clickedCard.getPaddingLeft() + dragCard.getPaddingRight() - clickedCard.getPaddingRight();
            int heightDiff = dragCard.getPaddingTop() - clickedCard.getPaddingTop() + dragCard.getPaddingBottom() - clickedCard.getPaddingBottom();
            int width = clickedView.getMeasuredWidth() + widthDiff;
            int height = clickedView.getMeasuredHeight() + heightDiff;
            dragView.setLayoutParams(new FrameLayout.LayoutParams(width, height));
            int widthSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
            int heightSpec = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
            dragView.measure(widthSpec, heightSpec);
        }

        @Override
        public void onStartDragAnimation(View dragView) {
            CardView dragCard = ((CardView) dragView.findViewById(R.id.card));
            ObjectAnimator anim = ObjectAnimator.ofFloat(dragCard, "CardElevation", dragCard.getCardElevation(), 40);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.setDuration(ANIMATION_DURATION);
            anim.start();
        }

        @Override
        public void onEndDragAnimation(View dragView) {
            CardView dragCard = ((CardView) dragView.findViewById(R.id.card));
            ObjectAnimator anim = ObjectAnimator.ofFloat(dragCard, "CardElevation", dragCard.getCardElevation(), 6);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.setDuration(ANIMATION_DURATION);
            anim.start();
        }
    }
}