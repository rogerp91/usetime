package co.usetime.services;

/**
 * Created by Roger Patiño on 02/12/2015.
 */
public class ManagerService {
    public static final String ACTION = "action."; // Action to start

    public static final String ACTION_START = "START"; // Action to start
    public static final String ACTION_STOP = "STOP"; // Action to stop
    public static final String ACTION_KEEPALIVE = "KEEPALIVE"; // Action to keep alive used by alarm manager
    public static final String ACTION_RECONNECT = "RECONNECT"; // Action to reconnect
    public static final String ACTION_CANCEL_RECONNECT = "RECONNECT"; // Action to reconnect
    public static final String ACTION_ALIVE = "ALIVE"; // Action to reconnect
    public static final String ACTION_STARTED = "STARTED"; // Action to reconnect
    public static final String ACTION_SCHEDULE = "SCHEDULE"; // Action to reconnect

    //services position
    public static final String ACTION_SETTING_GPS = ACTION + "setting.gps";

    //time
    public static final String HOUR_LOCALITATION = "hour_localitation";
    public static final String MINUTE_LOCALITATION = "minute_localitation";

}