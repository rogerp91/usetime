package co.usetime.services;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;

import co.usetime.UIApp;
import co.usetime.domain.repository.imp.TrafficRepositoryImp;
import co.usetime.domain.repository.interfaces.TrafficRepository;
import co.usetime.mvp.model.Traffic;
import co.usetime.mvp.model.TrafficItems;
import co.usetime.utils.Prefs;


/**
 * Created by Roger Patiño on 06/04/2016.
 */
public class TrafficService extends BaseService {
    protected static String TAG = TrafficService.class.getSimpleName();
    private static Context context = UIApp.getContext;

    public static void actionStart() {
        Intent i = new Intent(context, TrafficService.class);
        i.setAction((ManagerService.ACTION_START));
        context.startService(i);
    }

    public static void actionStop() {
        Intent i = new Intent(context, TrafficService.class);
        i.setAction((ManagerService.ACTION_STOP));
        context.stopService(i);
    }

    //variables
    public final static String WIFI = "wifi";
    public final static String MOBILE = "mobile";
    public final static String TOTAL = "total";

    //backgroud
    private Handler handler = null;
    private long dataUsageTotalLast = 0;
    private ArrayList<TrafficItems> trafficApplications = null;
    private TrafficRepository<Traffic> trafficRepository = null;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        trafficApplications = new ArrayList<>();
        //iconSize = Math.round(32 * getResources().getDisplayMetrics().density);
        if (TrafficStats.getTotalRxBytes() != TrafficStats.UNSUPPORTED && TrafficStats.getTotalTxBytes() != TrafficStats.UNSUPPORTED) {
            handler.postDelayed(runnable, 0);
            init();
        } else {
            //si no soporta traffic detener
            //stopSelf();
        }
    }

    public Runnable runnable = new Runnable() {
        public void run() {
            //Log.d(TAG, "---- Runnable ----");
            long wifiG = 0;
            long mobileG = 0;
            long totalG = 0;

            //trafico mobile y total
            long mobile = TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes();
            long total = TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes();

            //calculo a 1024byte
            wifiG = (total - mobile) / 1024;
            mobileG = mobile / 1024;
            totalG = total / 1024;

            System.out.println("DATA: " + wifiG + ", " + mobileG + ", " + totalG);

            //agregarla a preference
            Prefs.putLong(WIFI, wifiG);
            Prefs.putLong(MOBILE, mobileG);
            Prefs.putLong(TOTAL, totalG);

            if (dataUsageTotalLast != total) {
                dataUsageTotalLast = total;
                update();
            }
            handler.postDelayed(runnable, 180000);
        }
    };

    private synchronized void init() {
        Log.d(TAG, "---- init ----");
        trafficRepository = new TrafficRepositoryImp();
        for (ApplicationInfo app : getPackageManager().getInstalledApplications(0)) {
            final TrafficItems item = TrafficItems.create(app);
            if (item != null) {
                Log.d(TAG, "init: package");
                Log.d(TAG, "init: package " + item.getPackage() + ", totalKb: " + item.getTotalUsageKb());
                trafficApplications.add(item);
                Traffic traffic = new Traffic();
                traffic.setmPackage(item.getPackage());
                traffic.setmName(item.getApplicationLabel(getApplicationContext().getPackageManager()));
                traffic.setmTotalKb(item.getTotalUsageKb());

                if (isConnectedMobile()) {
                    item.setMobilTraffic(true);
                    traffic.setmMovilKb(item.getTotalUsegeMovilKb());
                    Log.d(TAG, "Package: " + item.getPackage() + ", Mobile Kb:" + Long.toString(item.getTotalUsegeMovilKb()));
                }

                if (isConnectedWifi()) {
                    item.setMobilTraffic(false);
                    traffic.setmWifiKb(item.getTotalUsegeWifiKb());
                    Log.d(TAG, "Package: " + item.getPackage() + ", Wifi Kb:" + Long.toString(item.getTotalUsegeWifiKb()));
                }


                Log.d(TAG, "Name:" + item.getPackage() + ",Total: " + Long.toString(item.getTotalUsageKb()) + ",Movil:  " + Integer.toString(item.getTotalUsegeMovilKb()));
                Log.d(TAG, "Name:" + item.getPackage() + ",Total: " + Long.toString(item.getTotalUsageKb()) + ", WIFI: " + Integer.toString(item.getTotalUsegeWifiKb()));


                Log.d(TAG, "Init Kb:" + Long.toString(item.getTotalUsageKb()));
                Log.d(TAG, item.getPackage());
                Log.d(TAG, "movil " + Integer.toString(item.getTotalUsegeMovilKb()));
            }
        }
    }

    private synchronized void update() {
        Log.d(TAG, "---- Update ----");
        trafficRepository = new TrafficRepositoryImp();
        for (int i = 0, l = trafficApplications.size(); i < l; i++) {
            final TrafficItems app = trafficApplications.get(i);
            //Log.d(TAG, "Update: " + app.getPackage() + ", totalKb: " + app.getTotalUsageKb());

            if (isConnectedMobile()) {
                app.setMobilTraffic(true);
            }
            if (isConnectedWifi()) {
                app.setMobilTraffic(false);
            }

            app.update();
            Traffic traffic = new Traffic();
            traffic.setmPackage(app.getPackage());
            traffic.setmName(app.getApplicationLabel(getApplicationContext().getPackageManager()));
            traffic.setmTotalKb(app.getTotalUsageKb());

            if (isConnectedMobile()) {
                app.setMobilTraffic(true);
                traffic.setmMovilKb(app.getTotalUsegeMovilKb());
                Log.d(TAG, "Package: " + app.getPackage() + ", Mobile Kb: " + Long.toString(app.getTotalUsegeMovilKb()));
            }

            if (isConnectedWifi()) {
                app.setMobilTraffic(false);
                traffic.setmWifiKb(app.getTotalUsegeWifiKb());
                Log.d(TAG, "Package: " + app.getPackage() + ", Wifi Kb: " + Long.toString(app.getTotalUsegeWifiKb()));
            }

            app.setMobilTraffic(false);
            Log.d(TAG, "Name:" + app.getPackage() + ", Total: " + Long.toString(app.getTotalUsageKb()) + ", Movil:  " + Integer.toString(app.getTotalUsegeMovilKb()));
            Log.d(TAG, "Name:" + app.getPackage() + ", Total: " + Long.toString(app.getTotalUsageKb()) + ", WIFI: " + Integer.toString(app.getTotalUsegeWifiKb()));
            Log.d(TAG, "Update Kb:" + Long.toString(app.getTotalUsageKb()));
            Log.d(TAG, app.getPackage());
            Log.d(TAG, "movil " + Integer.toString(app.getTotalUsegeMovilKb()));
        }
    }

    public boolean isConnectedWifi() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public boolean isConnectedMobile() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

}