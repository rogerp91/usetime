package co.usetime.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import co.usetime.UIApp;

/**
 * Created by Roger Patiño on 06/05/2016.
 */
public class PositionService extends IntentService {
    protected static String TAG = PositionService.class.getSimpleName();
    private static Context context = UIApp.getContext;
    public static boolean INSTANCE;

    public static void actionStart() {
        Intent i = new Intent(context, PositionService.class);
        i.setAction((ManagerService.ACTION_START));
        context.startService(i);
    }

    public static void actionStop() {
        Intent i = new Intent(context, PositionService.class);
        i.setAction((ManagerService.ACTION_STOP));
        context.stopService(i);
    }

    public PositionService() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        if (action == null) {
            Log.d(TAG, "Starting service with no action\n Probably from a crash");
            stopSelf();
        } else {
            if (action.equals(ManagerService.ACTION_START)) {
                Log.i(TAG, ManagerService.ACTION_START);
                start();
            } else {
                if (action.equals(ManagerService.ACTION_STOP)) {
                    Log.i(TAG, ManagerService.ACTION_STOP);
                    stop();
                    stopSelf();
                }
            }
        }
    }

    private synchronized void start() {

    }

    private synchronized void stop() {

    }

}