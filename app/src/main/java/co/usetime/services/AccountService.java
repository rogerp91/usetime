package co.usetime.services;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import co.usetime.R;
import co.usetime.UIApp;
import co.usetime.ui.activity.LoginActivity;
import co.usetime.ui.activity.MainActivity;
import co.usetime.utils.Constants;

public class AccountService extends BaseService {

    private static final String TAG = AccountService.class.getSimpleName();
    private static AccountAuthenticatorImpl sAccountAuthenticator = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "Binding the service");
        IBinder ret = null;
        if (intent.getAction().equals(AccountManager.ACTION_AUTHENTICATOR_INTENT)) {
            ret = getAuthenticator().getIBinder();
            return ret;
        } else {
            Log.e(TAG, "Action, Null");
            return null;
        }
    }

    private AccountAuthenticatorImpl getAuthenticator() {
        if (AccountService.sAccountAuthenticator == null) {
            AccountService.sAccountAuthenticator = new AccountAuthenticatorImpl(UIApp.getContext);
        }

        return AccountService.sAccountAuthenticator;
    }

    private static class AccountAuthenticatorImpl extends AbstractAccountAuthenticator {

        Context context = null;

        public AccountAuthenticatorImpl(Context context) {
            super(context);
            this.context = context;
        }

        @Override
        public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
            return null;
        }

        @Override
        public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
            Log.d(TAG, "Add Account");
            Bundle bundle = new Bundle();
            if (UIApp.getAccounts()) {
                bundle.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, true);
                MainActivity.ACTIVITY.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, context.getResources().getText(R.string.perm_account), Toast.LENGTH_SHORT).show();
                    }
                });
                return bundle;
            } else {
                Log.d(TAG, "The auth token type is " + authTokenType);
                Intent i = new Intent(context, LoginActivity.class);
                i.setAction(Constants.ADD_ACCOUNT_TYPE);
                i.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
                i.putExtra(Constants.AUTH_TOKEN_TYPE, authTokenType);
                bundle.putParcelable(AccountManager.KEY_INTENT, i);
                return bundle;
            }
        }

        @Override
        public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
            return null;
        }

        @Override
        public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
            final Bundle bundle = new Bundle();
            if (UIApp.getAccounts()) {
                MainActivity.ACTIVITY.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, context.getResources().getText(R.string.perm_account), Toast.LENGTH_SHORT).show();
                        bundle.putString(AccountManager.KEY_ERROR_MESSAGE, "Solo se admite una cuenta Usetime");
                    }
                });
                return bundle;
            }
            return null;
        }

        @Override
        public String getAuthTokenLabel(String authTokenType) {
            return null;
        }

        @Override
        public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
            return null;
        }

        @Override
        public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
            return null;
        }
    }

}