package co.usetime.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import android.util.Log;

import com.google.gson.Gson;

import co.usetime.UIApp;
import co.usetime.componets.LocationProvider;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.domain.repository.imp.LocationRepositoryImp;
import co.usetime.domain.repository.interfaces.LocationRepository;
import co.usetime.mvp.model.Geoposition;
import co.usetime.mvp.model.Loct;
import co.usetime.mvp.model.Position;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LocationService extends BaseService implements LocationProvider.LocationCallback {
    protected static String TAG = LocationService.class.getSimpleName();

    public static Context context = UIApp.getContext;
    public static boolean INSTANCE;

    public static void actionStart(Context ctx) {
        Intent i = new Intent(ctx, LocationService.class);
        i.setAction(ManagerService.ACTION_START);
        context.startService(i);
    }

    public static void actionStop(Context ctx) {
        Intent i = new Intent(ctx, LocationService.class);
        i.setAction(ManagerService.ACTION_STOP);
        context.startService(i);
    }

    public static void actionStop2(Context ctx) {
        Intent i = new Intent(ctx, LocationService.class);
        i.setAction(ManagerService.ACTION_STOP);
        context.stopService(i);
    }
    public static void actionAlive(Context ctx) {
        Intent i = new Intent(ctx, LocationService.class);
        i.setAction(ManagerService.ACTION_ALIVE);
        context.startService(i);
    }

    //var and GPS
    private LocationProvider mLocationProvider;
    protected LocationManager locationManager;
    ConnectionThread mConnection = null;
    int time = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.getAction().equals(ManagerService.ACTION_START)) {
                start();
            } else {
                if (intent.getAction().equals(ManagerService.ACTION_STOP)) {
                    stop();
                } else {
                    if (intent.getAction().equals(ManagerService.ACTION_RECONNECT)) {
                        reconnectNecessary();
                    }else{
                        if(intent.getAction().equals(ManagerService.ACTION_ALIVE)){
                            stopSelf();
                        }
                    }
                }
            }
        } else {
            stopSelf();
        }
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        INSTANCE = false;
    }

    //----------------------------------------------------------------------------------------------
    private synchronized void start() {
        Log.d(TAG, "startServices");
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            sendBroadcast(new Intent().setAction(ManagerService.ACTION_SETTING_GPS));
        } else {
            connectLocation();
        }
    }

    private synchronized void reconnectNecessary() {
        if ((mConnection == null || !mConnection.isAlive())) {
            Log.i(TAG, "Reconnecting...");
            connectLocation();
        }
    }

    private synchronized void connectLocation() {
        Log.d(TAG, "connect");
        mLocationProvider = new LocationProvider(this);
        mLocationProvider.connect();
    }

    private synchronized void stop() {
        if (mLocationProvider != null) {
            mLocationProvider.disconnect();
        }
        if (mConnection != null) {
            mConnection.abort();
            mConnection = null;
        }
        cancelReconnect();
    }

    public synchronized void cancelReconnect() {
        Log.i(TAG, "cancelReconnect");
        Intent i = new Intent();
        i.setClass(this, LocationService.class);
        i.setAction(ManagerService.ACTION_ALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        AlarmManager alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmMgr.cancel(pi);
    }

    public synchronized void scheduleReconnect(int startTime) {
        Log.i(TAG, "scheduleReconnect");
        Log.i(TAG, "startTime: " + Integer.toString(startTime));
        Intent i = new Intent();
        i.setClass(this, LocationService.class);
        i.setAction(ManagerService.ACTION_RECONNECT);
        PendingIntent alarmIntent = PendingIntent.getService(this, 0, i, 0);
        AlarmManager alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + startTime, alarmIntent);
    }

    @Override
    public void handleNewLocation(Location location) {
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        System.out.println(TAG + ": " + currentLongitude + ":" + currentLatitude);
        if (location != null) {
            if (location.getLatitude() != 0 && location.getLongitude() != 0) {
                Loct loct1 = new Loct();
                loct1.setLongitude(location.getLongitude());
                loct1.setLatitude(location.getLatitude());
                loct1.setSend(true);
                LocationRepository<Loct> locationRepository = new LocationRepositoryImp();
                //locationRepository.persist(loct1);
                postGeolocalization(location);
            } else {
            }
        } else {
            mLocationProvider.connect();
        }

    }

    private void postGeolocalization(Location location) {
        //System.out.println(TAG + "LOC: " + location.getLatitude() + ":" + location.getLatitude());
        mConnection = new ConnectionThread(location.getLatitude(), location.getLongitude());
        mConnection.start();
    }

    private class ConnectionThread extends Thread {

        private HttpRestClient client = null;
        private Call<Void> call = null;

        private volatile boolean mAbort = false;
        private volatile boolean mRun = true;

        public ConnectionThread(double latitude, double longitude) {
            //System.out.println(TAG + "LOCATION: " + longitude + ":" + latitude);/
            /**
            System.out.println(TAG + " GSON:" + new Gson().toJson(new Position(new Geoposition(latitude, longitude))));
            client = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(false), Functions.provideOkHttpClient()));
            call = client.perfomGeoposition(HttpRestClient.PREFIX + Prefs.getString(Constants.TOKEN, ""), new Gson().toJson(new Geoposition(latitude, longitude)));
            time = Prefs.getInt(ManagerService.HOUR_LOCALITATION, 0) + Prefs.getInt(ManagerService.MINUTE_LOCALITATION, 0);
            Log.d(TAG, "TIME: " + time);
            if (time == 0) {
                time = Functions.getMinuteToMilisecond(Integer.toString(1));
            }
             */
            client = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(false), Functions.provideOkHttpClient()));
            call = client.perfomGeoposition(HttpRestClient.PREFIX + Prefs.getString(Constants.TOKEN, ""), new Geoposition(latitude, longitude));
            time = Prefs.getInt(ManagerService.HOUR_LOCALITATION, 0) + Prefs.getInt(ManagerService.MINUTE_LOCALITATION, 0);
            if (time == 0) {
                time = Functions.getMinuteToMilisecond(Integer.toString(1));
            }

        }

        @Override
        public void run() {
            super.run();
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Response<Void> response, Retrofit retrofit) {
                    switch (response.code()) {
                        case 200:
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            System.out.println("Body: " + response.body());
                            System.out.println("Body: " + response.message());
                            boolean error = false;
                            mLocationProvider.disconnect();
                            scheduleReconnect(time);
                            break;
                        case 400:
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 401:
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 403:
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 404:
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 500:
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        default:
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    mRun = false;
                }
            });
        }

        public void abort() {
            while (true) {
                try {
                    mAbort = true;
                    join();
                    break;
                } catch (InterruptedException e) {
                    Log.i(TAG, "Cant't join");
                }
            }
        }
    }
}