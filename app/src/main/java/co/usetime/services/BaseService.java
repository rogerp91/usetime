package co.usetime.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public abstract class BaseService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
