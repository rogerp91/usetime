package co.usetime.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.usetime.UIApp;
import co.usetime.domain.repository.api.HttpRestClient;
import co.usetime.domain.repository.imp.ClassRepository;
import co.usetime.domain.repository.imp.ProcessRepository;
import co.usetime.domain.repository.interfaces.Repository2;
import co.usetime.exception.GetException;
import co.usetime.mvp.model.AppProcess;
import co.usetime.mvp.model.CaptureApp;
import co.usetime.mvp.model.Example;
import co.usetime.mvp.model.NameClass;
import co.usetime.mvp.model.Registries;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 06/05/2016.
 */
public class UploadProcessService extends BaseService {

    protected static String TAG = UploadProcessService.class.getSimpleName();
    private static Context context = UIApp.getContext;
    public static boolean INSTANCE;

    private boolean mStarted = false; // Is the Client started?
    private AlarmManager mAlarmManager;            // Alarm manager to perform repeating tasks
    private ConnectivityManager mConnectivityManager; // To check for connectivity changes
    private static final int KEEP_ALIVE = 300000; // KeepAlive Interval in MS 300000

    public static void actionStart(Context ctx) {
        Intent i = new Intent(ctx, UploadProcessService.class);
        i.setAction((ManagerService.ACTION_START));
        context.startService(i);
    }

    public static void actionStop(Context ctx) {
        Intent i = new Intent(ctx, UploadProcessService.class);
        i.setAction((ManagerService.ACTION_STOP));
        context.startService(i);
    }

    public static void actionKeepalive(Context ctx) {
        Intent i = new Intent(ctx, UploadProcessService.class);
        i.setAction(ManagerService.ACTION_KEEPALIVE);
        context.startService(i);
    }

    public static void actionAlive(Context ctx) {
        Intent i = new Intent(ctx, ProcessService.class);
        i.setAction(ManagerService.ACTION_ALIVE);
        context.startService(i);
    }

    private UploadThread mUploadThread = null;
    private HttpRestClient client = null;
    private String token;
    private Repository2<AppProcess> processRepository = null;
    private Repository2<NameClass> classRepository = null;
    private List<Registries> registriesList;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = true;
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action == null) {
            Log.d(TAG, "Starting service with no action Probably from a crash");
            stopSelf();
        } else {
            if (action.equals(ManagerService.ACTION_START)) {
                Log.i(TAG, ManagerService.ACTION_START);
                start();
            } else {
                if (action.equals(ManagerService.ACTION_STOP)) {
                    Log.i(TAG, ManagerService.ACTION_STOP);
                    stop();
                    stopSelf();
                } else {
                    if (action.equals(ManagerService.ACTION_KEEPALIVE)) {
                        keepAlive();
                    } else {
                        if (action.equals(ManagerService.ACTION_RECONNECT)) {
                            if (isNetworkAvailable()) {
                                //reconnectIfNecessary();
                            }
                        }
                    }
                }
            }
        }
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        INSTANCE = false;
    }

    private synchronized void start() {
        if (mStarted) {
            Log.d(TAG, "Attempt to start while already started");
            return;
        }

        if (hasScheduledKeepAlives()) {
            stopKeepAlives();
        }

        connect();
        registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

    }

    private synchronized void stop() {
        if (!mStarted) {
            Log.d(TAG, "Attemtpign to stop connection that isn't running");
            return;
        }
        mUploadThread = null;
        client = null;
        mStarted = false;
        stopKeepAlives();
        unregisterReceiver(mConnectivityReceiver);
    }

    /**
     * Schedules keep alives via a PendingIntent
     * in the Alarm Manager
     */
    private void startKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, UploadProcessService.class);
        i.setAction(ManagerService.ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + KEEP_ALIVE, KEEP_ALIVE, pi);
    }

    /**
     * Cancels the Pending Intent
     * in the alarm manager
     */
    private void stopKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, UploadProcessService.class);
        i.setAction(ManagerService.ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        mAlarmManager.cancel(pi);
    }

    /**
     * Checkes the current connectivity
     * and reconnects if it is required.
     */
    private synchronized void reconnectIfNecessary() {
        connect();
    }

    /**
     * Query's the NetworkInfo via ConnectivityManager
     * to return the current connected state
     *
     * @return boolean true if we are connected false otherwise
     */
    private boolean isNetworkAvailable() {
        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }


    /**
     * Receiver that listens for connectivity chanes
     * via ConnectivityManager
     */
    private final BroadcastReceiver mConnectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Connectivity Changed...");
        }
    };

    /**
     * Query's the AlarmManager to check if there is
     * a keep alive currently scheduled
     *
     * @return true if there is currently one scheduled false otherwise
     */
    private synchronized boolean hasScheduledKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, UploadProcessService.class);
        i.setAction(ManagerService.ACTION_KEEPALIVE);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, PendingIntent.FLAG_NO_CREATE);
        return (pi != null);
    }

    private synchronized void connect() {
        Log.d(TAG, "Connecting with URL: ");
        List<AppProcess> appProcesses = new ArrayList<>();
        registriesList = new ArrayList<>();
        token = Prefs.getString(Constants.TOKEN, "");
        if (processRepository == null) {
            processRepository = new ProcessRepository();
        }
        if (classRepository == null) {
            classRepository = new ClassRepository();
        }
        if (client == null) {
            client = Functions.provideHttpRestClient(Functions.provideRetrofit(Functions.provideGson(true), Functions.provideOkHttpClient()));
        }

        try {
            appProcesses = processRepository.obtain();
        } catch (GetException | NullPointerException e) {
            e.printStackTrace();
        }

        if (appProcesses.size() != 0) {
            for (AppProcess process : appProcesses) {
                Registries registries = new Registries();
                registries.setApplication(process.getmPackage());
                registries.setCaption(process.getmName());
                registries.setDatetime(process.getmDate());
                registries.setTimestamp(process.getmTime());
                registries.setDuration(process.getmDuration());
                registries.setPath(process.getmPath());
                registries.setKeystrokes(0);
                registries.setMouse(0);
                registries.setTaskId(0);
                registries.setType(0);
                registriesList.add(registries);
            }
        } else {
            Log.d(TAG, "processRepository.get == null");
        }

        mUploadThread = new UploadThread();
        mUploadThread.start();

    }

    /**
     * Publishes a KeepALive to the topic
     * in the broker
     */
    private synchronized void keepAlive() {
        reconnectIfNecessary();
    }

    private class UploadThread extends Thread {

        @Override
        public void run() {
            super.run();
            //Log.d(TAG, "run");
            String arrayListToJson = new Gson().toJson(registriesList);
            System.out.println(TAG + " -- " + arrayListToJson);
            Call<Void> call = client.perfomRegistries(HttpRestClient.PREFIX + token, registriesList);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Response<Void> response, Retrofit retrofit) {
                    System.out.println("headers : " + response.headers());
                    System.out.println("body : " + response.body());
                    System.out.println("message : " + response.message());
                    System.out.println("errorBody : " + response.errorBody());
                    System.out.println("raw : " + response.raw());
                    switch (response.code()) {
                        case 200:
                            System.out.println(response.body());
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            registriesList.clear();
                            processRepository.clear();
                            break;
                        case 204:
                            startKeepAlives();
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 400:
                            startKeepAlives();
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            System.out.println(response.body());
                            break;
                        case 401:
                            startKeepAlives();
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 403:
                            startKeepAlives();
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 404:
                            startKeepAlives();
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 422:
                            startKeepAlives();
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        case 500:
                            startKeepAlives();
                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                            break;
                        default:
                            break;
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e(TAG, t.toString());
                }
            });
        }

        public void abort() {
            while (true) {
                try {
                    join();
                    break;
                } catch (InterruptedException e) {
                    Log.i(TAG, "Cant't join");
                }
            }
        }

    }
}//endClass


