package co.usetime.services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import co.usetime.UIApp;
import co.usetime.domain.repository.imp.ProcessRepository;
import co.usetime.domain.repository.interfaces.Repository2;
import co.usetime.exception.AddException;
import co.usetime.exception.UniqueException;
import co.usetime.mvp.model.AppInfo;
import co.usetime.mvp.model.AppProcess;
import co.usetime.utils.Constants;
import co.usetime.utils.Functions;
import co.usetime.utils.Prefs;

/**
 * Created by Roger Patiño on 06/05/2016.
 */
public class ProcessService extends BaseService {

    protected static String TAG = ProcessService.class.getSimpleName();
    private static Context context = UIApp.getContext;
    public static boolean INSTANCE;

    public static void actionStart(Context ctx) {
        Intent i = new Intent(ctx, ProcessService.class);
        i.setAction((ManagerService.ACTION_START));
        context.startService(i);
    }

    public static void actionStop(Context ctx) {
        Intent i = new Intent(ctx, ProcessService.class);
        i.setAction((ManagerService.ACTION_STOP));
        context.stopService(i);
    }

    /**
     * var
     */
    private ExecutorService executorService = null;
    private ActivityManager activityManager = null;
    private ProcessThread thread = null;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = true;
        executorService = Executors.newSingleThreadExecutor();
        activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        thread = new ProcessThread();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        INSTANCE = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action == null) {
            Log.d(TAG, "Starting service with no action\n Probably from a crash");
            stopSelf();
        } else {
            if (action.equals(ManagerService.ACTION_START)) {
                Log.i(TAG, ManagerService.ACTION_START);
                start();
            } else {
                if (action.equals(ManagerService.ACTION_STOP)) {
                    Log.i(TAG, ManagerService.ACTION_STOP);
                    stop();
                    stopSelf();
                }
            }
        }
        return Service.START_REDELIVER_INTENT;
    }

    /**
     *
     */
    private synchronized void start() {
        if (executorService != null) {
            executorService.submit(thread);
        }
    }

    /**
     *
     */
    private synchronized void stop() {
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    /**
     *
     */
    public class ProcessThread implements Runnable {

        String isPackage = "";
        String isNameClass = "";

        //fecha
        String isDateOld = "";
        String isTimeOld = "";

        String isClassTime = "";
        String isClassDate = "";

        //valores que van hacer almacenado
        String mPackName = "";
        String mClassName = "";

        Repository2<AppProcess> processRepository = null;

        public ProcessThread() {
        }

        @Override
        public void run() {
            //Log.d(TAG, "run");
            while (true) {
                isPackage = Prefs.getString(Constants.IS_PACKAGE, "");
                isNameClass = Prefs.getString(Constants.IS_CLASS, "");

                processRepository = new ProcessRepository();

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    mPackName = activityManager.getAppTasks().get(0).getTaskInfo().topActivity.getPackageName();
                    mClassName = activityManager.getRunningTasks(1).get(0).topActivity.getShortClassName();
                } else {
                    mPackName = activityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
                    mClassName = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
                }

                //Log.d(TAG, Functions.getTimestamp());

                if (isPackage.equals("")) {
                    //Log.d(TAG, "no hay nada almacenado");
                    String mName = "";
                    String mPath = "";
                    List<AppInfo> appInfoList = UIApp.getInstalledApps(false);
                    for (int i = 0; i < appInfoList.size(); i++) {
                        if (appInfoList.get(i).packageName.equals(mPackName)) {
                            mName = appInfoList.get(i).getAppName();
                            mPath = appInfoList.get(i).getSrcDir();
                        }
                    }

                    Prefs.putString(Constants.IS_PACKAGE, mPackName);
                    Prefs.putString(Constants.IS_PACKAGE_NAME, mName);
                    //tiempo para calcular duracion
                    Prefs.putString(Constants.IS_PACKAGE_DATE, Functions.getDate());
                    Prefs.putString(Constants.IS_PACKAGE_TIME, Functions.getTime());

                    //vedadores fecha
                    Prefs.putString(Constants.IS_PACKAGE_DURATION_DATE, Functions.getDateTime2());
                    Prefs.putString(Constants.IS_PACKAGE_DURATION_TIME, Functions.getTimestamp());

                    Prefs.putString(Constants.IS_PACKAGE_DURATION, "");
                    Prefs.putString(Constants.IS_PACKAGE_PATH, mPath);

                } else {
                    if (isPackage.equals(mPackName)) {
                        //Log.d(TAG, "paquete son iguales");
                    } else {
                        //Log.d(TAG, "paquete son diferentes");

                        String mPackage = Prefs.getString(Constants.IS_PACKAGE, "");
                        isDateOld = Prefs.getString(Constants.IS_PACKAGE_DATE, "");
                        isTimeOld = Prefs.getString(Constants.IS_PACKAGE_TIME, "");

                        String duration = Functions.compareTime(isTimeOld, isDateOld);

                        String mName = "";
                        String mPath = "";
                        List<AppInfo> appInfoList = UIApp.getInstalledApps(false);
                        for (int i = 0; i < appInfoList.size(); i++) {
                            if (appInfoList.get(i).packageName.equals(mPackage)) {
                                mName = appInfoList.get(i).getAppName();
                                mPath = appInfoList.get(i).getSrcDir();
                            }
                        }

                        AppProcess appProcess = new AppProcess();
                        appProcess.setmPackage(mPackage);
                        appProcess.setmName(mName);
                        appProcess.setmDate(Prefs.getString(Constants.IS_PACKAGE_DURATION_DATE, ""));
                        appProcess.setmTime(Prefs.getString(Constants.IS_PACKAGE_DURATION_TIME, ""));
                        appProcess.setmPath(mPath);
                        appProcess.setmDuration(duration);

                        //Log.d(TAG, "NAME: " + mName);
                        try {
                            processRepository.add(appProcess);
                            //Log.d(TAG, "addApp");
                        } catch (AddException | UniqueException e) {
                            //Log.d(TAG, "Not-addApp");
                            Log.e(TAG, e.toString());
                        }

                        //nuevo proceso
                        String mNameSig = "";
                        String mPathSig = "";
                        List<AppInfo> appInfoList2 = UIApp.getInstalledApps(false);
                        for (int i = 0; i < appInfoList2.size(); i++) {
                            if (appInfoList2.get(i).packageName.equals(mPackName)) {
                                mNameSig = appInfoList2.get(i).getAppName();
                                mPathSig = appInfoList2.get(i).getSrcDir();
                            }
                        }
                        //Log.d(TAG, mPathSig);
                        Prefs.putString(Constants.IS_PACKAGE, mPackName);//almacenar la actividad nueva
                        Prefs.putString(Constants.IS_PACKAGE_NAME, mNameSig);
                        Prefs.putString(Constants.IS_PACKAGE_PATH, mPathSig);
                        Prefs.putString(Constants.IS_PACKAGE_DATE, Functions.getDate());
                        Prefs.putString(Constants.IS_PACKAGE_TIME, Functions.getTime());
                        Prefs.putString(Constants.IS_PACKAGE_DURATION, "");
                        Prefs.putString(Constants.IS_PACKAGE_DURATION_DATE, Functions.getDateTime2());
                        Prefs.putString(Constants.IS_PACKAGE_DURATION_TIME, Functions.getTimestamp());

                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}