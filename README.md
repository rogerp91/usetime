# Usetime #

Android - Code - Screenshot

------

# Descripción

Usetime es una aplicación desarrollada en Android basada en la nube que facilita la gestión local y remota de proyectos y tareas, se puede ver detalles de tareas, subida de fotos y archivos al servidor, comentarios, localización, todo de elementos, personas invitadas, cambias los estado de un tarea etc.

Tecnologías Utilizadas: ButterKnife, Dagger, Retrofit, GSON, Picasso, Play Services Location, Icepick, MaterialDialogs, FloatingActionButton, DragListView, SubsamplingScaleImageView y Firebase Google.

---

# Descripción

![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503365617/fa323a23-616f-4c3b-80d6-3a0007e1f422-large_vpxf1q.jpg)
![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503365616/c459aad7-7ea5-4035-a253-f7040b8edea4-large_axoplk.jpg)
![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503365616/62affb8c-8cd9-4eef-95e7-40046d83ba26-large_dc2t0i.jpg)
![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503365616/8e064678-6945-449d-b51c-06db7e7d3fdc-large_qn27gm.jpg)